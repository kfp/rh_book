BEGIN { print "\\input{style}"; }
END {}

/^\\begin{fricasmath}/ {
        print "$$"
        next                                                                    
}                                                                               
                                                                                
/^\\end{fricasmath}/ {                                                          
        print "$$"                                                    
        next                                                                    
} 

/^\\begin{spadsrc}/ {
        print "\\begin{verbatim}"
        next                                                                    
}                                                                               
                                                                                
/^\\end{spadsrc}/ {                                                          
        print "\\end{verbatim}"                                                    
        next                                                                    
} 


/^\\epsffile/ {
        #print "\\begin{figure}";
        print "\\includegraphics{../png/bessintr.png}";
        #print "\\end{figure}";
        next
}

{
gsub("\"\\$\"","\"\\$\"",$0); # fix $ by \$
gsub("\"\\@\"","\"\\@\"",$0); # fix @ by \@
gsub("%x","?x",$0); # ug08
gsub("%y","?y",$0);
gsub("%a","?a",$0);
gsub("{%}","{\\%}",$0); # fix % by \%
gsub("{%%}","{\\%\\%}",$0); # fix %% by \%\%
gsub("{%%\\(","{\\%\\%\(",$0); # fix %%( by \%\%(
gsub("spad{%","spad{\\%",$0); 
gsub("-> %","-> \\%",$0);
gsub("\\(%, ","\(\\%, ",$0);
gsub(" %\\)"," \\%\)",$0);
# fixed tex_orig ug07.tex, line 43 
gsub("%pi","pi\(\)",$0);
gsub("head{chapter}","\chapter",$0);
gsub("head{section}","\section",$0);
gsub("head{subsection}","\subsection",$0);
gsub("begin{MATRIX}","begin{bmatrix}",$0);
gsub("end{MATRIX}","end{bmatrix}",$0);
print $0
}
