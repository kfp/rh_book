Packages
========

ugPackages Packages provide the bulk of FriCAS’s algorithmic library,
from numeric packages for computing special functions to symbolic
facilities for differential equations, symbolic integration, and limits.

In , we developed several useful functions for drawing vector fields and
complex functions. We now show you how you can add these functions to
the FriCAS library to make them available for general use.

The way we created the functions in is typical of how you, as an
advanced FriCAS user, may interact with FriCAS. You have an application.
You go to your editor and create an input file defining some functions
for the application. Then you run the file and try the functions. Once
you get them all to work, you will often want to extend them, add new
features, perhaps write additional functions.

Eventually, when you have a useful set of functions for your
application, you may want to add them to your local FriCAS library. To
do this, you embed these function definitions in a package and add that
package to the library.

To introduce new packages, categories, and domains into the system, you
need to use the FriCAS compiler to convert the constructors into
executable machine code. An existing compiler in FriCAS is available on
an “as-is” basis. A new, faster compiler will be available in version
2.0 of FriCAS.

C ==> Complex DoubleFloat – All constructors used in a file S ==>
Segment DoubleFloat – must be spelled out in full INT ==> Integer –
unless abbreviated by macros DFLOAT ==> DoubleFloat – like these at the
top of VIEW3D ==> ThreeDimensionalViewport – a file. CURVE ==> List List
Point DFLOAT

)abbrev package DRAWCX DrawComplex – Identify kinds and abbreviations
DrawComplex(): Exports == Implementation where – Type definition begins
here.

Exports == with – Export part begins. drawComplex: (C -> C,S,S,Boolean)
-> VIEW3D – Exported Operations drawComplexVectorField: (C -> C,S,S) ->
VIEW3D setRealSteps: INT -> INT setImagSteps: INT -> INT setClipValue:
DFLOAT-> DFLOAT

Implementation == add – Implementation part begins. arrowScale : DFLOAT
:= (0.2)::DFLOAT – (relative size) Local variable 1. arrowAngle : DFLOAT
:= pi()-pi()/(20::DFLOAT) – Local variable 2. realSteps : INT := 11 –
(real steps) Local variable 3. imagSteps : INT := 11 – (imaginary steps)
Local variable 4. clipValue : DFLOAT := 10::DFLOAT – (maximum vector
length) Local variable 5.

setRealSteps(n) == realSteps := n – Exported function definition 1.
setImagSteps(n) == imagSteps := n – Exported function definition 2.
setClipValue(c) == clipValue := c – Exported function definition 3.

clipFun: DFLOAT -> DFLOAT – Clip large magnitudes. clipFun(x) ==
min(max(x, -clipValue), clipValue) – Local function definition 1.

makeArrow: (Point DFLOAT,Point DFLOAT,DFLOAT,DFLOAT) -> CURVE
makeArrow(p1, p2, len, arg) == ... – Local function definition 2.

drawComplex(f, realRange, imagRange, arrows?) == ... – Exported function
definition 4.

Names, Abbreviations, and File Structure
----------------------------------------

ugPackagesNames Each package has a name and an abbreviation. For a
package of the complex draw functions from , we choose the name and
abbreviation . [1]_ To be sure that you have not chosen a name or
abbreviation already used by the system, issue the system command for
both the name and the abbreviation.

Once you have named the package and its abbreviation, you can choose any
new filename you like with extension “****” to hold the definition of
your package. We choose the name **drawpak**. If your application
involves more than one package, you can put them all in the same file.
FriCAS assumes no relationship between the name of a library file, and
the name or abbreviation of a package.

Near the top of the “****” file, list all the abbreviations for the
packages using , each command beginning in column one. Macros giving
names to FriCAS expressions can also be placed near the top of the file.
The macros are only usable from their point of definition until the end
of the file.

Consider the definition of in Figure [fig-pak-cdraw]. After the macro
definition

::

    S      ==> Segment DoubleFloat

the name S can be used in the file as a shorthand for . [2]_ The
abbreviation command for the package

::

    )abbrev package DRAWCX DrawComplex

is given after the macros (although it could precede them).

Syntax
------

ugPackagesSyntax The definition of a package has the syntax:

The syntax for defining a package constructor is the same as that for
defining any function in FriCAS. In practice, the definition extends
over many lines so that this syntax is not practical. Also, the type of
a package is expressed by the operator followed by an explicit list of
operations. A preferable way to write the definition of a package is
with a expression:

The definition of a package usually has the form: The package takes no
parameters and exports five operations, each a separate item of a . Each
operation is described as a : a name, followed by a colon (), followed
by the type of the operation. All operations have types expressed as
with the syntax

*sourcetarget*

Abstract Datatypes
------------------

ugPackagesAbstract A constructor as defined in FriCAS is called an in
the computer science literature. Abstract datatypes separate
“specification” (what operations are provided) from “implementation”
(how the operations are implemented). The Exports (specification) part
of a constructor is said to be “public” (it provides the user interface
to the package) whereas the Implementation part is “private”
(information here is effectively hidden—programs cannot take advantage
of it).

The Exports part specifies what operations the package provides to
users. As an author of a package, you must ensure that the
Implementation part provides a function for each operation in the
Exports part. [3]_

An important difference between interactive programming and the use of
packages is in the handling of global variables such as and . In
interactive programming, you simply change the values of variables by .
With packages, such variables are local to the package—their values can
only be set using functions exported by the package. In our example
package, we provide two functions and for this purpose.

Another local variable is which can be changed using the exported
operation . This value is referenced by the internal function that
decides whether to use the computed value of the function at a point or,
if the magnitude of that value is too large, the value assigned to (with
the appropriate sign).

Capsules
--------

ugPackagesCapsules The part to the right of add in the Implementation
part of the definition is called a . The purpose of a capsule is:

-  to define a function for each exported operation, and

-  to define a for these functions to run.

What is a local environment? First, what is an environment? Think of the
capsule as an input file that FriCAS reads from top to bottom. Think of
the input file as having a at the top so that initially no variables or
functions are defined. When this file is read, variables such as and in
are set to initial values. Also, all the functions defined in the
capsule are compiled. These include those that are exported (like ), and
those that are not (like ). At the end, you get a set of name-value
pairs: variable names (like and ) are paired with assigned values, while
operation names (like and ) are paired with function values.

This set of name-value pairs is called an . Actually, we call this
environment the “initial environment” of a package: it is the
environment that exists immediately after the package is first built.
Afterwards, functions of this capsule can access or reset a variable in
the environment. The environment is called *local* since any changes to
the value of a variable in this environment can be seen *only* by these
functions.

Only the functions from the package can change the variables in the
local environment. When two functions are called successively from a
package, any changes caused by the first function called are seen by the
second.

Since the environment is local to the package, its names don’t get mixed
up with others in the system or your workspace. If you happen to have a
variable called in your workspace, it does not affect what the functions
do in any way.

The functions in a package are compiled into machine code. Unlike
function definitions in input files that may be compiled repeatedly as
you use them with varying argument types, functions in packages have a
unique type (generally parameterized by the argument parameters of a
package) and a unique compilation residing on disk.

The capsule itself is turned into a compiled function. This so-called
*capsule function* is what builds the initial environment spoken of
above. If the package has arguments (see below), then each call to the
package constructor with a distinct pair of arguments builds a distinct
package, each with its own local environment.

Input Files vs. Packages
------------------------

ugPackagesInputFiles A good question at this point would be “Is writing
a package more difficult than writing an input file?”

The programs in input files are designed for flexibility and
ease-of-use. FriCAS can usually work out all of your types as it reads
your program and does the computations you request. Let’s say that you
define a one-argument function without giving its type. When you first
apply the function to a value, this value is understood by FriCAS as
identifying the type for the argument parameter. Most of the time FriCAS
goes through the body of your function and figures out the target type
that you have in mind. FriCAS sometimes fails to get it right. Then—and
only then—do you need a declaration to tell FriCAS what type you want.

Input files are usually written to be read by FriCAS—and by you. Without
suitable documentation and declarations, your input files are likely
incomprehensible to a colleague—and to you some months later!

Packages are designed for legibility, as well as run-time efficiency.
There are few new concepts you need to learn to write packages. Rather,
you just have to be explicit about types and type conversions. The types
of all functions are pre-declared so that FriCAS—and the reader— knows
precisely what types of arguments can be passed to and from the
functions (certainly you don’t want a colleague to guess or to have to
work this out from context!). The types of local variables are also
declared. Type conversions are explicit, never automatic. [4]_

In summary, packages are more tedious to write than input files. When
writing input files, you can casually go ahead, giving some facts now,
leaving others for later. Writing packages requires forethought, care
and discipline.

Compiling Packages
------------------

ugPackagesPackages Once you have defined the package , you need to
compile and test it. To compile the package, issue the system command .
FriCAS reads the file **drawpak** and compiles its contents into machine
binary. If all goes well, the file **DRAWCX.NRLIB** is created in your
local directory for the package. To test the package, you must load the
package before trying an operation.

Compile the package.

::

    )compile drawpak

Expose the package.

::

    )expose DRAWCX 

DrawComplex is now explicitly exposed in frame initial

Use an odd step size to avoid a pole at the origin.

::

    setRealSteps 51 

.. math:: 51

``PositiveInteger``

::

    setImagSteps 51 

.. math:: 51

``PositiveInteger``

Define to be the Gamma function.

::

    f(z) == Gamma(z) 

Clip values of function with magnitude larger than 7.

::

    setClipValue 7

.. math:: {\texttt{7.0}}

``DoubleFloat``

Draw the function.

::

    drawComplex(f,-pi()..pi(),-pi()..pi(), false) 

|image|

Parameters
----------

ugPackagesParameters The power of packages becomes evident when packages
have parameters. Usually these parameters are domains and the exported
operations have types involving these parameters.

In , you learned that categories denote classes of domains. Although we
cover this notion in detail in the next chapter, we now give you a sneak
preview of its usefulness.

In , we defined functions and to sort a list of integers. If you look at
the code for these functions, you see that they may be used to sort
*any* structure with the right properties. Also, the functions can be
used to sort lists of *any* elements—not just integers. Let us now
recall the code for .

::

    bubbleSort(m) ==
      n := #m
      for i in 1..(n-1) repeat
        for j in n..(i+1) by -1 repeat
          if m.j < m.(j-1) then swap!(m,j,j-1)
      m

What properties of “lists of integers” are assumed by the sorting
algorithm? In the first line, the operation computes the maximum index
of the list. The first obvious property is that must have a finite
number of elements. In FriCAS, this is done by your telling FriCAS that
has the “attribute” . An is a property that a domain either has or does
not have. As we show later in , programs can query domains as to the
presence or absence of an attribute.

The operation swaps elements of . Using , you find that requires its
elements to come from a domain of category with attribute . This
attribute means that you can change the internal components of without
changing its external structure. Shallowly-mutable data structures
include lists, streams, one- and two-dimensional arrays, vectors, and
matrices.

The category designates the class of aggregates whose elements can be
accessed by the notation for suitable selectors . The category takes two
arguments: , a domain of selectors for the aggregate, and , a domain of
entries for the aggregate. Since the sort functions access elements by
integers, we must choose . The most general class of domains for which
and are defined are those of category with the two attributes and .

Using , you can also discover that FriCAS has many kinds of domains with
attribute . Those of class include , , , , , and , and also and with
integer keys. Although you may never want to sort all such structures,
we nonetheless demonstrate FriCAS’s ability to do so.

Another requirement is that has an operation . One way to get this
operation is to assume that has category . By definition, will then
export a operation. A more general approach is to allow any comparison
function to be used for sorting. This function will be passed as an
argument to the sorting functions.

Our sorting package then takes two arguments: a domain of objects of
*any* type, and a domain , an aggregate of type with the above two
attributes. Here is its definition using what are close to the original
definitions of and for sorting lists of integers. The symbol is added to
the ends of the operation names. This uniform naming convention is used
for FriCAS operation names that destructively change one or more of
their arguments.

SortPackage(S,A) : Exports == Implementation where S: Object A:
IndexedAggregate(Integer,S) with (finiteAggregate; shallowlyMutable)

Exports == with bubbleSort!: (A,(S,S) -> Boolean) -> A insertionSort!:
(A, (S,S) -> Boolean) -> A

Implementation == add bubbleSort!(m,f) == n := #m for i in 1..(n-1)
repeat for j in n..(i+1) by -1 repeat if f(m.j,m.(j-1)) then
swap!(m,j,j-1) m insertionSort!(m,f) == for i in 2..#m repeat j := i
while j > 1 and f(m.j,m.(j-1)) repeat swap!(m,j,j-1) j := (j - 1)
pretend PositiveInteger m

Conditionals
------------

ugPackagesConds When packages have parameters, you can say that an
operation is or is not exported depending on the values of those
parameters. When the domain of objects has an operation, we can supply
one-argument versions of and which use this operation for sorting. The
presence of the operation is guaranteed when is an ordered set.

Exports == with bubbleSort!: (A,(S,S) -> Boolean) -> A insertionSort!:
(A, (S,S) -> Boolean) -> A

if S has OrderedSet then bubbleSort!: A -> A insertionSort!: A -> A

In addition to exporting the one-argument sort operations conditionally,
we must provide conditional definitions for the operations in the
Implementation part. This is easy: just have the one-argument functions
call the corresponding two-argument functions with the operation from .

Implementation == add ... if S has OrderedSet then bubbleSort!(m) ==
bubbleSort!(m,<\ :math:`S)
      insertionSort!(m) == insertionSort!(m,<`\ S)

In , we give an alternative definition of using and that is more
efficient for a list (for which access to any element requires
traversing the list from its first node). To implement a more efficient
algorithm for lists, we need the operation which allows us to
destructively change the and of a list. Using , you find that these
operations come from category . Several aggregate types are unary
recursive aggregates including those of and . We provide two different
implementations for and : one for list-like structures, another for
array-like structures.

Implementation == add ... if A has UnaryRecursiveAggregate(S) then
bubbleSort!(m,fn) == empty? m => m l := m while not empty? (r := l.rest)
repeat r := bubbleSort! r x := l.first if fn(r.first,x) then l.first :=
r.first r.first := x l.rest := r l := l.rest m insertionSort!(m,fn) ==
...

The ordering of definitions is important. The standard definitions come
first and then the predicate

::

    A has UnaryRecursiveAggregate(S)

is evaluated. If true, the special definitions cover up the standard
ones.

Another equivalent way to write the capsule is to use an expression:

if A has UnaryRecursiveAggregate(S) then ... else ...

Testing
-------

ugPackagesCompiling Once you have written the package, embed it in a
file, for example, **sortpak**. Be sure to include an command at the top
of the file:

::

    )abbrev package SORTPAK SortPackage

Now compile the file (using ).

Expose the constructor. You are then ready to begin testing.

::

    )expose SORTPAK

SortPackage is now explicitly exposed in frame initial

Define a list.

::

    l := [1,7,4,2,11,-7,3,2]

.. math:: {{\left \lbrack {}1{,\:}7{,\:}4{,\:}2{,\:}11{,\:}-{7}{,\:}3{,\:}2 \right \rbrack {}}}

``List(Integer)``

Since the integers are an ordered set, a one-argument operation will do.

::

    bubbleSort!(l)

.. math:: {{\left \lbrack {}-{7}{,\:}1{,\:}2{,\:}2{,\:}3{,\:}4{,\:}7{,\:}11 \right \rbrack {}}}

``List(Integer)``

Re-sort it using “greater than.”

::

    bubbleSort!(l,(x,y) +-> x > y)

.. math:: {{\left \lbrack {}11{,\:}7{,\:}4{,\:}3{,\:}2{,\:}2{,\:}1{,\:}-{7} \right \rbrack {}}}

``List(Integer)``

Now sort it again using on integers.

::

    bubbleSort!(l, <$Integer)

.. math:: {{\left \lbrack {}-{7}{,\:}1{,\:}2{,\:}2{,\:}3{,\:}4{,\:}7{,\:}11 \right \rbrack {}}}

``List(Integer)``

A string is an aggregate of characters so we can sort them as well.

::

    bubbleSort! "Mathematical Sciences"

.. math:: {\texttt{"\ MSaaaccceeehiilmnstt"}}

``String``

Is defined on booleans?

::

    false < true

.. math:: {\texttt{true}}

``Boolean``

Good! Create a bit string representing ten consecutive boolean values .

::

    u : Bits := new(10,true)

.. math:: {\texttt{"1111111111"}}

``Bits``

Set bits 3 through 5 to , then display the result.

::

    u(3..5) := false; u

.. math:: {\texttt{"1100011111"}}

``Bits``

Now sort these booleans.

::

    bubbleSort! u

.. math:: {\texttt{"0001111111"}}

``Bits``

Create an “eq-table” (see ), a table having integers as keys and strings
as values.

::

    t : EqTable(Integer,String) := table()

.. math:: {\texttt{table}}{{\left ( {}\right ) {}}}

``EqTable(Integer, String)``

Give the table a first entry.

::

    t.1 := "robert"

.. math:: {\texttt{"robert"}}

``String``

And a second.

::

    t.2 := "richard"

.. math:: {\texttt{"richard"}}

``String``

What does the table look like?

::

    t

.. math:: {\texttt{table}}{{\left ( {}2={\texttt{"richard"}},1={\texttt{"robert"}} \right ) {}}}

``EqTable(Integer, String)``

Now sort it.

::

    bubbleSort! t

.. math:: {\texttt{table}}{{\left ( {}2={\texttt{"robert"}},1={\texttt{"richard"}} \right ) {}}}

``EqTable(Integer, String)``

How Packages Work
-----------------

ugPackagesHow Recall that packages as abstract datatypes are compiled
independently and put into the library. The curious reader may ask: “How
is the interpreter able to find an operation such as ? Also, how is a
single compiled function such as able to sort data of different types?”

After the interpreter loads the package , the four operations from the
package become known to the interpreter. Each of these operations is
expressed as a *modemap* in which the type of the operation is written
in terms of symbolic domains.

::

    )expose SORTPAK

SortPackage is already explicitly exposed in frame initial

See the modemaps for .

::

    )display op bubbleSort!

There are 2 exposed functions called bubbleSort! : [1] (D1,((D3,D3) ->
Boolean)) -> D1 from SortPackage(D3,D1) if D3 has TYPE and D1 has
Join(IXAGG(INT,D3),ATFINAG, ATSHMUT) [2] D1 -> D1 from
SortPackage(D2,D1) if D2 has ORDSET and D2 has TYPE and D1 has
Join(IXAGG(INT, D2),ATFINAG,ATSHMUT)

What happens if you ask for ? There is a unique modemap for an operation
named with one argument. Since is a list of integers, the symbolic
domain is defined as . For some operation to apply, it must satisfy the
predicate for some . What ? The third expression of the requires D1 has
IndexedAggregate(Integer, D2) with two attributes. So the interpreter
searches for an among the ancestors of (see ). It finds one: . The
interpreter tries defining as . After substituting for and , the
predicate evaluates to . An applicable operation has been found!

Now FriCAS builds the package . According to its definition, this
package exports the required operation: : . The interpreter then asks
the package for a function implementing this operation. The package gets
all the functions it needs (for example, and ) from the appropriate
domains and then it returns a to the interpreter together with the local
environment for . The interpreter applies the function to the argument .
The function is executed in its local environment and produces the
result.

.. [1]
   An abbreviation can be any string of between two and seven capital
   letters and digits, beginning with a letter. See for more
   information.

.. [2]
   The interpreter also allows macro for macro definitions.

.. [3]
   The package enhances the facility described in by allowing a complex
   function to have arrows emanating from the surface to indicate the
   direction of the complex argument.

.. [4]
   There is one exception to this rule: conversions from a subdomain to
   a domain are automatic. After all, the objects both have the domain
   as a common type.

.. |image| image:: ../png/bessintr.png

