#!/bin/bash
# convert/make
cd ./tex
cp book.tex ../sphinx -v
cp fricasrst.sty ../sphinx -v
cp test.tex ../sphinx -v
awk -f pand.awk ug00.tex > ../sphinx/ug00.tex 
awk -f pand.awk ug01.tex > ../sphinx/ug01.tex
awk -f pand.awk tecintro.tex > ../sphinx/tecintro.tex
cd ../sphinx
pandoc --mathjax -f latex -t rst book.tex > book.rst
pandoc --mathjax -f latex -t rst test.tex > test.rst 
sed -i '1iFriCAS Book' book.rst # we need a title
sed -i '2i===========' book.rst
make html
