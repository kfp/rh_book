Dedicated to Calvin and Hobbes.

Preface
=======

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus
ante, auctor et pulvinar non, posuere ac lacus. Praesent egestas nisi id
metus rhoncus ac lobortis sem hendrerit. Etiam et sapien eget lectus
interdum posuere sit amet ac urna.

Un-numbered sample section
--------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus
ante, auctor et pulvinar non, posuere ac lacus. Praesent egestas nisi id
metus rhoncus ac lobortis sem hendrerit. Etiam et sapien eget lectus
interdum posuere sit amet ac urna. Aliquam pellentesque imperdiet erat,
eget consectetur felis malesuada quis. Pellentesque sollicitudin, odio
sed dapibus eleifend, magna sem luctus turpis.

Another sample section
----------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus
ante, auctor et pulvinar non, posuere ac lacus. Praesent egestas nisi id
metus rhoncus ac lobortis sem hendrerit. Etiam et sapien eget lectus
interdum posuere sit amet ac urna. Aliquam pellentesque imperdiet erat,
eget consectetur felis malesuada quis. Pellentesque sollicitudin, odio
sed dapibus eleifend, magna sem luctus turpis, id aliquam felis dolor eu
diam. Etiam ullamcorper, nunc a accumsan adipiscing, turpis odio
bibendum erat, id convallis magna eros nec metus.

Structure of book
-----------------

Each unit will focus on <SOMETHING>.

About the companion website
---------------------------

The website [3]_ for this file contains:

-  A link to (freely downlodable) latest version of this document.

-  Link to download LaTeX source for this document.

-  Miscellaneous material (e.g. suggested readings etc).

Acknowledgements
----------------

-  A special word of thanks goes to Professor Don Knuth [4]_ (for TeX)
   and Leslie Lamport [5]_ (for LaTeX).

-  I’ll also like to thank Gummi [6]_ developers and LaTeXila [7]_
   development team for their awesome LaTeX editors.

-  I’m deeply indebted my parents, colleagues and friends for their
   support and encouragement.

| 
| Amber Jain
| http://amberj.devio.us/

Introductory Chapter
====================

Author’s name, *Source of this quote* “This is a quote and I don’t know
who said this.”

Section heading
---------------

| Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
  eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
  minim veniam, quis nostrud exercitation ullamco laboris nisi ut
  aliquip ex ea commodo consequat.
| Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
  dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
  proident, sunt in culpa qui officia deserunt mollit anim id est
  laborum.
| Lorem ipsum list:

1. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

2. Duis ac mi magna, a consectetur elit.

3. Curabitur posuere erat *dignissim ligula euismod* ut euismod nisi.

4. Fusce vulputate facilisis neque, et ornare mauris mattis vel.

5. Mauris sit amet nulla mi, vitae rutrum ante.

6. Maecenas quis nulla risus, vel tincidunt ligula.

7. Nullam ac enim neque, non *dapibus* mauris.

8. Integer volutpat leo a orci suscipit eget rhoncus urna eleifend.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus
ante, auctor et pulvinar non, posuere ac lacus. Praesent egestas nisi id
metus rhoncus ac lobortis sem hendrerit. Etiam et sapien eget lectus
interdum posuere sit amet ac urna [8]_:

Lorem ipsum dolor sit amet, consectetur adipiscing elit.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus
ante, auctor et pulvinar non, posuere ac lacus. Praesent egestas nisi id
metus rhoncus ac lobortis sem hendrerit. Etiam et sapien eget lectus
interdum posuere sit amet ac urna. Aliquam pellentesque imperdiet erat,
eget consectetur felis malesuada quis. Pellentesque sollicitudin, odio
sed dapibus eleifend, magna sem luctus turpis, id aliquam felis dolor eu
diam. Etiam ullamcorper, nunc a accumsan adipiscing, turpis odio
bibendum erat, id convallis magna eros nec metus. Sed vel ligula justo,
sit amet vestibulum dolor. Sed vitae augue sit amet magna ullamcorper
suscipit. Quisque dictum ipsum a sapien egestas facilisis.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus
ante, auctor et pulvinar non, posuere ac lacus. Praesent egestas nisi id
metus rhoncus ac lobortis sem hendrerit. Etiam et sapien eget lectus
interdum posuere sit amet ac urna.

Lorem ipsum dolor sit amet, consectetur adipiscing.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus
ante, auctor et pulvinar non, posuere ac lacus. Praesent egestas nisi id
metus rhoncus ac lobortis sem hendrerit. Etiam et sapien eget lectus
interdum posuere sit amet ac urna. Aliquam pellentesque imperdiet erat,
eget consectetur felis malesuada quis. Pellentesque sollicitudin, odio
sed dapibus eleifend, magna sem luctus turpis, id aliquam felis dolor eu
diam.

Lorem ipsum dolor sit amet
~~~~~~~~~~~~~~~~~~~~~~~~~~

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus
ante, auctor et pulvinar non, posuere ac lacus. Praesent egestas nisi id
metus rhoncus ac lobortis sem hendrerit. Etiam et sapien eget lectus
interdum posuere sit amet ac urna. Aliquam pellentesque imperdiet [9]_
erat, eget consectetur felis malesuada quis. Pellentesque sollicitudin,
odio sed dapibus eleifend, magna sem luctus turpis, id aliquam felis
dolor eu diam. Etiam ullamcorper, nunc a accumsan adipiscing, turpis
odio bibendum erat, id convallis magna eros nec metus. Sed vel ligula
justo, sit amet vestibulum dolor. Sed vitae augue sit amet magna
ullamcorper suscipit. Quisque dictum ipsum a sapien egestas facilisis.

In hac habitasse platea dictumst. Nullam turpis erat, porttitor ut
pretium ac, condimentum sed dui. Praesent arcu elit, tristique sit amet
viverra at, auctor quis tortor. Etiam eleifend posuere aliquam. Donec
sed mattis sapien. Aenean urna arcu, suscipit at rutrum ac, adipiscing
ac felis. Class aptent taciti sociosqu ad litora torquent per conubia
nostra, per inceptos himenaeos. Praesent condimentum felis a ipsum
ullamcorper semper. Vivamus eu odio sem, dictum luctus nunc. Etiam
tincidunt venenatis dolor non pellentesque.

Lorem ipsum dolor sit amet, auctor et pulvinar non
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus
ante, auctor et pulvinar non, posuere ac lacus. Praesent egestas nisi id
metus rhoncus ac lobortis sem hendrerit. Etiam et sapien eget lectus
interdum posuere sit amet ac urna. Aliquam pellentesque imperdiet erat,
eget consectetur felis malesuada quis. Pellentesque sollicitudin, odio
sed dapibus eleifend, magna sem luctus turpis, id aliquam felis dolor eu
diam. Etiam ullamcorper, nunc a accumsan adipiscing, turpis odio
bibendum erat, id convallis magna eros nec metus. Sed vel ligula justo,
sit amet vestibulum dolor. Sed vitae augue sit amet magna ullamcorper
suscipit. Quisque dictum ipsum a sapien egestas facilisis.

In hac habitasse platea dictumst. Nullam turpis erat, porttitor ut
pretium ac, condimentum sed dui. Praesent arcu elit, tristique sit amet
viverra at, auctor quis tortor. Etiam eleifend posuere aliquam. Donec
sed mattis sapien. Aenean urna arcu, suscipit at rutrum ac, adipiscing
ac felis. Class aptent taciti sociosqu ad litora torquent per conubia
nostra, per inceptos himenaeos. Praesent condimentum felis a ipsum
ullamcorper semper. Vivamus eu odio sem, dictum luctus nunc. Etiam
tincidunt venenatis dolor non pellentesque.

Another section heading
-----------------------

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
commodo consequat.

+-------------+------------+------------+------------+
| S. No.      | Column#1   | Column#2   | Column#3   |
+-------------+------------+------------+------------+
| [0.5ex] 1   | 50         | 837        | 970        |
+-------------+------------+------------+------------+
| 2           | 47         | 877        | 230        |
+-------------+------------+------------+------------+
| 3           | 31         | 25         | 415        |
+-------------+------------+------------+------------+
| 4           | 35         | 144        | 2356       |
+-------------+------------+------------+------------+
| 5           | 45         | 300        | 556        |
+-------------+------------+------------+------------+
| [1ex]       |            |            |            |
+-------------+------------+------------+------------+

Table: Sample table

[table:nonlin]

| Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
  dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
  proident, sunt in culpa qui officia deserunt mollit anim id est
  laborum.
| Lorem ipsum list:

-  Mauris sit amet nulla mi, vitae rutrum ante.

-  Maecenas quis nulla risus, vel tincidunt ligula.

-  Nullam ac enim neque, non *dapibus* mauris.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus
ante, auctor et pulvinar non, posuere ac lacus. Praesent egestas nisi id
metus rhoncus ac lobortis sem hendrerit. Etiam et sapien eget lectus
interdum posuere sit amet ac urna [10]_:

Lorem ipsum dolor sit amet, consectetur adipiscing elit.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus
ante, auctor et pulvinar non, posuere ac lacus. Praesent egestas nisi id
metus rhoncus ac lobortis sem hendrerit. Etiam et sapien eget lectus
interdum posuere sit amet ac urna. Aliquam pellentesque imperdiet erat,
eget consectetur felis malesuada quis. Pellentesque sollicitudin, odio
sed dapibus eleifend, magna sem luctus turpis, id aliquam felis dolor eu
diam. Etiam ullamcorper, nunc a accumsan adipiscing, turpis odio
bibendum erat, id convallis magna eros nec metus. Sed vel ligula justo,
sit amet vestibulum dolor. Sed vitae augue sit amet magna ullamcorper
suscipit. Quisque dictum ipsum a sapien egestas facilisis.

Lorem ipsum dolor sit amet, consectetur adipiscing
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus
ante, auctor et pulvinar non, posuere ac lacus. Praesent egestas nisi id
metus rhoncus ac lobortis sem hendrerit. Etiam et sapien eget lectus
interdum posuere sit amet ac urna. Aliquam pellentesque imperdiet erat,
eget consectetur felis malesuada quis. Pellentesque sollicitudin, odio
sed dapibus eleifend, magna sem luctus turpis, id aliquam felis dolor eu
diam.

Math Tests
----------

Test 1: ``begin-equation``

.. math:: - \frac{\hbar^2}{2m} \Delta\Psi + V(x) \Psi = E \Psi

Test 2: ``begin-equation-*``

.. math:: (\alpha+\beta)^n + \frac{1+x+x^2+x^3+x^4}{y+\pi}

.. [1]
   This is a footnote.

.. [2]
   This is yet another footnote.

.. [3]
   https://github.com/amberj/latex-book-template

.. [4]
   http://www-cs-faculty.stanford.edu/~uno/

.. [5]
   http://www.lamport.org/

.. [6]
   http://gummi.midnightcoding.org/

.. [7]
   http://projects.gnome.org/latexila/

.. [8]
   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus
   ante, auctor et pulvinar non, posuere ac lacus.

.. [9]
   `www.example.com <www.example.com>`__

.. [10]
   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus
   ante, auctor et pulvinar non, posuere ac lacus.
