.. FriCAS Book documentation master file, created by
   sphinx-quickstart on Mon Mar 20 18:21:47 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FriCAS Book's documentation!
=======================================

Contents:

.. toctree::
   :maxdepth: 2

   book
   test

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

