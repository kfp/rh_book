FriCAS Book
===========
    This is an attempt to make the content of the Axiom book (by Jenks
    and Sutor) available again in order to describe the FriCAS project
    (which is a fork of the original Axiom code). The following material
    is mainly taken from the original Axiom book, but is partly tailored
    to new developments in FriCAS.

    **WARNING: This is work-in-progress! There might be errors and even
    false statements.**

    Plan is as follows:

    #. Make LaTeX compilation work and include (generated) pictures and
       generated output of algebra commands.

    #. Simplify and remove redundant LaTeX commands.

    #. Build on modern advanced latex packages.

    #. Generate indices and hyperrefs.

    All of the above will be done by taking care of still generating the
    same ``.ht`` files although the technicalities might look different.
    The contents of the files (no matter how wrong it actually is) will
    only be changed marginally. Rewriting of the contents will only
    happen after the technical part of producing the PDF file has been
    finished.

    Ralf Hemmecke

Welcome to the world of FriCAS. We call FriCAS a scientific computation
system: a self-contained toolbox designed to meet your scientific
programming needs, from symbolics, to numerics, to graphics.

This introduction is a quick overview of what FriCAS offers.

FriCAS provides a wide range of simple commands for symbolic
mathematical problem solving. Do you need to solve an equation, to
expand a series, or to obtain an integral? If so, just ask FriCAS to do
it.

Integrate :math:`\frac{1}{(x^3 \  {(a+b x)}^{1/3})}` with respect to .

::

    integrate(1/(x^3 * (a+b*x)^(1/3)),x)

.. math:: \frac{-{2{\:}{{{{b}}}^{2}}{\:}{{{{x}}}^{2}}{\:}\sqrt{3}{\:}\log{{{\left ( {}{\sqrt[3]{{{a}}}}{\:}{{{\sqrt[3]{{{b}}{\:}{{x}}+{{a}}}}}^{2}}+{{{\sqrt[3]{{{a}}}}}^{2}}{\:}{\sqrt[3]{{{b}}{\:}{{x}}+{{a}}}}+{{a}} \right ) {}}}}}+4{\:}{{{{b}}}^{2}}{\:}{{{{x}}}^{2}}{\:}\sqrt{3}{\:}\log{{{\left ( {}{{{\sqrt[3]{{{a}}}}}^{2}}{\:}{\sqrt[3]{{{b}}{\:}{{x}}+{{a}}}}-{{{a}}} \right ) {}}}}+12{\:}{{{{b}}}^{2}}{\:}{{{{x}}}^{2}}{\:}\arctan{{{\left ( {}\frac{2{\:}\sqrt{3}{\:}{{{\sqrt[3]{{{a}}}}}^{2}}{\:}{\sqrt[3]{{{b}}{\:}{{x}}+{{a}}}}+{{a}}{\:}\sqrt{3}}{3{\:}{{a}}} \right ) {}}}}+{{\left ( {}12{\:}{{b}}{\:}{{x}}-{9{\:}{{a}}} \right ) {}}}{\:}\sqrt{3}{\:}{\sqrt[3]{{{a}}}}{\:}{{{\sqrt[3]{{{b}}{\:}{{x}}+{{a}}}}}^{2}}}{18{\:}{{{{a}}}^{2}}{\:}{{{{x}}}^{2}}{\:}\sqrt{3}{\:}{\sqrt[3]{{{a}}}}}

``Union(Expression(Integer), ...)``

FriCAS provides state-of-the-art algebraic machinery to handle your most
advanced symbolic problems. For example, FriCAS’s integrator gives you
the answer when an answer exists. If one does not, it provides a proof
that there is no answer. Integration is just one of a multitude of
symbolic operations that FriCAS provides.

FriCAS has a numerical library that includes operations for linear
algebra, solution of equations, and special functions. For many of these
operations, you can select any number of floating point digits to be
carried out in the computation.

Solve :math:`x^{49}-49x^4+9` to 49 digits of accuracy.

::

    solve(x^49-49*x^4+9 = 0,1.e-49)

.. math:: {{\left \lbrack {}{{x}}=-{{\texttt{0.65465367069042711367}}}{,\:}{{x}}={\texttt{1.0869213956538595085}}{,\:}{{x}}={\texttt{0.65465367072552717397}} \right \rbrack {}}}

``List(Equation(Polynomial(Float)))``

The output of a computation can be converted to FORTRAN to be used in a
later numerical computation. Besides floating point numbers, FriCAS
provides literally dozens of kinds of numbers to compute with. These
range from various kinds of integers, to fractions, complex numbers,
quaternions, continued fractions, and to numbers represented with an
arbitrary base.

What is to the power in base ?

::

    radix(10^100,32)

.. math:: 4{\texttt{I}}9{\texttt{L}}{\texttt{K}}{\texttt{I}}{\texttt{P}}9{\texttt{G}}{\texttt{R}}{\texttt{S}}{\texttt{T}}{\texttt{C}}5{\texttt{I}}{\texttt{F}}164{\texttt{P}}{\texttt{O}}5{\texttt{V}}72{\texttt{M}}{\texttt{E}}827226{\texttt{J}}{\texttt{S}}{\texttt{L}}{\texttt{A}}{\texttt{P}}462585{\texttt{Q}}7{\texttt{H}}00000000000000000000

``RadixExpansion(32)``

You may often want to visualize a symbolic formula or draw a graph from
a set of numerical values. To do this, you can call upon the FriCAS
graphics capability.

Draw :math:`J_0(\sqrt{x^2+y^2})` for :math:`-20 \leq x,y \leq 20`.

::

    draw(5*besselJ(0,sqrt(x^2+y^2)), x=-20..20, y=-20..20)

Graphs in FriCAS are interactive objects you can manipulate with your
mouse. Just click on the graph, and a control panel pops up. Using this
mouse and the control panel, you can translate, rotate, zoom, change the
coloring, lighting, shading, and perspective on the picture. You can
also generate a PostScript copy of your graph to produce hard-copy
output.

presents you windows on the world of FriCAS, offering on-line help,
examples, tutorials, a browser, and reference material. gives you
on-line access to this book in a “hypertext” format. Words that appear
in a different font (for example, , , and ) are generally mouse-active;
if you click on one with your mouse, shows you a new window for that
word.

As another example of a facility, suppose that you want to compute the
roots of :math:`x^{49} - 49x^4 + 9` to digits (as in our previous
example) and you don’t know how to tell FriCAS to do this. The “basic
command” facility of leads the way. Through the series of windows shown
in Figure [fig-intro-br] and the specified mouse clicks, you and
generate the correct command to issue to compute the answer.

.5

FriCAS’s interactive programming language lets you define your own
functions. A simple example of a user-defined function is one that
computes the successive Legendre polynomials. FriCAS lets you define
these polynomials in a piece-wise way.

The first Legendre polynomial.

::

    p(0) == 1

The second Legendre polynomial.

::

    p(1) == x

The Legendre polynomial for :math:`(n > 1)`.

::

    p(n) == ((2*n-1)*x*p(n-1) - (n-1) * p(n-2))/n

In addition to letting you define simple functions like this, the
interactive language can be used to create entire application packages.
All the graphs in the section in the center of the book, for example,
were created by programs written in the interactive language.

The above definitions for do no computation—they simply tell FriCAS how
to compute for some positive integer . To actually get a value of a
Legendre polynomial, you ask for it.

What is the tenth Legendre polynomial?

::

    p(10)

Compiling function p with type Integer -> Polynomial(Fraction( Integer))

Compiling function p as a recurrence relation.

.. math:: \frac{46189}{256}{\:}{{{{x}}}^{10}}-{\frac{109395}{256}{\:}{{{{x}}}^{8}}}+\frac{45045}{128}{\:}{{{{x}}}^{6}}-{\frac{15015}{128}{\:}{{{{x}}}^{4}}}+\frac{3465}{256}{\:}{{{{x}}}^{2}}-{\frac{63}{256}}

``Polynomial(Fraction(Integer))``

FriCAS applies the above pieces for to obtain the value of . But it does
more: it creates an optimized, compiled function for . The function is
formed by putting the pieces together into a single piece of code. By
*compiled*, we mean that the function is translated into basic
machine-code. By *optimized*, we mean that certain transformations are
performed on that code to make it run faster. For , FriCAS actually
translates the original definition that is recursive (one that calls
itself) to one that is iterative (one that consists of a simple loop).

What is the coefficient of in ?

::

    coefficient(p(90),x,90)

.. math:: \frac{56882655420520178222 23458237426581853561 497449095175}{77371252455336267181 195264}

``Polynomial(Fraction(Integer))``

In general, a user function is type-analyzed and compiled on first use.
Later, if you use it with a different kind of object, the function is
recompiled if necessary.

A variety of data structures are available for interactive use. These
include strings, lists, vectors, sets, multisets, and hash tables. A
particularly useful structure for interactive use is the infinite
stream:

Create the infinite stream of derivatives of Legendre polynomials

::

    [D(p(i),x) for i in 1..]

There are no library operations named p Use HyperDoc Browse or issue
)what op p to learn if there is any operation containing “ p ” in its
name.

Cannot find a definition or applicable library operation named p with
argument type(s) PositiveInteger

Perhaps you should use “@” to indicate the required return type, or “$”
to specify which version of the function you need.

FriCAS will attempt to step through and interpret the code.

Interpret-Code mode is not supported for stream bodies.

Streams display only a few of their initial elements. Otherwise, they
are “lazy”: they only compute elements when you ask for them.

Data structures are an important component for building application
software. Advanced users can represent data for applications in optimal
fashion. In all, FriCAS offers over forty kinds of aggregate data
structures, ranging from mutable structures (such as cyclic lists and
flexible arrays) to storage efficient structures (such as bit vectors).
As an example, streams are used as the internal data structure for power
series.

What is the series expansion of :math:`\log(\cot(x))` about
:math:`x=\pi/2`?

::

    series(log(cot(x)),x = %pi/2)

.. math:: \log{{{\left ( {}\frac{-{2{\:}{{x}}}+\pi }{2} \right ) {}}}}+\frac{1}{3}{\:}{{{{\left ( {}{{x}}-{\frac{\pi }{2}} \right ) {}}}}^{2}}+\frac{7}{90}{\:}{{{{\left ( {}{{x}}-{\frac{\pi }{2}} \right ) {}}}}^{4}}+\frac{62}{2835}{\:}{{{{\left ( {}{{x}}-{\frac{\pi }{2}} \right ) {}}}}^{6}}+{{\operatorname{O}}}{{\left ( {}{{{{\left ( {}{{x}}-{\frac{\pi }{2}} \right ) {}}}}^{8}} \right ) {}}}

````\ GeneralUnivariatePowerSeries(Expression(Integer), x, %pi/2)

Series and streams make no attempt to compute *all* their elements!
Rather, they stand ready to deliver elements on demand.

What is the coefficient of the term of this series?

::

    coefficient(%,50)

.. math:: \frac{44590788901016030052 44724230085655096564 4}{71314692864386691115 84090881309360354581 359130859375}

``Expression(Integer)``

FriCAS also has many kinds of mathematical structures. These range from
simple ones (like polynomials and matrices) to more esoteric ones (like
ideals and Clifford algebras). Most structures allow the construction of
arbitrarily complicated “types.”

Even a simple input expression can result in a type with several levels.

::

    matrix [[x + %i,0], [1,-2]]

.. math:: \begin{bmatrix}{2}{{x}}+{i}&0\\1&-{2}\end{bmatrix}

``Matrix(Polynomial(Complex(Integer)))``

The FriCAS interpreter builds types in response to user input. Often,
the type of the result is changed in order to be applicable to an
operation.

The inverse operation requires that elements of the above matrices are
fractions.

::

    inverse(%)

.. math:: \begin{bmatrix}{2}\frac{1}{{{x}}+{i}}&0\\\frac{1}{2{\:}{{x}}+2{\:}{i}}&-{\frac{1}{2}}\end{bmatrix}

``Union(Matrix(Fraction(Polynomial(Complex(Integer)))), ...)``

A convenient facility for symbolic computation is “pattern matching.”
Suppose you have a trigonometric expression and you want to transform it
to some equivalent form. Use a command to describe the transformation
rules you need. Then give the rules a name and apply that name as a
function to your trigonometric expression.

Introduce two rewrite rules.

::

    sinCosExpandRules := rule
      sin(x+y) == sin(x)*cos(y) + sin(y)*cos(x)
      cos(x+y) == cos(x)*cos(y) - sin(x)*sin(y)
      sin(2*x) == 2*sin(x)*cos(x)
      cos(2*x) == cos(x)^2 - sin(x)^2

.. math:: {\left\{ \sin{{{\left ( {}{{y}}+{{x}} \right ) {}}}}{{\ ==\ }}\cos{{{x}}}{\:}\sin{{{y}}}+\cos{{{y}}}{\:}\sin{{{x}}}{,\:}\cos{{{\left ( {}{{y}}+{{x}} \right ) {}}}}{{\ ==\ }}-{\sin{{{x}}}{\:}\sin{{{y}}}}+\cos{{{x}}}{\:}\cos{{{y}}}{,\:}\sin{{{\left ( {}2{\:}{{x}} \right ) {}}}}{{\ ==\ }}2{\:}\cos{{{x}}}{\:}\sin{{{x}}}{,\:}\cos{{{\left ( {}2{\:}{{x}} \right ) {}}}}{{\ ==\ }}-{{{{{\left ( {}\sin{{{x}}} \right ) {}}}}^{2}}}+{{{{\left ( {}\cos{{{x}}} \right ) {}}}}^{2}} \right\}}

``Ruleset(Integer, Integer, Expression(Integer))``

Apply the rules to a simple trigonometric expression.

::

    sinCosExpandRules(sin(a+2*b+c))

.. math:: {{\left ( {}-{\cos{{{a}}}{\:}{{{{\left ( {}\sin{{{b}}} \right ) {}}}}^{2}}}-{2{\:}\cos{{{b}}}{\:}\sin{{{a}}}{\:}\sin{{{b}}}}+\cos{{{a}}}{\:}{{{{\left ( {}\cos{{{b}}} \right ) {}}}}^{2}} \right ) {}}}{\:}\sin{{{c}}}-{\cos{{{c}}}{\:}\sin{{{a}}}{\:}{{{{\left ( {}\sin{{{b}}} \right ) {}}}}^{2}}}+2{\:}\cos{{{a}}}{\:}\cos{{{b}}}{\:}\cos{{{c}}}{\:}\sin{{{b}}}+{{{{\left ( {}\cos{{{b}}} \right ) {}}}}^{2}}{\:}\cos{{{c}}}{\:}\sin{{{a}}}

``Expression(Integer)``

Using input files, you can create your own library of transformation
rules relevant to your applications, then selectively apply the rules
you need.

All components of the FriCAS algebra library are written in the FriCAS
library language. This language is similar to the interactive language
except for protocols that authors are obliged to follow. The library
language permits you to write “polymorphic algorithms,” algorithms
defined to work in their most natural settings and over a variety of
types.

Define a system of polynomial equations .

::

    S := [3*x^3 + y + 1 = 0,y^2 = 4]

.. math:: {{\left \lbrack {}{{y}}+3{\:}{{{{x}}}^{3}}+1=0{,\:}{{{{y}}}^{2}}=4 \right \rbrack {}}}

``List(Equation(Polynomial(Integer)))``

Solve the system using rational number arithmetic and 30 digits of
accuracy.

::

    solve(S,1/10^30)

.. math:: {{\left \lbrack {}{{\left \lbrack {}{{y}}=-{2}{,\:}{{x}}=\frac{57602201066248085349 435651342568509}{83076749736557242056 487941267521536} \right \rbrack {}}}{,\:}{{\left \lbrack {}{{y}}=2{,\:}{{x}}=-{\frac{18707220957835557353 00716585876842265159 59365500929}{18707220957835557353 00716585876842265159 59365500928}} \right \rbrack {}}} \right \rbrack {}}}

``List(List(Equation(Polynomial(Fraction(Integer)))))``

Solve with the solutions expressed in radicals.

::

    radicalSolve(S)

.. math:: {{\left \lbrack {}{{\left \lbrack {}{{y}}=2{,\:}{{x}}=-{1} \right \rbrack {}}}{,\:}{{\left \lbrack {}{{y}}=2{,\:}{{x}}=\frac{-{\sqrt{-{3}}}+1}{2} \right \rbrack {}}}{,\:}{{\left \lbrack {}{{y}}=2{,\:}{{x}}=\frac{\sqrt{-{3}}+1}{2} \right \rbrack {}}}{,\:}{{\left \lbrack {}{{y}}=-{2}{,\:}{{x}}=\frac{1}{{\sqrt[3]{3}}} \right \rbrack {}}}{,\:}{{\left \lbrack {}{{y}}=-{2}{,\:}{{x}}=\frac{\sqrt{-{1}}{\:}\sqrt{3}-{1}}{2{\:}{\sqrt[3]{3}}} \right \rbrack {}}}{,\:}{{\left \lbrack {}{{y}}=-{2}{,\:}{{x}}=\frac{-{\sqrt{-{1}}{\:}\sqrt{3}}-{1}}{2{\:}{\sqrt[3]{3}}} \right \rbrack {}}} \right \rbrack {}}}

``List(List(Equation(Expression(Integer))))``

While these solutions look very different, the results were produced by
the same internal algorithm! The internal algorithm actually works with
equations over any “field.” Examples of fields are the rational numbers,
floating point numbers, rational functions, power series, and general
expressions involving radicals.

Users and system developers alike can augment the FriCAS library, all
using one common language. Library code, like interpreter code, is
compiled into machine binary code for run-time efficiency.

Using this language, you can create new computational types and new
algorithmic packages. All library code is polymorphic, described in
terms of a database of algebraic properties. By following the language
protocols, there is an automatic, guaranteed interaction between your
code and that of colleagues and system implementers.

FriCAS has both an *interactive language* for user interactions and a
*programming language* for building library modules. Like Modula 2,
PASCAL, FORTRAN, and Ada, the programming language emphasizes strict
type-checking. Unlike these languages, types in FriCAS are dynamic
objects: they are created at run-time in response to user commands.

Here is the idea of the FriCAS programming language in a nutshell.
FriCAS types range from algebraic ones (like polynomials, matrices, and
power series) to data structures (like lists, dictionaries, and input
files). Types combine in any meaningful way. You can build polynomials
of matrices, matrices of polynomials of power series, hash tables with
symbolic keys and rational function entries, and so on.

*Categories* define algebraic properties to ensure mathematical
correctness. They ensure, for example, that matrices of polynomials are
OK, but matrices of input files are not. Through categories, programs
can discover that polynomials of continued fractions have a commutative
multiplication whereas polynomials of matrices do not.

Categories allow algorithms to be defined in their most natural setting.
For example, an algorithm can be defined to solve polynomial equations
over *any* field. Likewise a greatest common divisor can compute the
“gcd” of two elements from *any* Euclidean domain. Categories foil
attempts to compute meaningless “gcds”, for example, of two hashtables.
Categories also enable algorithms to be compiled into machine code that
can be run with arbitrary types.

The FriCAS interactive language is oriented towards ease-of-use. The
FriCAS interpreter uses type-inferencing to deduce the type of an object
from user input. Type declarations can generally be omitted for common
types in the interactive language.

So much for the nutshell. Here are these basic ideas described by ten
design principles:

Basic types are called *domains of computation*, or, simply, *domains.*
Domains are defined by FriCAS programs of the form:

::

    Name(...): Exports == Implementation

Each domain has a capitalized Name that is used to refer to the class of
its members. For example, denotes “the class of integers,” , “the class
of floating point numbers,” and , “the class of strings.”

The “...” part following Name lists zero or more parameters to the
constructor. Some basic ones like take no parameters. Others, like , and
, take a single parameter that again must be a domain. For example,
denotes “matrices over the integers,” denotes “polynomial with floating
point coefficients,” and denotes “lists of matrices of polynomials over
the integers.” There is no restriction on the number or type of
parameters of a domain constructor. The Exports part specifies
operations for creating and manipulating objects of the domain. For
example, type exports constants and , and operations , , and . While
these operations are common, others such as and are not.

The Implementation part defines functions that implement the exported
operations of the domain. These functions are frequently described in
terms of another lower-level domain used to represent the objects of the
domain.

Every FriCAS object belongs to a *unique* domain. The domain of an
object is also called its *type.* Thus the integer has type and the
string “daniel” has type .

The type of an object, however, is not unique. The type of integer is
not only but , , and possibly, in general, any other “subdomain” of the
domain . A *subdomain* is a domain with a “membership predicate”. is a
subdomain of with the predicate “is the integer ?”.

Subdomains with names are defined by abstract datatype programs similar
to those for domains. The *Export* part of a subdomain, however, must
list a subset of the exports of the domain. The Implementation part
optionally gives special definitions for subdomain objects.

Domain and subdomains in FriCAS are themselves objects that have types.
The type of a domain or subdomain is called a *category*. Categories are
described by programs of the form:

::

    Name(...): Category == Exports

The type of every category is the distinguished symbol Category. The
category Name is used to designate the class of domains of that type.
For example, category designates the class of all rings. Like domains,
categories can take zero or more parameters as indicated by the “...”
part following Name. Two examples are and .

The Exports part defines a set of operations. For example, exports the
operations , , , , and . Many algebraic domains such as and are rings.
and (for any domain ) are not.

Categories serve to ensure the type-correctness. The definition of
matrices states Matrix(R: Ring) requiring its single parameter to be a
ring. Thus a “matrix of polynomials” is allowed, but “matrix of lists”
is not.

All operations have prescribed source and target types. Types can be
denoted by symbols that stand for domains, called “symbolic domains.”
The following lines of FriCAS code use a symbolic domain :

::

    R: Ring
    power: (R, NonNegativeInteger): R -> R
    power(x, n) == x ** n

Line 1 declares the symbol to be a ring. Line 2 declares the type of in
terms of . From the definition on line 3, produces 9 for and . Also,
produces for and . however fails since has type which is not a ring.

Using symbolic domains, algorithms can be defined in their most natural
or general setting.

Categories form hierarchies (technically, directed-acyclic graphs). A
simplified hierarchical world of algebraic categories is shown below in
Figure . At the top of this world is , the class of algebraic sets. The
notions of parents, ancestors, and descendants is clear. Thus ordered
sets (domains of category ) and rings are also algebraic sets. Likewise,
fields and integral domains are rings and algebraic sets. However fields
and integral domains are not ordered sets.

.5

+----------------------+--------------------+----------------------+--------------------+----------------------+
|                      |                    |                      |                    |                      |
+----------------------+--------------------+----------------------+--------------------+----------------------+
|                      | :math:`\swarrow`   | :math:`\downarrow`   | :math:`\searrow`   |                      |
+----------------------+--------------------+----------------------+--------------------+----------------------+
|                      |                    |                      |                    |                      |
+----------------------+--------------------+----------------------+--------------------+----------------------+
| :math:`\downarrow`   |                    |                      | :math:`\searrow`   | :math:`\downarrow`   |
+----------------------+--------------------+----------------------+--------------------+----------------------+
|                      |                    |                      |                    |                      |
+----------------------+--------------------+----------------------+--------------------+----------------------+
| :math:`\downarrow`   |                    |                      |                    |                      |
+----------------------+--------------------+----------------------+--------------------+----------------------+
|                      |                    |                      |                    |                      |
+----------------------+--------------------+----------------------+--------------------+----------------------+

.5

::

    SetCategory +---- Ring       ---- IntegralDomain ---- Field
                |
                +---- Finite     ---+
                |                    \
                +---- OrderedSet -----+ OrderedFinite

Figure 1. A simplified category hierarchy.

A category designates a class of domains. Which domains? You might think
that designates the class of all domains that export , , , , and . But
this is not so. Each domain must *assert* which categories it belongs
to.

The Export part of the definition for reads, for example:

::

    Join(OrderedSet, IntegralDomain,  ...) with ...

This definition asserts that is both an ordered set and an integral
domain. In fact, does not explicitly export constants and and operations
, and at all: it inherits them all from ! Since is a descendant of , is
therefore also a ring.

Assertions can be conditional. For example, defines its exports by:

::

    Ring with ... if R has Field then Field ...

Thus is a field but is not since is not a field.

You may wonder: “Why not simply let the set of operations determine
whether a domain belongs to a given category?”. FriCAS allows operation
names (for example, ) to have very different meanings in different
contexts. The meaning of an operation in FriCAS is determined by
context. By associating operations with categories, operation names can
be reused whenever appropriate or convenient to do so. As a simple
example, the operation might be used to denote lexicographic-comparison
in an algorithm. However, it is wrong to use the same with this
definition of absolute-value: abs(x) == if x < 0 then -x else x. Such a
definition for abs in FriCAS is protected by context: argument is
required to be a member of a domain of category .

In FriCAS, facilities for symbolic integration, solution of equations,
and the like are placed in “packages”. A *package* is a special kind of
domain: one whose exported operations depend solely on the parameters of
the constructor and/or explicit domains.

If you want to use FriCAS, for example, to define some algorithms for
solving equations of polynomials over an arbitrary field , you can do so
with a package of the form:

::

    MySolve(F: Field): Exports == Implementation

where Exports specifies the operations you wish to export and
Implementation defines functions for implementing your algorithms. Once
FriCAS has compiled your package, your algorithms can then be used for
any : floating-point numbers, rational numbers, complex rational
functions, and power series, to name a few.

The FriCAS interpreter reads user input then builds whatever types it
needs to perform the indicated computations. For example, to create the
matrix

.. math:: M = \begin{pmatrix}x^2+1&0\\0&x / 2\end{pmatrix}

the interpreter first loads the modules , , , and from the library, then
builds the *domain tower* “matrices of polynomials of rational numbers
(fractions of integers)”. Once a domain tower is built, computation
proceeds by calling operations down the tower. For example, suppose that
the user asks to square the above matrix. To do this, the function from
is passed to compute . The function is also passed an environment
containing that, in this case, is . This results in the successive
calling of the operations from , then from , and then finally from
before a result is passed back up the tower.

Categories play a policing role in the building of domains. Because the
argument of is required to be a ring, FriCAS will not build nonsensical
types such as “matrices of input files”.

FriCAS programs are statically compiled to machine code, then placed
into library modules. Categories provide an important role in obtaining
efficient object code by enabling:

-  static type-checking at compile time;

-  fast linkage to operations in domain-valued parameters;

-  optimization techniques to be used for partially specified types
   (operations for “vectors of ”, for instance, can be open-coded even
   though is unknown).

Users and system implementers alike use the FriCAS language to add
facilities to the FriCAS library. The entire FriCAS library is in fact
written in the FriCAS source code and available for user modification
and/or extension.

FriCAS’s use of abstract datatypes clearly separates the exports of a
domain (what operations are defined) from its implementation (how the
objects are represented and operations are defined). Users of a domain
can thus only create and manipulate objects through these exported
operations. This allows implementers to “remove and replace” parts of
the library safely by newly upgraded (and, we hope, correct)
implementations without consequence to its users.

Categories protect names by context, making the same names available for
use in other contexts. Categories also provide for code-economy.
Algorithms can be parameterized categorically to characterize their
correct and most general context. Once compiled, the same machine code
is applicable in all such contexts.

Finally, FriCAS provides an automatic, guaranteed interaction between
new and old code. For example:

-  if you write a new algorithm that requires a parameter to be a field,
   then your algorithm will work automatically with every field defined
   in the system; past, present, or future.

-  if you introduce a new domain constructor that produces a field, then
   the objects of that domain can be used as parameters to any algorithm
   using field objects defined in the system; past, present, or future.

These are the key ideas. For further information, we particularly
recommend your reading chapters 11, 12, and 13, where these ideas are
explained in greater detail.

Welcome to the FriCAS environment for interactive computation and
problem solving. Consider this chapter a brief, whirlwind tour of the
FriCAS world. We introduce you to FriCAS’s graphics and the FriCAS
language. Then we give a sampling of the large variety of facilities in
the FriCAS system, ranging from the various kinds of numbers, to data
types (like lists, arrays, and sets) and mathematical objects (like
matrices, integrals, and differential equations). We conclude with the
discussion of system commands and an interactive “undo.”

Before embarking on the tour, we need to brief those readers working
interactively with FriCAS on some details. Others can skip right
immediately to .

You need to know how to start the FriCAS system and how to stop it. We
assume that FriCAS has been correctly installed on your machine (as
described in another FriCAS document).

To begin using FriCAS, issue the command **axiom** to the operating
system shell. There is a brief pause, some start-up messages, and then
one or more windows appear.

If you are not running FriCAS under the X Window System, there is only
one window (the console). At the lower left of the screen there is a
prompt that looks like

::

    (1) ->

When you want to enter input to FriCAS, you do so on the same line after
the prompt. The “1” in “(1)” is the computation step number and is
incremented after you enter FriCAS statements. Note, however, that a
system command such as may change the step number in other ways. We talk
about step numbers more when we discuss system commands and the
workspace history facility.

If you are running FriCAS under the X Window System, there may be two
windows: the console window (as just described) and the main menu. is a
multiple-window hypertext system that lets you view FriCAS documentation
and examples on-line, execute FriCAS expressions, and generate graphics.
If you are in a graphical windowing environment, it is usually started
automatically when FriCAS begins. If it is not running, issue to start
it. We discuss the basics of in .

To interrupt an FriCAS computation, hold down the (control) key and
press . This brings you back to the FriCAS prompt.

To exit from FriCAS, move to the console window, type at the input
prompt and press the key. You will probably be prompted with the
following message:

| Please enter **y** or **yes** if you really want to leave the
| interactive environment and return to the operating system

You should respond **yes**, for example, to exit FriCAS.

We are purposely vague in describing exactly what your screen looks like
or what messages FriCAS displays. FriCAS runs on a number of different
machines, operating systems and window environments, and these
differences all affect the physical look of the system. You can also
change the way that FriCAS behaves via described later in this chapter
and in . System commands are special commands, like , that begin with a
closing parenthesis and are used to change your environment. For
example, you can set a system variable so that you are not prompted for
confirmation when you want to leave FriCAS.

If you are using FriCAS under the X Window System, the command line
editor is probably available and installed. With this editor you can
recall previous lines with the up and down arrow keys.

To move forward and backward on a line, use the right and left arrows.
You can use the key to toggle insert mode on or off. When you are in
insert mode, the cursor appears as a large block and if you type
anything, the characters are inserted into the line without deleting the
previous ones.

If you press the key, the cursor moves to the beginning of the line and
if you press the key, the cursor moves to the end of the line. Pressing
– deletes all the text from the cursor to the end of the line.

also provides FriCAS operation name completion for a limited set of
operations. If you enter a few letters and then press the key, tries to
use those letters as the prefix of an FriCAS operation name. If a name
appears and it is not what you want, press again to see another name.

You are ready to begin your journey into the world of FriCAS. Proceed to
the first stop.

In this book we have followed these typographical conventions:

-  Categories, domains and packages are displayed in a sans-serif
   typeface: , , .

-  Prefix operators, infix operators, and punctuation symbols in the
   FriCAS language are displayed in the text like this: , , .

-  FriCAS expressions or expression fragments are displayed in a
   monospace typeface: .

-  For clarity of presentation, TeX is often used to format expressions:
   :math:`g(x)=x^2+1.`

-  Function names and button names are displayed in the text in a bold
   typeface: , , **Lighting**.

-  Italics are used for emphasis and for words defined in the glossary:
   .

This book contains over 2500 examples of FriCAS input and output. All
examples were run though FriCAS and their output was created in TeX form
for this book by the FriCAS package. We have deleted system messages
from the example output if those messages are not important for the
discussions in which the examples appear.

The FriCAS language is a rich language for performing interactive
computations and for building components of the FriCAS library. Here we
present only some basic aspects of the language that you need to know
for the rest of this chapter. Our discussion here is intentionally
informal, with details unveiled on an “as needed” basis. For more
information on a particular construct, we suggest you consult the index
at the back of the book.

For arithmetic expressions, use the and as in mathematics. Use for
multiplication, and for exponentiation. To create a fraction, use . When
an expression contains several operators, those of highest are evaluated
first. For arithmetic operators, has highest precedence, and have the
next highest precedence, and and have the lowest precedence.

FriCAS puts implicit parentheses around operations of higher precedence,
and groups those of equal precedence from left to right.

::

    1 + 2 - 3 / 4 * 3 ^ 2 - 1

.. math:: -{\frac{19}{4}}

``Fraction(Integer)``

The above expression is equivalent to this.

::

    ((1 + 2) - ((3 / 4) * (3 ^ 2))) - 1

.. math:: -{\frac{19}{4}}

``Fraction(Integer)``

If an expression contains subexpressions enclosed in parentheses, the
parenthesized subexpressions are evaluated first (from left to right,
from inside out).

::

    1 + 2 - 3/ (4 * 3 ^ (2 - 1))

.. math:: \frac{11}{4}

``Fraction(Integer)``

Use the percent sign () to refer to the last result. Also, use to refer
to previous results. is equivalent to , returns the next to the last
result, and so on. returns the result from step number 1, returns the
result from step number 2, and so on. is not defined.

This is ten to the tenth power.

::

    10 ^ 10 

.. math:: 10000000000

``PositiveInteger``

This is the last result minus one.

::

    % - 1 

.. math:: 9999999999

``PositiveInteger``

This is the last result.

::

    %%(-1) 

.. math:: 9999999999

``PositiveInteger``

This is the result from step number 1.

::

    %%(1) 

.. math:: 10000000000

``PositiveInteger``

Everything in FriCAS has a type. The type determines what operations you
can perform on an object and how the object can be used. An entire
chapter of this book () is dedicated to the interactive use of types.
Several of the final chapters discuss how types are built and how they
are organized in the FriCAS library.

Positive integers are given type .

::

    8

.. math:: 8

``PositiveInteger``

Negative ones are given type . This fine distinction is helpful to the
FriCAS interpreter.

::

    -8

.. math:: -{8}

``Integer``

Here a positive integer exponent gives a polynomial result.

::

    x^8

.. math:: {{{{x}}}^{8}}

``Polynomial(Integer)``

Here a negative integer exponent produces a fraction.

::

    x^(-8)

.. math:: \frac{1}{{{{{x}}}^{8}}}

``Fraction(Polynomial(Integer))``

A is a literal used for the input of things like the “variables” in
polynomials and power series.

We use the three symbols , , and in entering this polynomial.

::

    (x - y*z)^2

.. math:: {{{{y}}}^{2}}{\:}{{{{z}}}^{2}}-{2{\:}{{x}}{\:}{{y}}{\:}{{z}}}+{{{{x}}}^{2}}

``Polynomial(Integer)``

A symbol has a name beginning with an uppercase or lowercase alphabetic
character, , or . Successive characters (if any) can be any of the
above, digits, or . Case is distinguished: the symbol is different from
the symbol .

A symbol can also be used in FriCAS as a . A variable refers to a value.
To a value to a variable, the operator is used. [1]_ A variable
initially has no restrictions on the kinds of values to which it can
refer.

This assignment gives the value (an integer) to a variable named .

::

    x := 4

.. math:: 4

``PositiveInteger``

This gives the value (a polynomial) to .

::

    x := z + 3/5

.. math:: {{z}}+\frac{3}{5}

``Polynomial(Fraction(Integer))``

To restrict the types of objects that can be assigned to a variable, use
a

::

    y : Integer 

After a variable is declared to be of some type, only values of that
type can be assigned to that variable.

::

    y := 89

.. math:: 89

``Integer``

The declaration for forces values assigned to to be converted to integer
values.

::

    y := sin %pi

.. math:: 0

``Integer``

If no such conversion is possible, FriCAS refuses to assign a value to .

::

    y := 2/3

Cannot convert right-hand side of assignment 2 - 3

to an object of the type Integer of the left-hand side.

A type declaration can also be given together with an assignment. The
declaration can assist FriCAS in choosing the correct operations to
apply.

::

    f : Float := 2/3

.. math:: {\texttt{0.66666666666666666667}}

``Float``

Any number of expressions can be given on input line. Just separate them
by semicolons. Only the result of evaluating the last expression is
displayed.

These two expressions have the same effect as the previous single
expression.

::

    f : Float; f := 2/3 

.. math:: {\texttt{0.66666666666666666667}}

``Float``

The type of a symbol is either or (*name*) where *name* is the name of
the symbol.

By default, the interpreter gives this symbol the type .

::

    q

.. math:: {{q}}

``Variable(q)``

When multiple symbols are involved, is used.

::

    [q, r]

.. math:: {{\left \lbrack {}{{q}}{,\:}{{r}} \right \rbrack {}}}

``List(OrderedVariableList([q, r]))``

What happens when you try to use a symbol that is the name of a
variable?

::

    f 

.. math:: {\texttt{0.66666666666666666667}}

``Float``

Use a single quote () before the name to get the symbol.

::

    'f

.. math:: {{f}}

``Variable(f)``

Quoting a name creates a symbol by preventing evaluation of the name as
a variable. Experience will teach you when you are most likely going to
need to use a quote. We try to point out the location of such trouble
spots.

Objects of one type can usually be “converted” to objects of several
other types. To an object to a new type, use the infix operator. [2]_
For example, to display an object, it is necessary to convert the object
to type .

This produces a polynomial with rational number coefficients.

::

    p := r^2 + 2/3 

.. math:: {{{{r}}}^{2}}+\frac{2}{3}

``Polynomial(Fraction(Integer))``

Create a quotient of polynomials with integer coefficients by using .

::

    p :: Fraction Polynomial Integer 

.. math:: \frac{3{\:}{{{{r}}}^{2}}+2}{3}

``Fraction(Polynomial(Integer))``

Some conversions can be performed automatically when FriCAS tries to
evaluate your input. Others conversions must be explicitly requested.

As we saw earlier, when you want to add or subtract two values, you
place the arithmetic operator or between the two denoting the values. To
use most other FriCAS operations, however, you use another syntax: write
the name of the operation first, then an open parenthesis, then each of
the arguments separated by commas, and, finally, a closing parenthesis.
If the operation takes only one argument and the argument is a number or
a symbol, you can omit the parentheses.

This calls the operation with the single integer argument .

::

    factor(120)

.. math:: {{2}^{3}}{\:}3{\:}5

``Factored(Integer)``

This is a call to with the two integer arguments and .

::

    divide(125,7)

.. math:: {{\left \lbrack {}{{quotient}}=17{,\:}{{remainder}}=6 \right \rbrack {}}}

``Record(quotient: Integer, remainder: Integer)``

This calls with four floating-point arguments.

::

    quatern(3.4,5.6,2.9,0.1)

.. math:: {\texttt{3.4}}+{\texttt{5.6}}{\:}{{i}}+{\texttt{2.9}}{\:}{{j}}+{\texttt{0.1}}{\:}{{k}}

``Quaternion(Float)``

This is the same as .

::

    factorial 10

.. math:: 3628800

``PositiveInteger``

An operations that returns a value (that is, or ) frequently has a name
suffixed with a question mark (“?”). For example, the operation returns
if its integer argument is an even number, otherwise.

An operation that can be destructive on one or more arguments usually
has a name ending in a exclamation point (“!”). This actually means that
it is *allowed* to update its arguments but it is not *required* to do
so. For example, the underlying representation of a collection type may
not allow the very last element to removed and so an empty object may be
returned instead. Therefore, it is important that you use the object
returned by the operation and not rely on a physical change having
occurred within the object. Usually, destructive operations are provided
for efficiency reasons.

FriCAS provides several for your convenience. [3]_ Macros are names (or
forms) that expand to larger expressions for commonly used values.

+----+--------------------------------------+
|    | The square root of -1.               |
+----+--------------------------------------+
|    | The base of the natural logarithm.   |
+----+--------------------------------------+
|    | :math:`\pi`.                         |
+----+--------------------------------------+
|    | :math:`\infty`.                      |
+----+--------------------------------------+
|    | :math:`+\infty`.                     |
+----+--------------------------------------+
|    | :math:`-\infty`.                     |
+----+--------------------------------------+

When you enter FriCAS expressions from your keyboard, there will be
times when they are too long to fit on one line. FriCAS does not care
how long your lines are, so you can let them continue from the right
margin to the left side of the next line.

Alternatively, you may want to enter several shorter lines and have
FriCAS glue them together. To get this glue, put an underscore (\_) at
the end of each line you wish to continue.

::

    2_
    +_
    3

is the same as if you had entered

::

    2+3

If you are putting your FriCAS statements in an input file (see ), you
can use indentation to indicate the structure of your program. (see ).

Comment statements begin with two consecutive hyphens or two consecutive
plus signs and continue until the end of the line.

The comment beginning with – is ignored by FriCAS.

::

    2 + 3   -- this is rather simple, no?

.. math:: 5

``PositiveInteger``

There is no way to write long multi-line comments other than starting
each line with or .

FriCAS has a two- and three-dimensional drawing and rendering package
that allows you to draw, shade, color, rotate, translate, map, clip,
scale and combine graphic output of FriCAS computations. The graphics
interface is capable of plotting functions of one or more variables and
plotting parametric surfaces. Once the graphics figure appears in a
window, move your mouse to the window and click. A control panel appears
immediately and allows you to interactively transform the object.

This is an example of FriCAS’s two-dimensional plotting. From the 2D
Control Panel you can rescale the plot, turn axes and units on and off
and save the image, among other things. This PostScript image was
produced by clicking on the 2D Control Panel button.

::

    draw(cos(5*t/8), t=0..16*%pi, coordinates==polar)

This is an example of FriCAS’s three-dimensional plotting. It is a
monochrome graph of the complex arctangent function. The image displayed
was rotated and had the “shade” and “outline” display options set from
the 3D Control Panel. The PostScript output was produced by clicking on
the 3D Control Panel button and then clicking on the button. See for
more details and examples of FriCAS’s numeric and graphics capabilities.

::

    draw((x,y) +-> real atan complex(x,y), -%pi..%pi, -%pi..%pi, colorFunction == (x,y) +-> argument atan complex(x,y))

An exhibit of is given in the center section of this book. For a
description of the commands and programs that produced these figures,
see . PostScript output is available so that FriCAS images can be
printed. [4]_ See for more examples and details about using FriCAS’s
graphics facilities.

FriCAS distinguishes very carefully between different kinds of numbers,
how they are represented and what their properties are. Here are a
sampling of some of these kinds of numbers and some things you can do
with them.

Integer arithmetic is always exact.

::

    11^13 * 13^11 * 17^7 - 19^5 * 23^3

.. math:: 25387751112538918594 666224484237298

``PositiveInteger``

Integers can be represented in factored form.

::

    factor 643238070748569023720594412551704344145570763243 

.. math:: {{11}^{13}}{\:}{{13}^{11}}{\:}{{17}^{7}}{\:}{{19}^{5}}{\:}{{23}^{3}}{\:}{{29}^{2}}

``Factored(Integer)``

Results stay factored when you do arithmetic. Note that the is
automatically factored for you.

::

    % * 12 

.. math:: {{2}^{2}}{\:}3{\:}{{11}^{13}}{\:}{{13}^{11}}{\:}{{17}^{7}}{\:}{{19}^{5}}{\:}{{23}^{3}}{\:}{{29}^{2}}

``Factored(Integer)``

Integers can also be displayed to bases other than 10. This is an
integer in base 11.

::

    radix(25937424601,11)

.. math:: 10000000000

``RadixExpansion(11)``

Roman numerals are also available for those special occasions.

::

    roman(1992)

.. math:: {{MCMXCII}}

``RomanNumeral``

Rational number arithmetic is also exact.

::

    r := 10 + 9/2 + 8/3 + 7/4 + 6/5 + 5/6 + 4/7 + 3/8 + 2/9

.. math:: \frac{55739}{2520}

``Fraction(Integer)``

To factor fractions, you have to map onto the numerator and denominator.

::

    map(factor,r) 

.. math:: \frac{139{\:}401}{{{2}^{3}}{\:}{{3}^{2}}{\:}5{\:}7}

``Fraction(Factored(Integer))``

Type refers to machine word-length integers. In English, this expression
means “ as a small integer”.

::

    11@SingleInteger

.. math:: 11

``SingleInteger``

Machine double-precision floating-point numbers are also available for
numeric and graphical applications.

::

    123.21@DoubleFloat

.. math:: {\texttt{123.21000000000001}}

``DoubleFloat``

The normal floating-point type in FriCAS, , is a software implementation
of floating-point numbers in which the exponent and the mantissa may
have any number of digits. [5]_ The types and are the corresponding
software implementations of complex floating-point numbers.

This is a floating-point approximation to about twenty digits. The is
used here to change from one kind of object (here, a rational number) to
another (a floating-point number).

::

    r :: Float 

.. math:: {\texttt{22.118650793650793651}}

``Float``

Use to change the number of digits in the representation. This operation
returns the previous value so you can reset it later.

::

    digits(22) 

.. math:: 20

``PositiveInteger``

To digits of precision, the number :math:`e^{\pi {\sqrt {163.0}}}`
appears to be an integer.

::

    exp(%pi * sqrt 163.0) 

.. math:: {\texttt{262537412640768744.0}}

``Float``

Increase the precision to forty digits and try again.

::

    digits(40);  exp(%pi * sqrt 163.0) 

.. math:: {\texttt{262537412640768743.9999999999992500725976}}

``Float``

Here are complex numbers with rational numbers as real and imaginary
parts.

::

    (2/3 + %i)^3 

.. math:: -{\frac{46}{27}}+\frac{1}{3}{\:}{i}

``Complex(Fraction(Integer))``

The standard operations on complex numbers are available.

::

    conjugate % 

.. math:: -{\frac{46}{27}}-{\frac{1}{3}{\:}{i}}

``Complex(Fraction(Integer))``

You can factor complex integers.

::

    factor(89 - 23 * %i)

.. math:: -{{{\left ( {}1+{i}\right ) {}}}{\:}{{{{\left ( {}2+{i}\right ) {}}}}^{2}}{\:}{{{{\left ( {}3+2{\:}{i}\right ) {}}}}^{2}}}

``Factored(Complex(Integer))``

Complex numbers with floating point parts are also available.

::

    exp(%pi/4.0 * %i)

.. math:: {\texttt{0.7071067811865475244008443621048490392849}}+{\texttt{0.7071067811865475244008443621048490392848}}{\:}{i}

``Complex(Float)``

Every rational number has an exact representation as a repeating decimal
expansion (see ).

::

    decimal(1/352)

.. math:: 0{\texttt{.}}00284\overline{09}

``DecimalExpansion``

A rational number can also be expressed as a continued fraction (see ).

::

    continuedFraction(6543/210)

.. math:: 31+{\frac{\left.{1}\right|}{\left|{6}\right.}}+{\frac{\left.{1}\right|}{\left|{2}\right.}}+{\frac{\left.{1}\right|}{\left|{1}\right.}}+{\frac{\left.{1}\right|}{\left|{3}\right.}}

``ContinuedFraction(Integer)``

Also, partial fractions can be used and can be displayed in a compact …

::

    partialFraction(1,factorial(10)) 

.. math:: \frac{159}{{{2}^{8}}}-{\frac{23}{{{3}^{4}}}}-{\frac{12}{{{5}^{2}}}}+\frac{1}{7}

``PartialFraction(Integer)``

or expanded format (see ).

::

    padicFraction(%) 

.. math:: \frac{1}{2}+\frac{1}{{{2}^{4}}}+\frac{1}{{{2}^{5}}}+\frac{1}{{{2}^{6}}}+\frac{1}{{{2}^{7}}}+\frac{1}{{{2}^{8}}}-{\frac{2}{{{3}^{2}}}}-{\frac{1}{{{3}^{3}}}}-{\frac{2}{{{3}^{4}}}}-{\frac{2}{5}}-{\frac{2}{{{5}^{2}}}}+\frac{1}{7}

``PartialFraction(Integer)``

Like integers, bases (radices) other than ten can be used for rational
numbers (see ). Here we use base eight.

::

    radix(4/7, 8)

.. math:: 0{\texttt{.}}\overline{4}

``RadixExpansion(8)``

Of course, there are complex versions of these as well. FriCAS decides
to make the result a complex rational number.

::

    % + 2/3*%i

.. math:: \frac{4}{7}+\frac{2}{3}{\:}{i}

``Complex(Fraction(Integer))``

You can also use FriCAS to manipulate fractional powers.

::

    (5 + sqrt 63 + sqrt 847)^(1/3)

.. math:: {\sqrt[3]{14{\:}\sqrt{7}+5}}

``AlgebraicNumber``

You can also compute with integers modulo a prime.

::

    x : PrimeField 7 := 5 

.. math:: 5

``PrimeField(7)``

Arithmetic is then done modulo .

::

    x^3 

.. math:: 6

``PrimeField(7)``

Since is prime, you can invert nonzero values.

::

    1/x 

.. math:: 3

``PrimeField(7)``

You can also compute modulo an integer that is not a prime.

::

    y : IntegerMod 6 := 5 

.. math:: 5

``IntegerMod(6)``

All of the usual arithmetic operations are available.

::

    y^3 

.. math:: 5

``IntegerMod(6)``

Inversion is not available if the modulus is not a prime number. Modular
arithmetic and prime fields are discussed in .

::

    1/y 

There are 12 exposed and 12 unexposed library operations named / having
2 argument(s) but none was determined to be applicable. Use HyperDoc
Browse, or issue )display op / to learn more about the available
operations. Perhaps package-calling the operation or using coercions on
the arguments will allow you to apply the operation.

Cannot find a definition or applicable library operation named / with
argument type(s) PositiveInteger IntegerMod(6)

Perhaps you should use “@” to indicate the required return type, or
“:math:`" to specify which version of the function you need.
\end{MessageOutput}
\end{xtc}
\begin{xtc}
\begin{xtccomment}
This defines \spad{a} to be an algebraic number, that is,
a root of a polynomial equation.
\end{xtccomment}
\begin{verbatim}
a := rootOf(a^5 + a^3 + a^2 + 3,a) 
\end{verbatim}
\begin{TeXOutput}
`\ :math:`
{{a}}`\ :math:`
\end{TeXOutput}
{\texttt{Expression(Integer)}}
\end{xtc}
\begin{xtc}
\begin{xtccomment}
Computations with \spad{a} are reduced according
to the polynomial equation.
\end{xtccomment}
\begin{verbatim}
(a + 1)^10
\end{verbatim}
\begin{TeXOutput}
`\ :math:`
-{85{\:}{{{{a}}}^{4}}}-{264{\:}{{{{a}}}^{3}}}-{378{\:}{{{{a}}}^{2}}}-{458{\:}{{a}}}-{287}`\ :math:`
\end{TeXOutput}
{\texttt{Expression(Integer)}}
\end{xtc}
\begin{xtc}
\begin{xtccomment}
Define \spad{b} to be an algebraic number involving \spad{a}.
\end{xtccomment}
\begin{verbatim}
b := rootOf(b^4 + a,b) 
\end{verbatim}
\begin{TeXOutput}
`\ :math:`
{{b}}`\ :math:`
\end{TeXOutput}
{\texttt{Expression(Integer)}}
\end{xtc}
\begin{xtc}
\begin{xtccomment}
Do some arithmetic.
\end{xtccomment}
\begin{verbatim}
2/(b - 1) 
\end{verbatim}
\begin{TeXOutput}
`\ :math:`
\frac{2}{{{b}}-{1}}`\ :math:`
\end{TeXOutput}
{\texttt{Expression(Integer)}}
\end{xtc}
\begin{xtc}
\begin{xtccomment}
To expand and simplify this, call \spadfun{ratDenom}
to rationalize the denominator.
\end{xtccomment}
\begin{verbatim}
ratDenom(\end{verbatim}
\begin{TeXOutput}
`\ :math:`
{{\left ( {}{{{{a}}}^{4}}-{{{{{a}}}^{3}}}+2{\:}{{{{a}}}^{2}}-{{{a}}}+1 \right ) {}}}{\:}{{{{b}}}^{3}}+{{\left ( {}{{{{a}}}^{4}}-{{{{{a}}}^{3}}}+2{\:}{{{{a}}}^{2}}-{{{a}}}+1 \right ) {}}}{\:}{{{{b}}}^{2}}+{{\left ( {}{{{{a}}}^{4}}-{{{{{a}}}^{3}}}+2{\:}{{{{a}}}^{2}}-{{{a}}}+1 \right ) {}}}{\:}{{b}}+{{{{a}}}^{4}}-{{{{{a}}}^{3}}}+2{\:}{{{{a}}}^{2}}-{{{a}}}+1`\ :math:`
\end{TeXOutput}
{\texttt{Expression(Integer)}}
\end{xtc}
\begin{xtc}
\begin{xtccomment}
If we do this, we should get \spad{b}.
\end{xtccomment}
\begin{verbatim}
2/\end{verbatim}
\begin{TeXOutput}
`\ :math:`
\frac{{{\left ( {}{{{{a}}}^{4}}-{{{{{a}}}^{3}}}+2{\:}{{{{a}}}^{2}}-{{{a}}}+1 \right ) {}}}{\:}{{{{b}}}^{3}}+{{\left ( {}{{{{a}}}^{4}}-{{{{{a}}}^{3}}}+2{\:}{{{{a}}}^{2}}-{{{a}}}+1 \right ) {}}}{\:}{{{{b}}}^{2}}+{{\left ( {}{{{{a}}}^{4}}-{{{{{a}}}^{3}}}+2{\:}{{{{a}}}^{2}}-{{{a}}}+1 \right ) {}}}{\:}{{b}}+{{{{a}}}^{4}}-{{{{{a}}}^{3}}}+2{\:}{{{{a}}}^{2}}-{{{a}}}+3}{{{\left ( {}{{{{a}}}^{4}}-{{{{{a}}}^{3}}}+2{\:}{{{{a}}}^{2}}-{{{a}}}+1 \right ) {}}}{\:}{{{{b}}}^{3}}+{{\left ( {}{{{{a}}}^{4}}-{{{{{a}}}^{3}}}+2{\:}{{{{a}}}^{2}}-{{{a}}}+1 \right ) {}}}{\:}{{{{b}}}^{2}}+{{\left ( {}{{{{a}}}^{4}}-{{{{{a}}}^{3}}}+2{\:}{{{{a}}}^{2}}-{{{a}}}+1 \right ) {}}}{\:}{{b}}+{{{{a}}}^{4}}-{{{{{a}}}^{3}}}+2{\:}{{{{a}}}^{2}}-{{{a}}}+1}`\ :math:`
\end{TeXOutput}
{\texttt{Expression(Integer)}}
\end{xtc}
\begin{xtc}
\begin{xtccomment}
But we need to rationalize the denominator again.
\end{xtccomment}
\begin{verbatim}
ratDenom(\end{verbatim}
\begin{TeXOutput}
`\ :math:`
{{b}}`\ :math:`
\end{TeXOutput}
{\texttt{Expression(Integer)}}
\end{xtc}
\begin{xtc}
\begin{xtccomment}
Types \spadtype{Quaternion} and \spadtype{Octonion} are also available.
Multiplication of quaternions is non-commutative, as expected.
\end{xtccomment}
\begin{verbatim}
q:=quatern(1,2,3,4)*quatern(5,6,7,8) - quatern(5,6,7,8)*quatern(1,2,3,4)
\end{verbatim}
\begin{TeXOutput}
`\ :math:`
-{8{\:}{{i}}}+16{\:}{{j}}-{8{\:}{{k}}}`\ :math:`
\end{TeXOutput}
{\texttt{Quaternion(Integer)}}
\end{xtc}

\head{section}{Data Structures}{ugIntroCollect}

{{FriCAS}}{} has a large variety of data structures available.
Many data structures are particularly useful for interactive
computation and others are useful for building applications.
The data structures of {{FriCAS}}{} are organized into
\spadglossSee{category hierarchies}{hierarchy} as shown on
the inside back cover.

A \spadgloss{list} is the most commonly used data structure in
{{FriCAS}}{} for holding objects all of the same
type.\footnote{Lists are discussed in \xmpref{List} and in
\spadref{ugLangIts}.}
The name {\it list} is short for ``linked-list of nodes.'' Each
node consists of a value (\spadfunFrom{first}{List}) and a link
(\spadfunFrom{rest}{List}) that
\spadglossSee{points}{pointer} to the next node, or to a
distinguished value denoting the empty list.
To get to, say, the third element, {{FriCAS}}{} starts at the front
of the list, then traverses across two links to the third node.

\begin{xtc}
\begin{xtccomment}
Write a list of elements using
square brackets with commas separating the elements.
\end{xtccomment}
\begin{verbatim}
u := [1,-7,11] 
\end{verbatim}
\begin{TeXOutput}
`\ :math:`
{{\left \lbrack {}1{,\:}-{7}{,\:}11 \right \rbrack {}}}`\ :math:`
\end{TeXOutput}
{\texttt{List(Integer)}}
\end{xtc}
\begin{xtc}
\begin{xtccomment}
This is the value at the third node.
Alternatively, you can say \spad{u.3}.
\end{xtccomment}
\begin{verbatim}
first rest rest u
\end{verbatim}
\begin{TeXOutput}
`\ :math:`
11`\ :math:`
\end{TeXOutput}
{\texttt{PositiveInteger}}
\end{xtc}

Many operations are defined on lists, such as:
\spadfun{empty?}, to test that a list has no elements;
\spadfun{cons}\spad{(x,l)}, to create a new list with
\spadfun{first} element \spad{x} and \spadfun{rest} \spad{l};
\spadfun{reverse}, to create a new list with elements in reverse
order; and \spadfun{sort}, to arrange elements in order.

An important point about lists is that they are ``mutable'': their
constituent elements and links can be changed ``in place.''
To do this, use any of the operations whose names end with the
character \spadSyntax{!}.

\begin{xtc}
\begin{xtccomment}
The operation \spadfunFromX{concat}{List}\spad{(u,v)}
replaces the last link of the list
\spad{u} to point to some other list \spad{v}.
Since \spad{u} refers to the original list,
this change is seen by \spad{u}.
\end{xtccomment}
\begin{verbatim}
concat!(u,[9,1,3,-4]); u
\end{verbatim}
\begin{TeXOutput}
`\ :math:`
{{\left \lbrack {}1{,\:}-{7}{,\:}11{,\:}9{,\:}1{,\:}3{,\:}-{4} \right \rbrack {}}}`\ :math:`
\end{TeXOutput}
{\texttt{List(Integer)}}
\end{xtc}
\begin{xtc}
\begin{xtccomment}
A {\it cyclic list} is a list with a ``cycle'':
\index{list!cyclic}
a link pointing back to an earlier node of the list.
\index{cyclic list}
To create a cycle, first get a node somewhere down
the list.
\end{xtccomment}
\begin{verbatim}
lastnode := rest(u,3)
\end{verbatim}
\begin{TeXOutput}
`\ :math:`
{{\left \lbrack {}9{,\:}1{,\:}3{,\:}-{4} \right \rbrack {}}}`\ :math:`
\end{TeXOutput}
{\texttt{List(Integer)}}
\end{xtc}
\begin{xtc}
\begin{xtccomment}
Use \spadfunFromX{setrest}{List} to
change the link emanating from that node to point back to an
earlier part of the list.
\end{xtccomment}
\begin{verbatim}
setrest!(lastnode,rest(u,2)); u
\end{verbatim}
\begin{TeXOutput}
`\ :math:`
{{\left \lbrack {}1{,\:}-{7}{,\:}\overline{11{,\:}9} \right \rbrack {}}}`\ :math:`
\end{TeXOutput}
{\texttt{List(Integer)}}
\end{xtc}

A \spadgloss{stream}
is a structure that (potentially) has an infinite number of
distinct elements.\footnote{Streams are discussed in
\xmpref{Stream} and in \spadref{ugLangIts}.}
Think of a stream as an ``infinite list'' where elements are
computed successively.

\begin{xtc}
\begin{xtccomment}
Create an infinite stream of factored integers.
Only a certain number of initial elements are computed
and displayed.
\end{xtccomment}
\begin{verbatim}
[factor(i) for i in 2.. by 2] 
\end{verbatim}
\begin{TeXOutput}
`\ :math:`
{{\left \lbrack {}2{,\:}{{2}^{2}}{,\:}2{\:}3{,\:}{{2}^{3}}{,\:}2{\:}5{,\:}{{2}^{2}}{\:}3{,\:}2{\:}7{,\:}{\texttt{...}} \right \rbrack {}}}`\ :math:`
\end{TeXOutput}
{\texttt{Stream(Factored(Integer))}}
\end{xtc}
\begin{xtc}
\begin{xtccomment}
{{FriCAS}}{} represents streams by a collection of already-computed
elements together with a function to compute the next element
``on demand.''
Asking for the \eth{n} element causes elements \spad{1} through
\spad{n} to be evaluated.
\end{xtccomment}
\begin{verbatim}
\end{verbatim}
\begin{TeXOutput}
`\ :math:`
{{2}^{3}}{\:}{{3}^{2}}`\ :math:`
\end{TeXOutput}
{\texttt{Factored(Integer)}}
\end{xtc}

Streams can also be finite or cyclic.
They are implemented by a linked list structure similar to lists
and have many of the same operations.
For example, \spadfun{first} and \spadfun{rest} are used to access
elements and successive nodes of a stream.

A \spadgloss{one-dimensional array} is another data structure used
to hold objects of the same type.\footnote{See \xmpref{OneDimensionalArray} for
details.}
Unlike lists, one-dimensional arrays are inflexible---they are
\index{array!one-dimensional}
implemented using a fixed block of storage.
Their advantage is that they give quick and equal access time to
any element.

\begin{xtc}
\begin{xtccomment}
A simple way to create a one-dimensional array is to apply the
operation \spadfun{oneDimensionalArray} to a list of elements.
\end{xtccomment}
\begin{verbatim}
a := oneDimensionalArray [1, -7, 3, 3/2]
\end{verbatim}
\begin{TeXOutput}
`\ :math:`
{{\left \lbrack {}1{,\:}-{7}{,\:}3{,\:}\frac{3}{2} \right \rbrack {}}}`\ :math:`
\end{TeXOutput}
{\texttt{OneDimensionalArray(Fraction(Integer))}}
\end{xtc}
\begin{xtc}
\begin{xtccomment}
One-dimensional arrays are also mutable:
you can change their constituent elements ``in place.''
\end{xtccomment}
\begin{verbatim}
a.3 := 11; a
\end{verbatim}
\begin{TeXOutput}
`\ :math:`
{{\left \lbrack {}1{,\:}-{7}{,\:}11{,\:}\frac{3}{2} \right \rbrack {}}}`\ :math:`
\end{TeXOutput}
{\texttt{OneDimensionalArray(Fraction(Integer))}}
\end{xtc}
\begin{xtc}
\begin{xtccomment}
However, one-dimensional arrays are not flexible structures.
You cannot destructively \spadfunX{concat} them together.
\end{xtccomment}
\begin{verbatim}
concat!(a,oneDimensionalArray [1,-2])
\end{verbatim}
\begin{MessageOutput}
   There are 5 exposed and 0 unexposed library operations named concat!
      having 2 argument(s) but none was determined to be applicable. 
      Use HyperDoc Browse, or issue
                             )display op concat!
      to learn more about the available operations. Perhaps 
      package-calling the operation or using coercions on the arguments
      will allow you to apply the operation.
\end{MessageOutput}
\begin{MessageOutput}
   Cannot find a definition or applicable library operation named 
      concat! with argument type(s) 
                   OneDimensionalArray(Fraction(Integer))
                        OneDimensionalArray(Integer)
      
      Perhaps you should use "@" to indicate the required return type, 
      or "`” to specify which version of the function you need.

Examples of datatypes similar to are: (vectors are mathematical
structures implemented by one-dimensional arrays), (arrays of
“characters,” represented by byte vectors), and (represented by “bit
vectors”).

A vector of 32 bits, each representing the value .

::

    bits(32,true)

.. math:: {\texttt{"11111111111111111111111111111111"}}

``Bits``

A is a cross between a list and a one-dimensional array. [6]_ Like a
one-dimensional array, a flexible array occupies a fixed block of
storage. Its block of storage, however, has room to expand! When it gets
full, it grows (a new, larger block of storage is allocated); when it
has too much room, it contracts.

Create a flexible array of three elements.

::

    f := flexibleArray [2, 7, -5]

.. math:: {{\left \lbrack {}2{,\:}7{,\:}-{5} \right \rbrack {}}}

``FlexibleArray(Integer)``

Insert some elements between the second and third elements.

::

    insert!(flexibleArray [11, -3],f,2)

.. math:: {{\left \lbrack {}2{,\:}11{,\:}-{3}{,\:}7{,\:}-{5} \right \rbrack {}}}

``FlexibleArray(Integer)``

Flexible arrays are used to implement “heaps.” A is an example of a data
structure called a , where elements are ordered with respect to one
another. [7]_ A heap is organized so as to optimize insertion and
extraction of maximum elements. The operation returns the maximum
element of the heap, after destructively removing that element and
reorganizing the heap so that the next maximum element is ready to be
delivered.

An easy way to create a heap is to apply the operation to a list of
values.

::

    h := heap [-4,7,11,3,4,-7]

.. math:: {{\left \lbrack {}11{,\:}7{,\:}-{4}{,\:}3{,\:}4{,\:}-{7} \right \rbrack {}}}

``Heap(Integer)``

This loop extracts elements one-at-a-time from until the heap is
exhausted, returning the elements as a list in the order they were
extracted.

::

    [extract!(h) while not empty?(h)]

.. math:: {{\left \lbrack {}11{,\:}7{,\:}4{,\:}3{,\:}-{4}{,\:}-{7} \right \rbrack {}}}

``List(Integer)``

A is a “tree” with at most two branches per node: it is either empty, or
else is a node consisting of a value, and a left and right subtree
(again, binary trees). [8]_

A *binary search tree* is a binary tree such that, for each node, the
value of the node is greater than all values (if any) in the left
subtree, and less than or equal all values (if any) in the right
subtree.

::

    binarySearchTree [5,3,2,9,4,7,11]

.. math:: {{\left \lbrack {}{{\left \lbrack {}2{,\:}3{,\:}4 \right \rbrack {}}}{,\:}5{,\:}{{\left \lbrack {}7{,\:}9{,\:}11 \right \rbrack {}}} \right \rbrack {}}}

``BinarySearchTree(PositiveInteger)``

A *balanced binary tree* is useful for doing modular computations. Given
a list of moduli, produces a balanced binary tree with the values
:math:`a \bmod m` at its leaves.

::

    modTree(8,[2,3,5,7])

.. math:: {{\left \lbrack {}0{,\:}2{,\:}3{,\:}1 \right \rbrack {}}}

``List(Integer)``

A is a collection of elements where duplication and order is
irrelevant. [9]_ Sets are always finite and have no corresponding
structure like streams for infinite collections.

Create sets by using the function.

::

    fs := set[1/3,4/5,-1/3,4/5] 

.. math:: {\left\{ -{\frac{1}{3}}{,\:}\frac{1}{3}{,\:}\frac{4}{5} \right\}}

``Set(Fraction(Integer))``

A is a set that keeps track of the number of duplicate values. [10]_

For all the primes between 2 and 1000, find the distribution of
:math:`p \bmod 5`.

::

    multiset [x rem 5 for x in primes(2,1000)]

.. math:: {\left\{ 47{\texttt{:\ }}2{,\:}42{\texttt{:\ }}3{,\:}0{,\:}40{\texttt{:\ }}1{,\:}38{\texttt{:\ }}4 \right\}}

``Multiset(Integer)``

A is conceptually a set of “key–value” pairs and is a generalization of
a multiset. [11]_ The domain provides a general-purpose type for tables
with *values* of type indexed by *keys* of type .

Compute the above distribution of primes using tables. First, let denote
an empty table of keys and values, each of type .

::

    t : Table(Integer,Integer) := empty()

.. math:: {\texttt{table}}{{\left ( {}\right ) {}}}

``Table(Integer, Integer)``

We define a function to return the number of values of a given modulus
seen so far. It calls which returns the number of values stored under
the key in table , or if no such value is yet stored in under .

In English, this says “Define as follows. First, let be the value of .
Then, if has the value , return the value ; otherwise return .”

::

    howMany(k) == (n:=search(k,t); n case "failed" => 1; n+1)

Run through the primes to create the table, then print the table. The
expression updates the value in table stored under key .

::

    for p in primes(2,1000) repeat (m:= p rem 5; t.m:= howMany(m)); t

Compiling function howMany with type Integer -> Integer

.. math:: {\texttt{table}}{{\left ( {}4=38,1=40,0=1,3=42,2=47 \right ) {}}}

``Table(Integer, Integer)``

A *record* is an example of an inhomogeneous collection of
objects. [12]_ A record consists of a set of named *selectors* that can
be used to access its components.

Declare that can only be assigned a record with two prescribed fields.

::

    daniel : Record(age : Integer, salary : Float) 

Give a value, using square brackets to enclose the values of the fields.

::

    daniel := [28, 32005.12] 

.. math:: {{\left \lbrack {}{{age}}=28{,\:}{{salary}}={\texttt{32005.12}} \right \rbrack {}}}

``Record(age: Integer, salary: Float)``

Give a raise.

::

    daniel.salary := 35000; daniel 

.. math:: {{\left \lbrack {}{{age}}=28{,\:}{{salary}}={\texttt{35000.0}} \right \rbrack {}}}

``Record(age: Integer, salary: Float)``

A *union* is a data structure used when objects have multiple
types. [13]_

Let be either an integer or a string value.

::

    dog: Union(licenseNumber: Integer, name: String)

Give a name.

::

    dog := "Whisper"

.. math:: {\texttt{"Whisper"}}

``Union(name: String, ...)``

All told, there are over forty different data structures in FriCAS.
Using the domain constructors described in , you can add your own data
structure or extend an existing one. Choosing the right data structure
for your application may be the key to obtaining good performance.

To get higher dimensional aggregates, you can create one-dimensional
aggregates with elements that are themselves aggregates, for example,
lists of lists, one-dimensional arrays of lists of multisets, and so on.
For applications requiring two-dimensional homogeneous aggregates, you
will likely find *two-dimensional arrays* and *matrices* most useful.

The entries in and objects are all the same type, except that those for
must belong to a . You create and access elements in roughly the same
way. Since matrices have an understood algebraic structure, certain
algebraic operations are available for matrices but not for arrays.
Because of this, we limit our discussion here to , that can be regarded
as an extension of . [14]_

You can create a matrix from a list of lists, where each of the inner
lists represents a row of the matrix.

::

    m := matrix([[1,2], [3,4]]) 

.. math:: \begin{bmatrix}{2}1&2\\3&4\end{bmatrix}

``Matrix(Integer)``

The “collections” construct (see ) is useful for creating matrices whose
entries are given by formulas.

::

    matrix([[1/(i + j - x) for i in 1..4] for j in 1..4]) 

.. math:: \begin{bmatrix}{4}-{\frac{1}{{{x}}-{2}}}&-{\frac{1}{{{x}}-{3}}}&-{\frac{1}{{{x}}-{4}}}&-{\frac{1}{{{x}}-{5}}}\\-{\frac{1}{{{x}}-{3}}}&-{\frac{1}{{{x}}-{4}}}&-{\frac{1}{{{x}}-{5}}}&-{\frac{1}{{{x}}-{6}}}\\-{\frac{1}{{{x}}-{4}}}&-{\frac{1}{{{x}}-{5}}}&-{\frac{1}{{{x}}-{6}}}&-{\frac{1}{{{x}}-{7}}}\\-{\frac{1}{{{x}}-{5}}}&-{\frac{1}{{{x}}-{6}}}&-{\frac{1}{{{x}}-{7}}}&-{\frac{1}{{{x}}-{8}}}\end{bmatrix}

``Matrix(Fraction(Polynomial(Integer)))``

Let denote the three by three Vandermonde matrix.

::

    vm := matrix [[1,1,1], [x,y,z], [x*x,y*y,z*z]] 

.. math:: \begin{bmatrix}{3}1&1&1\\{{x}}&{{y}}&{{z}}\\{{{{x}}}^{2}}&{{{{y}}}^{2}}&{{{{z}}}^{2}}\end{bmatrix}

``Matrix(Polynomial(Integer))``

Use this syntax to extract an entry in the matrix.

::

    vm(3,3) 

.. math:: {{{{z}}}^{2}}

``Polynomial(Integer)``

You can also pull out a or a .

::

    column(vm,2) 

.. math:: {{\left \lbrack {}1{,\:}{{y}}{,\:}{{{{y}}}^{2}} \right \rbrack {}}}

``Vector(Polynomial(Integer))``

You can do arithmetic.

::

    vm * vm 

.. math:: \begin{bmatrix}{3}{{{{x}}}^{2}}+{{x}}+1&{{{{y}}}^{2}}+{{y}}+1&{{{{z}}}^{2}}+{{z}}+1\\{{{{x}}}^{2}}{\:}{{z}}+{{x}}{\:}{{y}}+{{x}}&{{{{y}}}^{2}}{\:}{{z}}+{{{{y}}}^{2}}+{{x}}&{{{{z}}}^{3}}+{{y}}{\:}{{z}}+{{x}}\\{{{{x}}}^{2}}{\:}{{{{z}}}^{2}}+{{x}}{\:}{{{{y}}}^{2}}+{{{{x}}}^{2}}&{{{{y}}}^{2}}{\:}{{{{z}}}^{2}}+{{{{y}}}^{3}}+{{{{x}}}^{2}}&{{{{z}}}^{4}}+{{{{y}}}^{2}}{\:}{{z}}+{{{{x}}}^{2}}\end{bmatrix}

``Matrix(Polynomial(Integer))``

You can perform operations such as , , and .

::

    factor determinant vm 

.. math:: {{\left ( {}{{y}}-{{{x}}} \right ) {}}}{\:}{{\left ( {}{{z}}-{{{y}}} \right ) {}}}{\:}{{\left ( {}{{z}}-{{{x}}} \right ) {}}}

``Factored(Polynomial(Integer))``

FriCAS provides you with a very large library of predefined operations
and objects to compute with. You can use the FriCAS library of
constructors to create new objects dynamically of quite arbitrary
complexity. For example, you can make lists of matrices of fractions of
polynomials with complex floating point numbers as coefficients.
Moreover, the library provides a wealth of operations that allow you to
create and manipulate these objects.

For many applications, you need to interact with the interpreter and
write some FriCAS programs to tackle your application. FriCAS allows you
to write functions interactively, thereby effectively extending the
system library. Here we give a few simple examples, leaving the details
to .

We begin by looking at several ways that you can define the “factorial”
function in FriCAS. The first way is to give a piece-wise definition of
the function. This method is best for a general recurrence relation
since the pieces are gathered together and compiled into an efficient
iterative function. Furthermore, enough previously computed values are
automatically saved so that a subsequent call to the function can pick
up from where it left off.

Define the value of at .

::

    fact(0) == 1 

Define the value of for general .

::

    fact(n) == n*fact(n-1)

Ask for the value at . The resulting function created by FriCAS computes
the value by iteration.

::

    fact(50) 

Compiling function fact with type Integer -> Integer

Compiling function fact as a recurrence relation.

.. math:: 30414093201713378043 61260816606476884437 76415689605120000000 00000

``PositiveInteger``

A second definition uses an and recursion.

::

    fac(n) == if n < 3 then n else n * fac(n - 1) 

This function is less efficient than the previous version since each
iteration involves a recursive function call.

::

    fac(50) 

Compiling function fac with type Integer -> Integer

.. math:: 30414093201713378043 61260816606476884437 76415689605120000000 00000

``PositiveInteger``

A third version directly uses iteration.

::

    fa(n) == (a := 1; for i in 2..n repeat a := a*i; a) 

This is the least space-consumptive version.

::

    fa(50) 

Compiling function fa with type PositiveInteger -> PositiveInteger

.. math:: 30414093201713378043 61260816606476884437 76415689605120000000 00000

``PositiveInteger``

A final version appears to construct a large list and then reduces over
it with multiplication.

::

    f(n) == reduce(*,[i for i in 2..n]) 

In fact, the resulting computation is optimized into an efficient
iteration loop equivalent to that of the third version.

::

    f(50) 

Compiling function f with type PositiveInteger -> PositiveInteger

.. math:: 30414093201713378043 61260816606476884437 76415689605120000000 00000

``PositiveInteger``

The library version uses an algorithm that is different from the four
above because it highly optimizes the recurrence relation definition of
.

::

    factorial(50)

.. math:: 30414093201713378043 61260816606476884437 76415689605120000000 00000

``PositiveInteger``

You are not limited to one-line functions in FriCAS. If you place your
function definitions in **.input** files (see ), you can have multi-line
functions that use indentation for grouping.

Given elements, creates an by matrix with those elements down the
diagonal. This function uses a permutation matrix that interchanges the
th and th rows of a matrix by which it is right-multiplied.

This function definition shows a style of definition that can be used in
**.input** files. Indentation is used to create : sequences of
expressions that are evaluated in sequence except as modified by control
statements such as and .

::

    permMat(n, i, j) ==
      m := diagonalMatrix
        [(if i = k or j = k then 0 else 1)
          for k in 1..n]
      m(i,j) := 1
      m(j,i) := 1
      m

This creates a four by four matrix that interchanges the second and
third rows.

::

    p := permMat(4,2,3) 

Compiling function permMat with type (PositiveInteger,
PositiveInteger,PositiveInteger) -> Matrix(NonNegativeInteger)

.. math:: \begin{bmatrix}{4}1&0&0&0\\0&0&1&0\\0&1&0&0\\0&0&0&1\end{bmatrix}

``Matrix(NonNegativeInteger)``

Create an example matrix to permute.

::

    m := matrix [[4*i + j for j in 1..4] for i in 0..3]

.. math:: \begin{bmatrix}{4}1&2&3&4\\5&6&7&8\\9&10&11&12\\13&14&15&16\end{bmatrix}

``Matrix(NonNegativeInteger)``

Interchange the second and third rows of m.

::

    permMat(4,2,3) * m 

.. math:: \begin{bmatrix}{4}1&2&3&4\\9&10&11&12\\5&6&7&8\\13&14&15&16\end{bmatrix}

``Matrix(NonNegativeInteger)``

A function can also be passed as an argument to another function, which
then applies the function or passes it off to some other function that
does. You often have to declare the type of a function that has
functional arguments.

This declares to be a two-argument function that returns a . The first
argument is a function that takes one argument and returns a .

::

    t : (Float -> Float, Float) -> Float 

This is the definition of .

::

    t(fun, x) == fun(x)^2 + sin(x)^2 

We have not defined a in the workspace. The one from the FriCAS library
will do.

::

    t(cos, 5.2058) 

Compiling function t with type ((Float -> Float),Float) -> Float

.. math:: {\texttt{1.0}}

``Float``

Here we define our own (user-defined) function.

::

    cosinv(y) == cos(1/y) 

Pass this function as an argument to .

::

    t(cosinv, 5.2058) 

Compiling function cosinv with type Float -> Float

.. math:: {\texttt{1.739223724180051649254147684772932520785}}

``Float``

FriCAS also has pattern matching capabilities for simplification of
expressions and for defining new functions by rules. For example,
suppose that you want to apply regularly a transformation that groups
together products of radicals:

.. math::

   \sqrt{a}\:\sqrt{b} \mapsto \sqrt{ab}, \quad
   (\forall a)(\forall b)

 Note that such a transformation is not generally correct. FriCAS never
uses it automatically.

Give this rule the name .

::

    groupSqrt := rule(sqrt(a) * sqrt(b) == sqrt(a*b)) 

.. math:: {{{}}\%C}{\:}\sqrt{{{a}}}{\:}\sqrt{{{b}}}{{\ ==\ }}{{{}}\%C}{\:}\sqrt{{{a}}{\:}{{b}}}

``RewriteRule(Integer, Integer, Expression(Integer))``

Here is a test expression.

::

    a := (sqrt(x) + sqrt(y) + sqrt(z))^4 

.. math:: {{\left ( {}{{\left ( {}4{\:}{{z}}+4{\:}{{y}}+12{\:}{{x}} \right ) {}}}{\:}\sqrt{{{y}}}+{{\left ( {}4{\:}{{z}}+12{\:}{{y}}+4{\:}{{x}} \right ) {}}}{\:}\sqrt{{{x}}} \right ) {}}}{\:}\sqrt{{{z}}}+{{\left ( {}12{\:}{{z}}+4{\:}{{y}}+4{\:}{{x}} \right ) {}}}{\:}\sqrt{{{x}}}{\:}\sqrt{{{y}}}+{{{{z}}}^{2}}+{{\left ( {}6{\:}{{y}}+6{\:}{{x}} \right ) {}}}{\:}{{z}}+{{{{y}}}^{2}}+6{\:}{{x}}{\:}{{y}}+{{{{x}}}^{2}}

``Expression(Integer)``

The rule successfully simplifies the expression.

::

    groupSqrt a 

.. math:: {{\left ( {}4{\:}{{z}}+4{\:}{{y}}+12{\:}{{x}} \right ) {}}}{\:}\sqrt{{{y}}{\:}{{z}}}+{{\left ( {}4{\:}{{z}}+12{\:}{{y}}+4{\:}{{x}} \right ) {}}}{\:}\sqrt{{{x}}{\:}{{z}}}+{{\left ( {}12{\:}{{z}}+4{\:}{{y}}+4{\:}{{x}} \right ) {}}}{\:}\sqrt{{{x}}{\:}{{y}}}+{{{{z}}}^{2}}+{{\left ( {}6{\:}{{y}}+6{\:}{{x}} \right ) {}}}{\:}{{z}}+{{{{y}}}^{2}}+6{\:}{{x}}{\:}{{y}}+{{{{x}}}^{2}}

``Expression(Integer)``

Polynomials are the commonly used algebraic types in symbolic
computation. Interactive users of FriCAS generally only see one type of
polynomial: . This type represents polynomials in any number of
unspecified variables over a particular coefficient domain . This type
represents its coefficients : only terms with non-zero coefficients are
represented.

In building applications, many other kinds of polynomial representations
are useful. Polynomials may have one variable or multiple variables, the
variables can be named or unnamed, the coefficients can be stored
sparsely or densely. So-called “distributed multivariate polynomials”
store polynomials as coefficients paired with vectors of exponents. This
type is particularly efficient for use in algorithms for solving systems
of non-linear polynomial equations.

The polynomial constructor most familiar to the interactive user is .

::

    (x^2 - x*y^3 +3*y)^2

.. math:: {{{{x}}}^{2}}{\:}{{{{y}}}^{6}}-{6{\:}{{x}}{\:}{{{{y}}}^{4}}}-{2{\:}{{{{x}}}^{3}}{\:}{{{{y}}}^{3}}}+9{\:}{{{{y}}}^{2}}+6{\:}{{{{x}}}^{2}}{\:}{{y}}+{{{{x}}}^{4}}

``Polynomial(Integer)``

If you wish to restrict the variables used, provides polynomials in one
variable.

::

    p: UP(x,INT) := (3*x-1)^2 * (2*x + 8)

.. math:: 18{\:}{{{{x}}}^{3}}+60{\:}{{{{x}}}^{2}}-{46{\:}{{x}}}+8

``UnivariatePolynomial(x, Integer)``

The constructor provides polynomials in one or more specified variables.

::

    m: MPOLY([x,y],INT) := (x^2-x*y^3+3*y)^2 

.. math:: {{{{x}}}^{4}}-{2{\:}{{{{y}}}^{3}}{\:}{{{{x}}}^{3}}}+{{\left ( {}{{{{y}}}^{6}}+6{\:}{{y}} \right ) {}}}{\:}{{{{x}}}^{2}}-{6{\:}{{{{y}}}^{4}}{\:}{{x}}}+9{\:}{{{{y}}}^{2}}

``MultivariatePolynomial([x, y], Integer)``

You can change the way the polynomial appears by modifying the variable
ordering in the explicit list.

::

    m :: MPOLY([y,x],INT) 

.. math:: {{{{x}}}^{2}}{\:}{{{{y}}}^{6}}-{6{\:}{{x}}{\:}{{{{y}}}^{4}}}-{2{\:}{{{{x}}}^{3}}{\:}{{{{y}}}^{3}}}+9{\:}{{{{y}}}^{2}}+6{\:}{{{{x}}}^{2}}{\:}{{y}}+{{{{x}}}^{4}}

``MultivariatePolynomial([y, x], Integer)``

The constructor provides polynomials in one or more specified variables
with the monomials ordered lexicographically.

::

    m :: DMP([y,x],INT) 

.. math:: {{{{y}}}^{6}}{\:}{{{{x}}}^{2}}-{6{\:}{{{{y}}}^{4}}{\:}{{x}}}-{2{\:}{{{{y}}}^{3}}{\:}{{{{x}}}^{3}}}+9{\:}{{{{y}}}^{2}}+6{\:}{{y}}{\:}{{{{x}}}^{2}}+{{{{x}}}^{4}}

``DistributedMultivariatePolynomial([y, x], Integer)``

The constructor is similar except that the monomials are ordered by
total order refined by reverse lexicographic order.

::

    m :: HDMP([y,x],INT) 

.. math:: {{{{y}}}^{6}}{\:}{{{{x}}}^{2}}-{2{\:}{{{{y}}}^{3}}{\:}{{{{x}}}^{3}}}-{6{\:}{{{{y}}}^{4}}{\:}{{x}}}+{{{{x}}}^{4}}+6{\:}{{y}}{\:}{{{{x}}}^{2}}+9{\:}{{{{y}}}^{2}}

``HomogeneousDistributedMultivariatePolynomial([y, x], Integer)``

More generally, the domain constructor allows the user to provide an
arbitrary predicate to define his own term ordering. These last three
constructors are typically used in Gröbner basis applications and when a
flat (that is, non-recursive) display is wanted and the term ordering is
critical for controlling the computation.

FriCAS’s function is usually used to evaluate limits of quotients where
the numerator and denominator both tend to zero or both tend to
infinity. To find the limit of an expression as a real variable tends to
a limit value , enter . Use if the variable is complex. Additional
information and examples of limits are in .

You can take limits of functions with parameters.

::

    g := csc(a*x) / csch(b*x) 

.. math:: \frac{\csc{{{\left ( {}{{a}}{\:}{{x}} \right ) {}}}}}{{\operatorname{csch}}{{{\left ( {}{{b}}{\:}{{x}} \right ) {}}}}}

``Expression(Integer)``

As you can see, the limit is expressed in terms of the parameters.

::

    limit(g,x=0) 

.. math:: \frac{{{b}}}{{{a}}}

``Union(OrderedCompletion(Expression(Integer)), ...)``

A variable may also approach plus or minus infinity:

::

    h := (1 + k/x)^x 

.. math:: {{{{\left ( {}\frac{{{x}}+{{k}}}{{{x}}} \right ) {}}}}^{{{x}}}}

``Expression(Integer)``

Use and to denote :math:`\infty` and :math:`-\infty`.

::

    limit(h,x=%plusInfinity) 

.. math:: {{{e}}^{{{k}}}}

``Union(OrderedCompletion(Expression(Integer)), ...)``

A function can be defined on both sides of a particular value, but may
tend to different limits as its variable approaches that value from the
left and from the right.

::

    limit(sqrt(y^2)/y,y = 0)

.. math:: {{\left \lbrack {}{{leftHandLimit}}=-{1}{,\:}{{rightHandLimit}}=1 \right \rbrack {}}}

``Union(Record(leftHandLimit: Union(OrderedCompletion(Expression(Integer)), failed), rightHandLimit: Union(OrderedCompletion(Expression(Integer)), failed)), ...)``

As approaches along the real axis, tends to .

::

    limit(exp(-1/x^2),x = 0)

.. math:: 0

``Union(OrderedCompletion(Expression(Integer)), ...)``

However, if is allowed to approach along any path in the complex plane,
the limiting value of depends on the path taken because the function has
an essential singularity at . This is reflected in the error message
returned by the function.

::

    complexLimit(exp(-1/x^2),x = 0)

.. math:: {\texttt{"failed"}}

``Union(failed, ...)``

FriCAS also provides power series. By default, FriCAS tries to compute
and display the first ten elements of a series. Use to change the
default value to something else. For the purposes of this book, we have
used this system command to display fewer than ten terms. For more
information about working with series, see .

You can convert a functional expression to a power series by using the
operation . In this example, is expanded in powers of , that is, in
powers of .

::

    series(sin(a*x),x = 0)

.. math:: {{a}}{\:}{{x}}-{\frac{{{{{a}}}^{3}}}{6}{\:}{{{{x}}}^{3}}}+\frac{{{{{a}}}^{5}}}{120}{\:}{{{{x}}}^{5}}-{\frac{{{{{a}}}^{7}}}{5040}{\:}{{{{x}}}^{7}}}+{{\operatorname{O}}}{{\left ( {}{{{{x}}}^{9}} \right ) {}}}

``UnivariatePuiseuxSeries(Expression(Integer), x, 0)``

This expression expands in powers of .

::

    series(sin(a*x),x = %pi/4)

.. math:: \sin{{{\left ( {}\frac{{{a}}{\:}\pi }{4} \right ) {}}}}+{{a}}{\:}\cos{{{\left ( {}\frac{{{a}}{\:}\pi }{4} \right ) {}}}}{\:}{{\left ( {}{{x}}-{\frac{\pi }{4}} \right ) {}}}-{\frac{{{{{a}}}^{2}}{\:}\sin{{{\left ( {}\frac{{{a}}{\:}\pi }{4} \right ) {}}}}}{2}{\:}{{{{\left ( {}{{x}}-{\frac{\pi }{4}} \right ) {}}}}^{2}}}-{\frac{{{{{a}}}^{3}}{\:}\cos{{{\left ( {}\frac{{{a}}{\:}\pi }{4} \right ) {}}}}}{6}{\:}{{{{\left ( {}{{x}}-{\frac{\pi }{4}} \right ) {}}}}^{3}}}+\frac{{{{{a}}}^{4}}{\:}\sin{{{\left ( {}\frac{{{a}}{\:}\pi }{4} \right ) {}}}}}{24}{\:}{{{{\left ( {}{{x}}-{\frac{\pi }{4}} \right ) {}}}}^{4}}+\frac{{{{{a}}}^{5}}{\:}\cos{{{\left ( {}\frac{{{a}}{\:}\pi }{4} \right ) {}}}}}{120}{\:}{{{{\left ( {}{{x}}-{\frac{\pi }{4}} \right ) {}}}}^{5}}-{\frac{{{{{a}}}^{6}}{\:}\sin{{{\left ( {}\frac{{{a}}{\:}\pi }{4} \right ) {}}}}}{720}{\:}{{{{\left ( {}{{x}}-{\frac{\pi }{4}} \right ) {}}}}^{6}}}-{\frac{{{{{a}}}^{7}}{\:}\cos{{{\left ( {}\frac{{{a}}{\:}\pi }{4} \right ) {}}}}}{5040}{\:}{{{{\left ( {}{{x}}-{\frac{\pi }{4}} \right ) {}}}}^{7}}}+{{\operatorname{O}}}{{\left ( {}{{{{\left ( {}{{x}}-{\frac{\pi }{4}} \right ) {}}}}^{8}} \right ) {}}}

````\ UnivariatePuiseuxSeries(Expression(Integer), x, %pi/4)

FriCAS provides series with rational number exponents. The first
argument to is an in-place function that computes the coefficient.
(Recall that the is an infix operator meaning “maps to.”)

::

    series(n +-> (-1)^((3*n - 4)/6)/factorial(n - 1/3),x = 0,4/3..,2)

.. math:: {{{{x}}}^{\frac{4}{3}}}-{\frac{1}{6}{\:}{{{{x}}}^{\frac{10}{3}}}}+{{\operatorname{O}}}{{\left ( {}{{{{x}}}^{4}} \right ) {}}}

``UnivariatePuiseuxSeries(Expression(Integer), x, 0)``

Once you have created a power series, you can perform arithmetic
operations on that series. We compute the Taylor expansion of .

::

    f := series(1/(1-x),x = 0) 

.. math:: 1+{{x}}+{{{{x}}}^{2}}+{{{{x}}}^{3}}+{{{{x}}}^{4}}+{{{{x}}}^{5}}+{{{{x}}}^{6}}+{{{{x}}}^{7}}+{{\operatorname{O}}}{{\left ( {}{{{{x}}}^{8}} \right ) {}}}

``UnivariatePuiseuxSeries(Expression(Integer), x, 0)``

Compute the square of the series.

::

    f ^ 2 

.. math:: 1+2{\:}{{x}}+3{\:}{{{{x}}}^{2}}+4{\:}{{{{x}}}^{3}}+5{\:}{{{{x}}}^{4}}+6{\:}{{{{x}}}^{5}}+7{\:}{{{{x}}}^{6}}+8{\:}{{{{x}}}^{7}}+{{\operatorname{O}}}{{\left ( {}{{{{x}}}^{8}} \right ) {}}}

``UnivariatePuiseuxSeries(Expression(Integer), x, 0)``

The usual elementary functions (, , trigonometric functions, and so on)
are defined for power series.

::

    f := series(1/(1-x),x = 0) 

.. math:: 1+{{x}}+{{{{x}}}^{2}}+{{{{x}}}^{3}}+{{{{x}}}^{4}}+{{{{x}}}^{5}}+{{{{x}}}^{6}}+{{{{x}}}^{7}}+{{\operatorname{O}}}{{\left ( {}{{{{x}}}^{8}} \right ) {}}}

``UnivariatePuiseuxSeries(Expression(Integer), x, 0)``

::

    g := log(f) 

.. math:: {{x}}+\frac{1}{2}{\:}{{{{x}}}^{2}}+\frac{1}{3}{\:}{{{{x}}}^{3}}+\frac{1}{4}{\:}{{{{x}}}^{4}}+\frac{1}{5}{\:}{{{{x}}}^{5}}+\frac{1}{6}{\:}{{{{x}}}^{6}}+\frac{1}{7}{\:}{{{{x}}}^{7}}+\frac{1}{8}{\:}{{{{x}}}^{8}}+{{\operatorname{O}}}{{\left ( {}{{{{x}}}^{9}} \right ) {}}}

``UnivariatePuiseuxSeries(Expression(Integer), x, 0)``

::

    exp(g) 

.. math:: 1+{{x}}+{{{{x}}}^{2}}+{{{{x}}}^{3}}+{{{{x}}}^{4}}+{{{{x}}}^{5}}+{{{{x}}}^{6}}+{{{{x}}}^{7}}+{{\operatorname{O}}}{{\left ( {}{{{{x}}}^{8}} \right ) {}}}

``UnivariatePuiseuxSeries(Expression(Integer), x, 0)``

Here is a way to obtain numerical approximations of from the Taylor
series expansion of . First create the desired Taylor expansion.

::

    f := taylor(exp(x)) 

.. math:: 1+{{x}}+\frac{1}{2}{\:}{{{{x}}}^{2}}+\frac{1}{6}{\:}{{{{x}}}^{3}}+\frac{1}{24}{\:}{{{{x}}}^{4}}+\frac{1}{120}{\:}{{{{x}}}^{5}}+\frac{1}{720}{\:}{{{{x}}}^{6}}+\frac{1}{5040}{\:}{{{{x}}}^{7}}+{{\operatorname{O}}}{{\left ( {}{{{{x}}}^{8}} \right ) {}}}

``UnivariateTaylorSeries(Expression(Integer), x, 0)``

Evaluate the series at the value . As you see, you get a sequence of
partial sums.

::

    eval(f,1.0) 

.. math:: {{\left \lbrack {}{\texttt{1.0}}{,\:}{\texttt{2.0}}{,\:}{\texttt{2.5}}{,\:}{\texttt{2.666666666666666666666666666666666666667}}{,\:}{\texttt{2.708333333333333333333333333333333333333}}{,\:}{\texttt{2.716666666666666666666666666666666666667}}{,\:}{\texttt{2.718055555555555555555555555555555555556}}{,\:}{\texttt{...}} \right \rbrack {}}}

``Stream(Expression(Float))``

Use the FriCAS function to differentiate an expression.

To find the derivative of an expression with respect to a variable ,
enter .

::

    f := exp exp x 

.. math:: {{{e}}^{{{{e}}^{{{x}}}}}}

``Expression(Integer)``

::

    D(f, x) 

.. math:: {{{e}}^{{{x}}}}{\:}{{{e}}^{{{{e}}^{{{x}}}}}}

``Expression(Integer)``

An optional third argument in asks FriCAS for the derivative of . This
finds the fourth derivative of with respect to .

::

    D(f, x, 4) 

.. math:: {{\left ( {}{{{{\left ( {}{{{e}}^{{{x}}}} \right ) {}}}}^{4}}+6{\:}{{{{\left ( {}{{{e}}^{{{x}}}} \right ) {}}}}^{3}}+7{\:}{{{{\left ( {}{{{e}}^{{{x}}}} \right ) {}}}}^{2}}+{{{e}}^{{{x}}}} \right ) {}}}{\:}{{{e}}^{{{{e}}^{{{x}}}}}}

``Expression(Integer)``

You can also compute partial derivatives by specifying the order of
differentiation.

::

    g := sin(x^2 + y) 

.. math:: \sin{{{\left ( {}{{y}}+{{{{x}}}^{2}} \right ) {}}}}

``Expression(Integer)``

::

    D(g, y) 

.. math:: \cos{{{\left ( {}{{y}}+{{{{x}}}^{2}} \right ) {}}}}

``Expression(Integer)``

::

    D(g, [y, y, x, x]) 

.. math:: 4{\:}{{{{x}}}^{2}}{\:}\sin{{{\left ( {}{{y}}+{{{{x}}}^{2}} \right ) {}}}}-{2{\:}\cos{{{\left ( {}{{y}}+{{{{x}}}^{2}} \right ) {}}}}}

``Expression(Integer)``

FriCAS can manipulate the derivatives (partial and iterated) of
expressions involving formal operators. All the dependencies must be
explicit.

This returns since (so far) does not explicitly depend on .

::

    D(F,x)

.. math:: 0

``Polynomial(Integer)``

Suppose that we have a function of , , and , where and are themselves
functions of .

Start by declaring that , , and are operators.

::

    F := operator 'F; x := operator 'x; y := operator 'y

.. math:: {{y}}

``BasicOperator``

You can use , , and in expressions.

::

    a := F(x z, y z, z^2) + x y(z+1) 

.. math:: {{\operatorname{x}}}{{\left ( {}{{\operatorname{y}}}{{\left ( {}{{z}}+1 \right ) {}}} \right ) {}}}+{{\operatorname{F}}}{{\left ( {}{{\operatorname{x}}}{{\left ( {}{{z}} \right ) {}}},{{\operatorname{y}}}{{\left ( {}{{z}} \right ) {}}},{{{{z}}}^{2}} \right ) {}}}

``Expression(Integer)``

Differentiate formally with respect to . The formal derivatives
appearing in are not just formal symbols, but do represent the
derivatives of , , and .

::

    dadz := D(a, z)

.. math:: 2{\:}{{z}}{\:}{{{{F}}}_{{{,}}3}}{{\left ( {}{{\operatorname{x}}}{{\left ( {}{{z}} \right ) {}}},{{\operatorname{y}}}{{\left ( {}{{z}} \right ) {}}},{{{{z}}}^{2}} \right ) {}}}+{{{{y}}}^{{\texttt{,}}}}{{\left ( {}{{z}} \right ) {}}}{\:}{{{{F}}}_{{{,}}2}}{{\left ( {}{{\operatorname{x}}}{{\left ( {}{{z}} \right ) {}}},{{\operatorname{y}}}{{\left ( {}{{z}} \right ) {}}},{{{{z}}}^{2}} \right ) {}}}+{{{{x}}}^{{\texttt{,}}}}{{\left ( {}{{z}} \right ) {}}}{\:}{{{{F}}}_{{{,}}1}}{{\left ( {}{{\operatorname{x}}}{{\left ( {}{{z}} \right ) {}}},{{\operatorname{y}}}{{\left ( {}{{z}} \right ) {}}},{{{{z}}}^{2}} \right ) {}}}+{{{{x}}}^{{\texttt{,}}}}{{\left ( {}{{\operatorname{y}}}{{\left ( {}{{z}}+1 \right ) {}}} \right ) {}}}{\:}{{{{y}}}^{{\texttt{,}}}}{{\left ( {}{{z}}+1 \right ) {}}}

``Expression(Integer)``

You can evaluate the above for particular functional values of , , and .
If is and is , then this evaluates .

::

    eval(eval(dadz, 'x, z +-> exp z), 'y, z +-> log(z+1))

.. math:: \frac{{{\left ( {}2{\:}{{{{z}}}^{2}}+2{\:}{{z}} \right ) {}}}{\:}{{{{F}}}_{{{,}}3}}{{\left ( {}{{{e}}^{{{z}}}},\log{{{\left ( {}{{z}}+1 \right ) {}}}},{{{{z}}}^{2}} \right ) {}}}+{{{{F}}}_{{{,}}2}}{{\left ( {}{{{e}}^{{{z}}}},\log{{{\left ( {}{{z}}+1 \right ) {}}}},{{{{z}}}^{2}} \right ) {}}}+{{\left ( {}{{z}}+1 \right ) {}}}{\:}{{{e}}^{{{z}}}}{\:}{{{{F}}}_{{{,}}1}}{{\left ( {}{{{e}}^{{{z}}}},\log{{{\left ( {}{{z}}+1 \right ) {}}}},{{{{z}}}^{2}} \right ) {}}}+{{z}}+1}{{{z}}+1}

``Expression(Integer)``

You obtain the same result by first evaluating and then differentiating.

::

    eval(eval(a, 'x, z +-> exp z), 'y, z +-> log(z+1)) 

.. math:: {{\operatorname{F}}}{{\left ( {}{{{e}}^{{{z}}}},\log{{{\left ( {}{{z}}+1 \right ) {}}}},{{{{z}}}^{2}} \right ) {}}}+{{z}}+2

``Expression(Integer)``

::

    D(%, z)

.. math:: \frac{{{\left ( {}2{\:}{{{{z}}}^{2}}+2{\:}{{z}} \right ) {}}}{\:}{{{{F}}}_{{{,}}3}}{{\left ( {}{{{e}}^{{{z}}}},\log{{{\left ( {}{{z}}+1 \right ) {}}}},{{{{z}}}^{2}} \right ) {}}}+{{{{F}}}_{{{,}}2}}{{\left ( {}{{{e}}^{{{z}}}},\log{{{\left ( {}{{z}}+1 \right ) {}}}},{{{{z}}}^{2}} \right ) {}}}+{{\left ( {}{{z}}+1 \right ) {}}}{\:}{{{e}}^{{{z}}}}{\:}{{{{F}}}_{{{,}}1}}{{\left ( {}{{{e}}^{{{z}}}},\log{{{\left ( {}{{z}}+1 \right ) {}}}},{{{{z}}}^{2}} \right ) {}}}+{{z}}+1}{{{z}}+1}

``Expression(Integer)``

FriCAS has extensive library facilities for integration.

The first example is the integration of a fraction with denominator that
factors into a quadratic and a quartic irreducible polynomial. The usual
partial fraction approach used by most other computer algebra systems
either fails or introduces expensive unneeded algebraic numbers.

We use a factorization-free algorithm.

::

    integrate((x^2+2*x+1)/((x+1)^6+1),x)

.. math:: \frac{\arctan{{{\left ( {}{{{{x}}}^{3}}+3{\:}{{{{x}}}^{2}}+3{\:}{{x}}+1 \right ) {}}}}}{3}

``Union(Expression(Integer), ...)``

When real parameters are present, the form of the integral can depend on
the signs of some expressions.

Rather than query the user or make sign assumptions, FriCAS returns all
possible answers.

::

    integrate(1/(x^2 + a),x)

.. math:: {{\left \lbrack {}\frac{\log{{{\left ( {}\frac{{{\left ( {}{{{{x}}}^{2}}-{{{a}}} \right ) {}}}{\:}\sqrt{-{{{a}}}}+2{\:}{{a}}{\:}{{x}}}{{{{{x}}}^{2}}+{{a}}} \right ) {}}}}}{2{\:}\sqrt{-{{{a}}}}}{,\:}\frac{\arctan{{{\left ( {}\frac{{{x}}{\:}\sqrt{{{a}}}}{{{a}}} \right ) {}}}}}{\sqrt{{{a}}}} \right \rbrack {}}}

``Union(List(Expression(Integer)), ...)``

The operation generally assumes that all parameters are real. The only
exception is when the integrand has complex valued quantities.

If the parameter is complex instead of real, then the notion of sign is
undefined and there is a unique answer. You can request this answer by
“prepending” the word “complex” to the command name:

::

    complexIntegrate(1/(x^2 + a),x)

.. math:: \frac{\sqrt{-{\frac{1}{{{a}}}}}{\:}\log{{{\left ( {}{{a}}{\:}\sqrt{-{\frac{1}{{{a}}}}}+{{x}} \right ) {}}}}-{\sqrt{-{\frac{1}{{{a}}}}}{\:}\log{{{\left ( {}-{{{a}}{\:}\sqrt{-{\frac{1}{{{a}}}}}}+{{x}} \right ) {}}}}}}{2}

``Expression(Integer)``

The following two examples illustrate the limitations of table-based
approaches. The two integrands are very similar, but the answer to one
of them requires the addition of two new algebraic numbers.

This one is the easy one. The next one looks very similar but the answer
is much more complicated.

::

    integrate(x^3 / (a+b*x)^(1/3),x)

.. math:: \frac{{{\left ( {}120{\:}{{{{b}}}^{3}}{\:}{{{{x}}}^{3}}-{135{\:}{{a}}{\:}{{{{b}}}^{2}}{\:}{{{{x}}}^{2}}}+162{\:}{{{{a}}}^{2}}{\:}{{b}}{\:}{{x}}-{243{\:}{{{{a}}}^{3}}} \right ) {}}}{\:}{{{\sqrt[3]{{{b}}{\:}{{x}}+{{a}}}}}^{2}}}{440{\:}{{{{b}}}^{4}}}

``Union(Expression(Integer), ...)``

Only an algorithmic approach is guaranteed to find what new constants
must be added in order to find a solution.

::

    integrate(1 / (x^3 * (a+b*x)^(1/3)),x)

.. math:: \frac{-{2{\:}{{{{b}}}^{2}}{\:}{{{{x}}}^{2}}{\:}\sqrt{3}{\:}\log{{{\left ( {}{\sqrt[3]{{{a}}}}{\:}{{{\sqrt[3]{{{b}}{\:}{{x}}+{{a}}}}}^{2}}+{{{\sqrt[3]{{{a}}}}}^{2}}{\:}{\sqrt[3]{{{b}}{\:}{{x}}+{{a}}}}+{{a}} \right ) {}}}}}+4{\:}{{{{b}}}^{2}}{\:}{{{{x}}}^{2}}{\:}\sqrt{3}{\:}\log{{{\left ( {}{{{\sqrt[3]{{{a}}}}}^{2}}{\:}{\sqrt[3]{{{b}}{\:}{{x}}+{{a}}}}-{{{a}}} \right ) {}}}}+12{\:}{{{{b}}}^{2}}{\:}{{{{x}}}^{2}}{\:}\arctan{{{\left ( {}\frac{2{\:}\sqrt{3}{\:}{{{\sqrt[3]{{{a}}}}}^{2}}{\:}{\sqrt[3]{{{b}}{\:}{{x}}+{{a}}}}+{{a}}{\:}\sqrt{3}}{3{\:}{{a}}} \right ) {}}}}+{{\left ( {}12{\:}{{b}}{\:}{{x}}-{9{\:}{{a}}} \right ) {}}}{\:}\sqrt{3}{\:}{\sqrt[3]{{{a}}}}{\:}{{{\sqrt[3]{{{b}}{\:}{{x}}+{{a}}}}}^{2}}}{18{\:}{{{{a}}}^{2}}{\:}{{{{x}}}^{2}}{\:}\sqrt{3}{\:}{\sqrt[3]{{{a}}}}}

``Union(Expression(Integer), ...)``

Some computer algebra systems use heuristics or table-driven approaches
to integration. When these systems cannot determine the answer to an
integration problem, they reply “I don’t know.” FriCAS uses a algorithm
for integration. that conclusively proves that an integral cannot be
expressed in terms of elementary functions.

When FriCAS returns an integral sign, it has proved that no answer
exists as an elementary function.

::

    integrate(log(1 + sqrt(a*x + b)) / x,x)

.. math:: \int^{{{x}}} \frac{\log{{{\left ( {}\sqrt{{{b}}+{{\\SYMBOL{a}}}+1} \right ) {}}}}{{{{}}\%D}}{\:}{{d}}{{{}}\%D}

``Union(Expression(Integer), ...)``

FriCAS can handle complicated mixed functions much beyond what you can
find in tables.

Whenever possible, FriCAS tries to express the answer using the
functions present in the integrand.

::

    integrate((sinh(1+sqrt(x+b))+2*sqrt(x+b)) / (sqrt(x+b) * (x + cosh(1+sqrt(x + b)))), x)

.. math:: 2{\:}\log{{{\left ( {}\frac{-{2{\:}\cosh{{{\left ( {}\sqrt{{{x}}+{{b}}}+1 \right ) {}}}}}-{2{\:}{{x}}}}{\sinh{{{\left ( {}\sqrt{{{x}}+{{b}}}+1 \right ) {}}}}-{\cosh{{{\left ( {}\sqrt{{{x}}+{{b}}}+1 \right ) {}}}}}} \right ) {}}}}-{2{\:}\sqrt{{{x}}+{{b}}}}

``Union(Expression(Integer), ...)``

A strong structure-checking algorithm in FriCAS finds hidden algebraic
relationships between functions.

::

    integrate(tan(atan(x)/3),x)

.. math:: \frac{8{\:}\log{{{\left ( {}3{\:}{{{{\left ( {}\tan{{{\left ( {}\frac{\arctan{{{x}}}}{3} \right ) {}}}} \right ) {}}}}^{2}}-{1} \right ) {}}}}-{3{\:}{{{{\left ( {}\tan{{{\left ( {}\frac{\arctan{{{x}}}}{3} \right ) {}}}} \right ) {}}}}^{2}}}+18{\:}{{x}}{\:}\tan{{{\left ( {}\frac{\arctan{{{x}}}}{3} \right ) {}}}}}{18}

``Union(Expression(Integer), ...)``

The discovery of this algebraic relationship is necessary for correct
integration of this function. Here are the details:

#. If :math:`x=\tan t` and :math:`g=\tan (t/3)` then the following
   algebraic relation is true:

   .. math:: {g^3-3xg^2-3g+x=0}

#. Integrate using this algebraic relation; this produces:

   .. math:: \frac{(24g^2 - 8)\log(3g^2 - 1) + (81x^2 + 24)g^2 + 72xg - 27x^2 - 16}  {54g^2 - 18}

#. Rationalize the denominator, producing:

   .. math:: \frac{8\log(3g^2-1) - 3g^2 + 18xg + 16}{18}

    Replace by the initial definition :math:`g = \tan(\arctan(x)/3)` to
   produce the final result.

This is an example of a mixed function where the algebraic layer is over
the transcendental one.

::

    integrate((x + 1) / (x*(x + log x) ^ (3/2)), x)

.. math:: -{\frac{2{\:}\sqrt{\log{{{x}}}+{{x}}}}{\log{{{x}}}+{{x}}}}

``Union(Expression(Integer), ...)``

While incomplete for non-elementary functions, FriCAS can handle some of
them.

::

    integrate(exp(-x^2) * erf(x) / (erf(x)^3 - erf(x)^2 - erf(x) + 1),x)

.. math:: \frac{{{\left ( {}{\operatorname{erf}}{{{x}}}-{1} \right ) {}}}{\:}\sqrt{\pi }{\:}\log{{{\left ( {}\frac{{\operatorname{erf}}{{{x}}}-{1}}{{\operatorname{erf}}{{{x}}}+1} \right ) {}}}}-{2{\:}\sqrt{\pi }}}{8{\:}{\operatorname{erf}}{{{x}}}-{8}}

``Union(Expression(Integer), ...)``

More examples of FriCAS’s integration capabilities are discussed in .

The general approach used in integration also carries over to the
solution of linear differential equations.

Let’s solve some differential equations. Let be the unknown function in
terms of .

::

    y := operator 'y 

.. math:: {{y}}

``BasicOperator``

Here we solve a third order equation with polynomial coefficients.

::

    deq := x^3 * D(y x, x, 3) + x^2 * D(y x, x, 2) - 2 * x * D(y x, x) + 2 * y x = 2 * x^4 

.. math:: {{{{x}}}^{3}}{\:}{{{{y}}}^{{\texttt{,,,}}}}{{\left ( {}{{x}} \right ) {}}}+{{{{x}}}^{2}}{\:}{{{{y}}}^{{\texttt{,,}}}}{{\left ( {}{{x}} \right ) {}}}-{2{\:}{{x}}{\:}{{{{y}}}^{{\texttt{,}}}}{{\left ( {}{{x}} \right ) {}}}}+2{\:}{{\operatorname{y}}}{{\left ( {}{{x}} \right ) {}}}=2{\:}{{{{x}}}^{4}}

``Equation(Expression(Integer))``

::

    solve(deq, y, x) 

.. math:: {{\left \lbrack {}{{particular}}=\frac{{{{{x}}}^{5}}-{10{\:}{{{{x}}}^{3}}}+20{\:}{{{{x}}}^{2}}+4}{15{\:}{{x}}}{,\:}{{basis}}={{\left \lbrack {}\frac{2{\:}{{{{x}}}^{3}}-{3{\:}{{{{x}}}^{2}}}+1}{{{x}}}{,\:}\frac{{{{{x}}}^{3}}-{1}}{{{x}}}{,\:}\frac{{{{{x}}}^{3}}-{3{\:}{{{{x}}}^{2}}}-{1}}{{{x}}} \right \rbrack {}}} \right \rbrack {}}}

``Union(Record(particular: Expression(Integer), basis: List(Expression(Integer))), ...)``

Here we find all the algebraic function solutions of the equation.

::

    deq := (x^2 + 1) * D(y x, x, 2) + 3 * x * D(y x, x) + y x = 0 

.. math:: {{\left ( {}{{{{x}}}^{2}}+1 \right ) {}}}{\:}{{{{y}}}^{{\texttt{,,}}}}{{\left ( {}{{x}} \right ) {}}}+3{\:}{{x}}{\:}{{{{y}}}^{{\texttt{,}}}}{{\left ( {}{{x}} \right ) {}}}+{{\operatorname{y}}}{{\left ( {}{{x}} \right ) {}}}=0

``Equation(Expression(Integer))``

::

    solve(deq, y, x) 

.. math:: {{\left \lbrack {}{{particular}}=0{,\:}{{basis}}={{\left \lbrack {}\frac{1}{\sqrt{{{{{x}}}^{2}}+1}}{,\:}\frac{\log{{{\left ( {}\sqrt{{{{{x}}}^{2}}+1}-{{{x}}} \right ) {}}}}}{\sqrt{{{{{x}}}^{2}}+1}} \right \rbrack {}}} \right \rbrack {}}}

``Union(Record(particular: Expression(Integer), basis: List(Expression(Integer))), ...)``

Coefficients of differential equations can come from arbitrary constant
fields. For example, coefficients can contain algebraic numbers.

This example has solutions whose logarithmic derivative is an algebraic
function of degree two.

::

    eq := 2*x^3 * D(y x,x,2) + 3*x^2 * D(y x,x) - 2 * y x

.. math:: 2{\:}{{{{x}}}^{3}}{\:}{{{{y}}}^{{\texttt{,,}}}}{{\left ( {}{{x}} \right ) {}}}+3{\:}{{{{x}}}^{2}}{\:}{{{{y}}}^{{\texttt{,}}}}{{\left ( {}{{x}} \right ) {}}}-{2{\:}{{\operatorname{y}}}{{\left ( {}{{x}} \right ) {}}}}

``Expression(Integer)``

::

    solve(eq,y,x).basis

.. math:: {{\left \lbrack {}{{{e}}^{-{\frac{2}{\sqrt{{{x}}}}}}}{,\:}{{{e}}^{\frac{2}{\sqrt{{{x}}}}}} \right \rbrack {}}}

``List(Expression(Integer))``

Here’s another differential equation to solve.

::

    deq := D(y x, x) = y(x) / (x + y(x) * log y x) 

.. math:: {{{{y}}}^{{\texttt{,}}}}{{\left ( {}{{x}} \right ) {}}}=\frac{{{\operatorname{y}}}{{\left ( {}{{x}} \right ) {}}}}{{{\operatorname{y}}}{{\left ( {}{{x}} \right ) {}}}{\:}\log{{{\operatorname{y}}}{{\left ( {}{{x}} \right ) {}}}}+{{x}}}

``Equation(Expression(Integer))``

::

    solve(deq, y, x) 

.. math:: \frac{{{\operatorname{y}}}{{\left ( {}{{x}} \right ) {}}}{\:}{{{{\left ( {}\log{{{\operatorname{y}}}{{\left ( {}{{x}} \right ) {}}}} \right ) {}}}}^{2}}-{2{\:}{{x}}}}{2{\:}{{\operatorname{y}}}{{\left ( {}{{x}} \right ) {}}}}

``Union(Expression(Integer), ...)``

Rather than attempting to get a closed form solution of a differential
equation, you instead might want to find an approximate solution in the
form of a series.

Let’s solve a system of nonlinear first order equations and get a
solution in power series. Tell FriCAS that is also an operator.

::

    x := operator 'x

.. math:: {{x}}

``BasicOperator``

Here are the two equations forming our system.

::

    eq1 := D(x(t), t) = 1 + x(t)^2

.. math:: {{{{x}}}^{{\texttt{,}}}}{{\left ( {}{{t}} \right ) {}}}={{{{\operatorname{x}}}{{\left ( {}{{t}} \right ) {}}}}^{2}}+1

``Equation(Expression(Integer))``

::

    eq2 := D(y(t), t) = x(t) * y(t)

.. math:: {{{{y}}}^{{\texttt{,}}}}{{\left ( {}{{t}} \right ) {}}}={{\operatorname{x}}}{{\left ( {}{{t}} \right ) {}}}{\:}{{\operatorname{y}}}{{\left ( {}{{t}} \right ) {}}}

``Equation(Expression(Integer))``

We can solve the system around with the initial conditions and . Notice
that since we give the unknowns in the order , the answer is a list of
two series in the order .

::

    seriesSolve([eq2, eq1], [x, y], t = 0, [y(0) = 1, x(0) = 0])

Compiling function Expression(Integer),t,0)) ->
UnivariateTaylorSeries(Expression( Integer),t,0)

Compiling function Expression(Integer),t,0)) ->
UnivariateTaylorSeries(Expression( Integer),t,0)

.. math:: {{\left \lbrack {}{{t}}+\frac{1}{3}{\:}{{{{t}}}^{3}}+\frac{2}{15}{\:}{{{{t}}}^{5}}+\frac{17}{315}{\:}{{{{t}}}^{7}}+{{\operatorname{O}}}{{\left ( {}{{{{t}}}^{8}} \right ) {}}}{,\:}1+\frac{1}{2}{\:}{{{{t}}}^{2}}+\frac{5}{24}{\:}{{{{t}}}^{4}}+\frac{61}{720}{\:}{{{{t}}}^{6}}+{{\operatorname{O}}}{{\left ( {}{{{{t}}}^{8}} \right ) {}}} \right \rbrack {}}}

``List(UnivariateTaylorSeries(Expression(Integer), t, 0))``

FriCAS also has state-of-the-art algorithms for the solution of systems
of polynomial equations. When the number of equations and unknowns is
the same, and you have no symbolic coefficients, you can use for real
roots and for complex roots. In each case, you tell FriCAS how accurate
you want your result to be. All operations in the family return answers
in the form of a list of solution sets, where each solution set is a
list of equations.

A system of two equations involving a symbolic parameter .

::

    S(t) == [x^2-2*y^2 - t,x*y-y-5*x + 5]

Find the real roots of with rational arithmetic, correct to within .

::

    solve(S(19),1/10^20)

Compiling function S with type PositiveInteger -> List(Polynomial(
Integer))

.. math:: {{\left \lbrack {}{{\left \lbrack {}{{y}}=5{,\:}{{x}}=-{\frac{80336736493669365924 189585}{96714065569170333976 49408}} \right \rbrack {}}}{,\:}{{\left \lbrack {}{{y}}=5{,\:}{{x}}=\frac{80336736493669365924 189585}{96714065569170333976 49408} \right \rbrack {}}} \right \rbrack {}}}

``List(List(Equation(Polynomial(Fraction(Integer)))))``

Find the complex roots of with floating point coefficients to digits
accuracy in the mantissa.

::

    complexSolve(S(19),10.e-20)

.. math:: {{\left \lbrack {}{{\left \lbrack {}{{y}}={\texttt{5.0}}{,\:}{{x}}={\texttt{8.306623862918074852584262744905695155698151691481840582865006639146088}} \right \rbrack {}}}{,\:}{{\left \lbrack {}{{y}}={\texttt{5.0}}{,\:}{{x}}=-{{\texttt{8.306623862918074852584262744905695155698}}} \right \rbrack {}}}{,\:}{{\left \lbrack {}{{y}}=-{{\texttt{3.0}}{\:}{i}}{,\:}{{x}}={\texttt{1.0}} \right \rbrack {}}}{,\:}{{\left \lbrack {}{{y}}={\texttt{3.0}}{\:}{i}{,\:}{{x}}={\texttt{1.0}} \right \rbrack {}}} \right \rbrack {}}}

``List(List(Equation(Polynomial(Complex(Float)))))``

If a system of equations has symbolic coefficients and you want a
solution in radicals, try .

::

    radicalSolve(S(a),[x,y])

Compiling function S with type Variable(a) -> List(Polynomial( Integer))

.. math:: {{\left \lbrack {}{{\left \lbrack {}{{x}}=-{\sqrt{{{a}}+50}}{,\:}{{y}}=5 \right \rbrack {}}}{,\:}{{\left \lbrack {}{{x}}=\sqrt{{{a}}+50}{,\:}{{y}}=5 \right \rbrack {}}}{,\:}{{\left \lbrack {}{{x}}=1{,\:}{{y}}=\frac{\sqrt{-{{{a}}}+1}}{\sqrt{2}} \right \rbrack {}}}{,\:}{{\left \lbrack {}{{x}}=1{,\:}{{y}}=-{\frac{\sqrt{-{{{a}}}+1}}{\sqrt{2}}} \right \rbrack {}}} \right \rbrack {}}}

``List(List(Equation(Expression(Integer))))``

For systems of equations with symbolic coefficients, you can apply ,
listing the variables that you want FriCAS to solve for. For polynomial
equations, a solution cannot usually be expressed solely in terms of the
other variables. Instead, the solution is presented as a “triangular”
system of equations, where each polynomial has coefficients involving
only the succeeding variables. This is analogous to converting a linear
system of equations to “triangular form”.

A system of three equations in five variables.

::

    eqns := [x^2 - y + z,x^2*z + x^4 - b*y, y^2 *z - a - b*x]

.. math:: {{\left \lbrack {}{{z}}-{{{y}}}+{{{{x}}}^{2}}{,\:}{{{{x}}}^{2}}{\:}{{z}}-{{{b}}{\:}{{y}}}+{{{{x}}}^{4}}{,\:}{{{{y}}}^{2}}{\:}{{z}}-{{{b}}{\:}{{x}}}-{{{a}}} \right \rbrack {}}}

``List(Polynomial(Integer))``

Solve the system for unknowns , reducing the solution to triangular
form.

::

    solve(eqns,[x,y,z])

.. math:: {{\left \lbrack {}{{\left \lbrack {}{{x}}=-{\frac{{{a}}}{{{b}}}}{,\:}{{y}}=0{,\:}{{z}}=-{\frac{{{{{a}}}^{2}}}{{{{{b}}}^{2}}}} \right \rbrack {}}}{,\:}{{\left \lbrack {}{{x}}=\frac{{{{{z}}}^{3}}+2{\:}{{b}}{\:}{{{{z}}}^{2}}+{{{{b}}}^{2}}{\:}{{z}}-{{{a}}}}{{{b}}}{,\:}{{y}}={{z}}+{{b}}{,\:}{{{{z}}}^{6}}+4{\:}{{b}}{\:}{{{{z}}}^{5}}+6{\:}{{{{b}}}^{2}}{\:}{{{{z}}}^{4}}+{{\left ( {}4{\:}{{{{b}}}^{3}}-{2{\:}{{a}}} \right ) {}}}{\:}{{{{z}}}^{3}}+{{\left ( {}{{{{b}}}^{4}}-{4{\:}{{a}}{\:}{{b}}} \right ) {}}}{\:}{{{{z}}}^{2}}-{2{\:}{{a}}{\:}{{{{b}}}^{2}}{\:}{{z}}}-{{{{{b}}}^{3}}}+{{{{a}}}^{2}}=0 \right \rbrack {}}} \right \rbrack {}}}

``List(List(Equation(Fraction(Polynomial(Integer)))))``

We conclude our tour of FriCAS with a brief discussion of . System
commands are special statements that start with a closing parenthesis
(). They are used to control or display your FriCAS environment, start
the system, issue operating system commands and leave FriCAS. For
example, is used to issue commands to the operating system from FriCAS.
Here is a brief description of some of these commands. For more
information on specific commands, see .

Perhaps the most important user command is the command that initializes
your environment. Every section and subsection in this book has an
invisible that is read prior to the examples given in the section. gives
you a fresh, empty environment with no user variables defined and the
step number reset to . The command can also be used to selectively clear
values and properties of system variables.

Another useful system command is . A preferred way to develop an
application in FriCAS is to put your interactive commands into a file,
say **my.input** file. To get FriCAS to read this file, you use the
system command . If you need to make changes to your approach or
definitions, go into your favorite editor, change **my.input**, then
again.

Other system commands include: , to display previous input and/or output
lines; , to display properties and values of workspace variables; and .

Issue to get a list of FriCAS objects that contain a given substring in
their name.

::

    )what operations integrate

Operations whose names satisfy the above pattern(s):

HermiteIntegrate algintegrate complexIntegrate expintegrate fintegrate
infieldIntegrate integrate integrateIfCan integrate\_sols
internalIntegrate internalIntegrate0 lambintegrate lazyGintegrate
lazyIntegrate lfintegrate monomialIntegrate palgintegrate
pmComplexintegrate pmintegrate primintegrate

To get more information about an operation such as integrate , issue the
command )display op integrate

A useful system command is . Sometimes while computing interactively
with FriCAS, you make a mistake and enter an incorrect definition or
assignment. Or perhaps you need to try one of several alternative
approaches, one after another, to find the best way to approach an
application. For this, you will find the facility of FriCAS helpful.

System command means “undo back to step ”; it restores the values of
user variables to those that existed immediately after input expression
was evaluated. Similarly, undoes changes caused by the last input
expressions. Once you have done an , you can continue on from there, or
make a change and **redo** all your input expressions from the point of
the forward. The is completely general: it changes the environment like
any user expression. Thus you can any previous undo.

Here is a sample dialogue between user and FriCAS.

“Let me define two mutually dependent functions and piece-wise.”

::

    f(0) == 1; g(0) == 1

“Here is the general term for .”

::

    f(n) == e/2*f(n-1) - x*g(n-1)

“And here is the general term for .”

::

    g(n) == -x*f(n-1) + d/3*g(n-1)

“What is value of ?”

::

    f(3)

Compiling function g with type Integer -> Polynomial(Fraction( Integer))

Compiling function g as a recurrence relation.

Compiling function g with type Integer -> Polynomial(Fraction( Integer))

Compiling function g as a recurrence relation.

Compiling function f with type Integer -> Polynomial(Fraction( Integer))

Compiling function f as a recurrence relation.

.. math:: -{{{{{x}}}^{3}}}+{{\left ( {}{{e}}+\frac{1}{3}{\:}{{d}} \right ) {}}}{\:}{{{{x}}}^{2}}+{{\left ( {}-{\frac{1}{4}{\:}{{{{e}}}^{2}}}-{\frac{1}{6}{\:}{{d}}{\:}{{e}}}-{\frac{1}{9}{\:}{{{{d}}}^{2}}} \right ) {}}}{\:}{{x}}+\frac{1}{8}{\:}{{{{e}}}^{3}}

``Polynomial(Fraction(Integer))``

“Hmm, I think I want to define differently. Undo to the environment
right after I defined .”

::

    )undo 2

“Here is how I think I want to be defined instead.”

::

    f(n) == d/3*f(n-1) - x*g(n-1)

1 old definition(s) deleted for function or rule f

Redo the computation from expression forward.

::

    )undo )redo

“I want my old definition of after all. Undo the undo and restore the
environment to that immediately after .”

::

    )undo 4

“Check that the value of is restored.”

::

    f(3)

Compiling function g with type Integer -> Polynomial(Fraction( Integer))

Compiling function g as a recurrence relation.

Compiling function g with type Integer -> Polynomial(Fraction( Integer))

Compiling function g as a recurrence relation.

Compiling function f with type Integer -> Polynomial(Fraction( Integer))

Compiling function f as a recurrence relation.

.. math:: -{{{{{x}}}^{3}}}+{{\left ( {}{{e}}+\frac{1}{3}{\:}{{d}} \right ) {}}}{\:}{{{{x}}}^{2}}+{{\left ( {}-{\frac{1}{4}{\:}{{{{e}}}^{2}}}-{\frac{1}{6}{\:}{{d}}{\:}{{e}}}-{\frac{1}{9}{\:}{{{{d}}}^{2}}} \right ) {}}}{\:}{{x}}+\frac{1}{8}{\:}{{{{e}}}^{3}}

``Polynomial(Fraction(Integer))``

After you have gone off on several tangents, then backtracked to
previous points in your conversation using , you might want to save all
the “correct” input commands you issued, disregarding those undone. The
system command writes a clean straight-line program onto the file
**mynew.input** on your disk.

This concludes your tour of FriCAS. To disembark, issue the system
command to leave FriCAS and return to the operating system.

.. [1]
   FriCAS actually has two forms of assignment: *immediate* assignment,
   as discussed here, and *delayed assignment*. See for details.

.. [2]
   Conversion is discussed in detail in .

.. [3]
   See for a discussion on how to write your own macros.

.. [4]
   PostScript is a trademark of Adobe Systems Incorporated, registered
   in the United States.

.. [5]
   See and for additional information on floating-point types.

.. [6]
   See for details.

.. [7]
   See for more details. Heaps are also examples of data structures
   called . Other bag data structures are , , and .

.. [8]
   Example of binary tree types are (see , , , and (see ).

.. [9]
   See for more details.

.. [10]
   See for details.

.. [11]
   For examples of tables, see (), , (), (), (), (), and ().

.. [12]
   See for details.

.. [13]
   See for details.

.. [14]
   See for more information about arrays. For more information about
   FriCAS’s linear algebra facilities, see , , , ,  (computation of
   eigenvalues and eigenvectors) , and  (solution of linear and
   polynomial equations) .
