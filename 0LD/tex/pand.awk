BEGIN {}
END {}

/^\\begin{fricasmath}/ {
        print "$$"
        next                                                                    
}                                                                               
                                                                                
/^\\end{fricasmath}/ {                                                          
        print "$$"                                                    
        next                                                                    
} 

/^\\begin{spadsrc}/ {
        print "\\begin{verbatim}"
        next                                                                    
}                                                                               
                                                                                
/^\\end{spadsrc}/ {                                                          
        print "\\end{verbatim}"                                                    
        next                                                                    
} 



{
gsub("begin{MATRIX}","begin{bmatrix}",$0);
gsub("end{MATRIX}","end{bmatrix}",$0);
print $0
}
