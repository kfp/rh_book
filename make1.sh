cp -v style.tex tex_new/

awk -f pand.awk tex_orig/ug08.tex > tex_new/ug08.tex

pandoc  --mathjax -f latex -t rst tex_new/ug08.tex > rst/ug08.rst

cp rst/ug08.rst sphinx/
cd sphinx
make html