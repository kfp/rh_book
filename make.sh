cp -v style.tex tex_new/

for a in tex_orig/ug*.tex; do b=$(basename $a) ; \
  awk -f pand.awk tex_orig/$b > tex_new/$b ; done


for a in tex_new/ug*.tex; do b=$(basename $a .tex) ; \
  pandoc  --mathjax -f latex -t rst $a > rst/$b.rst ; done


for a in rst/ug*.rst; do cp -v $a sphinx/ ; done


cat <<'EOF' > sphinx/index.rst
.. FriCAS Book documentation master file, created by
   sphinx-quickstart on Thu Mar 23 19:49:44 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FriCAS Book's documentation!
=======================================

Contents:

.. toctree::
   :maxdepth: 2

   ug00
   ug01
   ug02
   ug03
   ug04
   ug05
   ug06
   ug07
   ug08
   ug10
   ug11
   ug12
   ug13
   ug14
   ug15
   ug16
   ug21

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
EOF

cd sphinx
make html
