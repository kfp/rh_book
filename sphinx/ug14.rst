Browse
======

ugBrowse This chapter discusses the component of . We suggest you invoke
FriCAS and work through this chapter, section by section, following our
examples to gain some familiarity with .

The Front Page: Searching the Library
-------------------------------------

ugBrowseStart To enter , click on **Browse** on the top level page of to
get the *front page* of .

(324,180)

To use this page, you first enter a into the input area at the top, then
click on one of the buttons below. We show the use of each of the
buttons by example.

Constructors
^^^^^^^^^^^^

First enter the search string Matrix into the input area and click on
**Constructors**. What you get is the *constructor page* for . We show
and describe this page in detail in . By convention, FriCAS does a
case-insensitive search for a match. Thus matrix is just as good as
Matrix, has the same effect as MaTrix, and so on. We recommend that you
generally use small letters for names however. A search string with only
capital letters has a special meaning (see ).

Click on to return to the front page.

Use the symbol “” in search strings as a . A wild card matches any
substring, including the empty string. For example, enter the search
string matrix\* into the input area and click on **Constructors**. [1]_
What you get is a table of all constructors whose names contain the
string “matrix.”

(324,180)

All constructors containing the string are listed, whether or . You can
hide the names of the unexposed constructors by clicking on the
*=*\ **unexposed** button in the *Views* panel at the bottom of the
window. (The button will change to **exposed** *only*.)

One of the names in this table is . Click on . What you get is again the
constructor page for . As you see, gives you a large network of
information in which there are many ways to reach the same pages.

Again click on the to return to the table of constructors whose names
contain matrix. Below the table is a *Views* panel. This panel contains
buttons that let you view constructors in different ways. To learn about
views of constructors, skip to .

Click on to return to the front page.

Operations
^^^^^^^^^^

Enter matrix into the input area and click on **Operations**. This time
you get a table of *operations* whose names end with matrix or Matrix.

(324,180)

If you select an operation name, you go to a page describing all the
operations in FriCAS of that name. At the bottom of an operation page is
another kind of *Views* panel, one for operation pages. To learn more
about these views, skip to .

Click on to return to the front page.

Attributes
^^^^^^^^^^

This button gives you a table of attribute names that match the search
string. Enter the search string and click on **Attributes** to get a
list of all system attributes.

Click on to return to the front page.

(324,180)

Again there is a *Views* panel at the bottom with buttons that let you
view the attributes in different ways.

General
^^^^^^^

This button does a general search for all constructor, operation, and
attribute names matching the search string. Enter the search string into
the input area. Click on **General** to find all constructs that have
matrix as a part of their name.

(324,180)

The summary gives you all the names under a heading when the number of
entries is less than 10. Click on to return to the front page.

Documentation
^^^^^^^^^^^^^

Again enter the search key matrix\* and this time click on
**Documentation**. This search matches any constructor, operation, or
attribute name whose documentation contains a substring matching matrix.

(324,180)

Click on to return to the front page.

Complete
^^^^^^^^

This search combines both **General** and **Documentation**.

(324,180)

The Constructor Page
--------------------

ugBrowseDomain In this section we look in detail at a constructor page
for domain . Enter matrix into the input area on the main page and click
on **Constructors**.

(324,180)

The header part tells you that has abbreviation and one argument called
R that must be a domain of category . Just what domains can be arguments
of ? To find this out, click on the R on the second line of the heading.
What you get is a table of all acceptable domain parameter values of R,
or a table of in FriCAS.

(324,180)

Click on to return to the constructor page for .

If you have access to the source code of FriCAS, the third line of the
heading gives you the name of the source file containing the definition
of . Click on it to pop up an editor window containing the source code
of .

(324,168)

We recommend that you leave the editor window up while working through
this chapter as you occasionally may want to refer to it.

Constructor Page Buttons
~~~~~~~~~~~~~~~~~~~~~~~~

ugBrowseDomainButtons We examine each button on this page in order.

Description
^^^^^^^^^^^

Click here to bring up a page with a brief description of constructor .
If you have access to system source code, note that these comments can
be found directly over the constructor definition.

(324,180)

Operations
^^^^^^^^^^

Click here to get a table of operations exported by . You may wish to
widen the window to have multiple columns as below.

(324,180)

If you click on an operation name, you bring up a description page for
the operations. For a detailed description of these pages, skip to .

Attributes
^^^^^^^^^^

Click here to get a table of the two attributes exported by : and .
These are two computational properties that result from being regarded
as a data structure.

(324,180)

Examples
^^^^^^^^

Click here to get an *examples page* with examples of operations to
create and manipulate matrices.

(324,180)

Read through this section. Try selecting the various buttons. Notice
that if you click on an operation name, such as , you bring up a
description page for that operation from .

Example pages have several examples of FriCAS commands. Each example has
an active button to its left. Click on it! A pre-computed answer is
pasted into the page immediately following the command. If you click on
the button a second time, the answer disappears. This button thus acts
as a toggle: “now you see it; now you don’t.”

Note also that the FriCAS commands themselves are active. If you want to
see FriCAS execute the command, then click on it! A new FriCAS window
appears on your screen and the command is executed.

Exports
^^^^^^^

Click here to see a page describing the exports of exactly as described
by the source code.

(324,180)

As you see, declares that it exports all the operations and attributes
exported by category . In addition, two operations, and , are explicitly
exported.

To learn a little about the structure of FriCAS, we suggest you do the
following exercise. Otherwise, go on to the next section. explicitly
exports only two operations. The other operations are thus exports of .
In general, operations are usually not explicitly exported by a domain.
Typically they are from several different categories. Let’s find out
from where the operations of come.

#. Click on **MatrixCategory**, then on **Exports**. Here you see that
   **MatrixCategory** explicitly exports many matrix operations. Also,
   it inherits its operations from .

#. Click on **TwoDimensionalArrayCategory**, then on **Exports**. Here
   you see explicit operations dealing with rows and columns. In
   addition, it inherits operations from .

#. Click on and then click on **Object**, then on **Exports**, where you
   see there are no exports.

#. Click on repeatedly to return to the constructor page for .

Related Operations
^^^^^^^^^^^^^^^^^^

Click here bringing up a table of operations that are exported by but
not by itself.

(324,180)

To see a table of such packages, use the **Relatives** button on the
**Cross Reference** page described next.

Cross Reference
~~~~~~~~~~~~~~~

ugBrowseCrossReference Click on the **Cross Reference** button on the
main constructor page for . This gives you a page having various cross
reference information stored under the respective buttons.

(324,180)

Parents
^^^^^^^

The parents of a domain are the same as the categories mentioned under
the **Exports** button on the first page. Domain has only one parent but
in general a domain can have any number.

Ancestors
^^^^^^^^^

The of a constructor consist of its parents, the parents of its parents,
and so on. Did you perform the exercise in the last section under
**Exports**? If so, you see here all the categories you found while
ascending the **Exports** chain for .

Relatives
^^^^^^^^^

The of a domain constructor are package constructors that provide
operations in addition to those by the domain.

Try this exercise.

#. Click on **Relatives**, bringing up a list of .

#. Click on **LinearSystemMatrixPackage** bringing up its constructor
   page. [2]_

#. Click on **Operations**. Here you see , an operation also exported by
   itself.

#. Click on **rank**. This has two arguments and thus is different from
   the from .

#. Click on to return to the list of operations for the package .

#. Click on **solve** to bring up a for linear systems of equations.

#. Click on several times to return to the cross reference page for .

Dependents
^^^^^^^^^^

The of a constructor are those or that mention that constructor either
as an argument or in its .

If you click on **Dependents** two entries may surprise you: and . This
happens because , as it turns out, appears in signatures of operations
exported by these domains.

Search Path
^^^^^^^^^^^

The term refers to the *search order* for functions. If you are an
expert user or curious about how the FriCAS system works, try the
following exercise. Otherwise, you best skip this button and go on to
**Users**.

Clicking on **Search Path** gives you a list of domain constructors: , ,
, , , , , , . What are these constructors and how are they used?

We explain by an example. Suppose you create a matrix using the
interpreter, then ask for its . FriCAS must then find a function
implementing the operation for matrices. The first place FriCAS looks
for is in the domain.

If not there, the search path of tells FriCAS where else to look.
Associated with the matrix domain are eight other search path domains.
Their order is important. FriCAS first searches the first one, . If not
there, it searches the second . And so on.

Where do these *search path constructors* come from? The source code for
contains this syntax for the of : [3]_

::

    InnerIndexedTwoDimensionalArray(R,mnRow,mnCol,Row,Col)
       add ...

where the “...” denotes all the code that follows. In English, this
means: “The functions for matrices are defined as those from domain
augmented by those defined in ‘...’,” where the latter take precedence.

This explains . The other names, those with names ending with an
ampersand are for categories to which belongs. Default packages are
ordered by the notion of “closest ancestor.”

Users
^^^^^

A user of is any constructor that uses in its implementation. For
example, is a user of ; it exports several operations that take matrices
as arguments or return matrices as values. [4]_

Uses
^^^^

A of is any constructor that uses in its implementation. This
information, like that for clients, is gathered from run-time
structures. [5]_

Cross reference pages for categories have some different buttons on
them. Starting with the constructor page of , click on producing its
constructor page. Click on **Cross Reference**, producing the
cross-reference page for . Here are buttons **Parents** and
**Ancestors** similar to the notion for domains, except for categories
the relationship between parent and child is defined through .

Children
^^^^^^^^

Category hierarchies go both ways. There are children as well as
parents. A child can have any number of parents, but always at least
one. Every category is therefore a descendant of exactly one category: .

Descendants
^^^^^^^^^^^

These are children, children of children, and so on.

Category hierarchies are complicated by the fact that categories take
parameters. Where a parameterized category fits into a hierarchy *may*
depend on values of its parameters. In general, the set of categories in
FriCAS forms a *directed acyclic graph*, that is, a graph with directed
arcs and no cycles.

Domains
^^^^^^^

This produces a table of all domain constructors that can possibly be
rings (members of category ). Some domains are unconditional rings.
Others are rings for some parameters and not for others. To find out
which, select the **conditions** button in the views panel. For example,
is a ring if R is a ring.

Views Of Constructors
~~~~~~~~~~~~~~~~~~~~~

ugBrowseViewsOfConstructors Below every constructor table page is a
*Views* panel. As an example, click on **Cross Reference** from the
constructor page of , then on **Benefactors** to produce a short table
of constructor names.

The *Views* panel is at the bottom of the page. Two items, *names* and
*conditions,* are in italics. Others are active buttons. The active
buttons are those that give you useful alternative views on this table
of constructors. Once you select a view, you notice that the button
turns off (becomes italicized) so that you cannot reselect it.

names
^^^^^

This view gives you a table of names. Selecting any of these names
brings up the constructor page for that constructor.

abbrs
^^^^^

This view gives you a table of abbreviations, in the same order as the
original constructor names. Abbreviations are in capitals and are
limited to 7 characters. They can be used interchangeably with
constructor names in input areas.

kinds
^^^^^

This view organizes constructor names into the three kinds: categories,
domains and packages.

files
^^^^^

This view gives a table of file names for the source code of the
constructors in alphabetic order after removing duplicates.

parameters
^^^^^^^^^^

This view presents constructors with the arguments. This view of the
benefactors of shows that uses as many as five different domains in its
implementation.

filter
^^^^^^

This button is used to refine the list of names or abbreviations.
Starting with the *names* view, enter m\* into the input area and click
on **filter**. You then get a shorter table with only the names
beginning with m.

documentation
^^^^^^^^^^^^^

This gives you documentation for each of the constructors.

conditions
^^^^^^^^^^

This page organizes the constructors according to predicates. The view
is not available for your example page since all constructors are
unconditional. For a table with conditions, return to the **Cross
Reference** page for , click on **Ancestors**, then on **conditions** in
the view panel. This page shows you that and are ancestors of only if R
belongs to category .

Giving Parameters to Constructors
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ugBrowseGivingParameters Notice the input area at the bottom of the
constructor page. If you leave this blank, then the information you get
is for the domain constructor , that is, for an arbitrary underlying
domain R.

In general, however, the exports and other information *do* usually
depend on the actual value of R. For example, exports the operation only
if the domain R is a . To see this, try this from the main constructor
page:

#. Enter Integer into the input area at the bottom of the page.

#. Click on **Operations**, producing a table of operations. Note the
   number of operation names that appear at the top of the page.

#. Click on to return to the constructor page.

#. Use the or keys to erase Integer from the input area.

#. Click on **Operations** to produce a new table of operations. Look at
   the number of operations you get. This number is greater than what
   you had before. Find, for example, the operation .

#. Click on **inverse** to produce a page describing the operation . At
   the bottom of the description, you notice that the **Conditions**
   line says “R has .” This operation is *not* exported by since is not
   a .

   Try putting the name of a domain such as (which is a field) into the
   input area, then clicking on **Operations**. As you see, the
   operation is exported.

Miscellaneous Features of Browse
--------------------------------

ugBrowseMiscellaneousFeatures

The Description Page for Operations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ugBrowseDescriptionPage From the constructor page of , click on
**Operations** to bring up the table of operations for .

Find the operation **inverse** in the table and click on it. This takes
you to a page showing the documentation for this operation.

(324,180)

Here is the significance of the headings you see.

Arguments
^^^^^^^^^

This lists each of the arguments of the operation in turn, paraphrasing
the of the operation. As for signatures, a is used to designate *this
domain*, that is, .

Returns
^^^^^^^

This describes the return value for the operation, analogous to the
**Arguments** part.

Origin
^^^^^^

This tells you which domain or category explicitly exports the
operation. In this example, the domain itself is the *Origin*.

Conditions
^^^^^^^^^^

This tells you that the operation is exported by only if “R has ,” that
is, “R is a member of category .” When no **Conditions** part is given,
the operation is exported for all values of R.

Description
^^^^^^^^^^^

Here are the comments that appear in the source code of its *Origin*,
here . You find these comments in the source code for .

(324,180)

Click on to return to the table of operations. Click on **map**. Here
you find three different operations named . This should not surprise
you. Operations are identified by name and . There are three operations
named , each with different signatures. What you see is the
*descriptions* view of the operations. If you like, select the button in
the heading of one of these descriptions to get *only* that operation.

Where
^^^^^

This part qualifies domain parameters mentioned in the arguments to the
operation.

Views of Operations
~~~~~~~~~~~~~~~~~~~

ugBrowseViewsOfOperations We suggest that you go to the constructor page
for and click on **Operations** to bring up a table of operations with a
*Views* panel at the bottom.

names
^^^^^

This view lists the names of the operations. Unlike constructors,
however, there may be several operations with the same name. The heading
for the page tells you the number of unique names and the number of
distinct operations when these numbers are different.

filter
^^^^^^

As for constructors, you can use this button to cut down the list of
operations you are looking at. Enter, for example, m\* into the input
area to the right of **filter** then click on **filter**. As usual, any
logical expression is permitted. For example, use

::

    *! or *?

to get a list of destructive operations and predicates.

documentation
^^^^^^^^^^^^^

This gives you the most information: a detailed description of all the
operations in the form you have seen before. Every other button
summarizes these operations in some form.

signatures
^^^^^^^^^^

This views the operations by showing their signatures.

parameters
^^^^^^^^^^

This views the operations by their distinct syntactic forms with
parameters.

origins
^^^^^^^

This organizes the operations according to the constructor that
explicitly exports them.

conditions
^^^^^^^^^^

This view organizes the operations into conditional and unconditional
operations.

usage
^^^^^

This button is only available if your user-level is set to
*development*. The **usage** button produces a table of constructors
that reference this operation. [6]_

implementation
^^^^^^^^^^^^^^

This button is only available if your user-level is set to
*development*. If you enter values for all domain parameters on the
constructor page, then the **implementation** button appears in place of
the **conditions** button. This button tells you what domains or
packages actually implement the various operations. [7]_

With your user-level set to *development*, we suggest you try this
exercise. Return to the main constructor page for , then enter Integer
into the input area at the bottom as the value of R. Then click on
**Operations** to produce a table of operations. Note that the
**conditions** part of the *Views* table is replaced by
**implementation**. Click on **implementation**. After some delay, you
get a page describing what implements each of the matrix operations,
organized by the various domains and packages.

(324,180)

generalize
^^^^^^^^^^

This button only appears for an operation page of a constructor
involving a unique operation name.

From an operations page for , select any operation name, say **rank**.
In the views panel, the **filter** button is replaced by **generalize**.
Click on it! What you get is a description of all FriCAS operations
named . [8]_

(324,180)

all domains
^^^^^^^^^^^

This button only appears on an operation page resulting from a search
from the front page of or from selecting **generalize** from an
operation page for a constructor.

Note that the **filter** button in the *Views* panel is replaced by
**all domains**. Click on it to produce a table of *all* domains or
packages that export a operation.

(324,180)

We note that this table specifically refers to all the operations shown
in the preceding page. Return to the descriptions of all the operations
and select one of them by clicking on the button in its heading. Select
**all domains**. As you see, you have a smaller table of constructors.
When there is only one constructor, you get the constructor page for
that constructor.

Capitalization Convention
~~~~~~~~~~~~~~~~~~~~~~~~~

ugBrowseCapitalizationConvention When entering search keys for
constructors, you can use capital letters to search for abbreviations.
For example, enter UTS into the input area and click on
**Constructors**. Up comes a page describing whose abbreviation is .

Constructor abbreviations always have three or more capital letters. For
short constructor names (six letters or less), abbreviations are not
generally helpful as their abbreviation is typically the constructor
name in capitals. For example, the abbreviation for is .

Abbreviations can also contain numbers. For example, is the abbreviation
for constructor . For default packages, the abbreviation is the same as
the abbreviation for the corresponding category with the “&” replaced by
“-”. For example, for the category default package the abbreviation is
since the corresponding category has abbreviation .

.. [1]
   To get only categories, domains, or packages, rather than all
   constructors, you can click on the corresponding button to the right
   of **Constructors**.

.. [2]
   You may want to widen your window to make what follows more legible.

.. [3]
   is a special domain implemented for matrix-like domains to provide
   efficient implementations of arrays. For example, domains of category
   can have any integer as their . Matrices and other members of this
   special “inner” array have their defined as .

.. [4]
   A constructor is a user of if it handles any matrix. For example, a
   constructor having internal (unexported) operations dealing with
   matrices is also a user.

.. [5]
   The benefactors exclude constructors such as whose operations
   macro-expand and so vanish from sight!

.. [6]
   FriCAS requires an especially long time to produce this table, so
   anticipate this when requesting this information.

.. [7]
   This button often takes a long time; expect a delay while you wait
   for an answer.

.. [8]
   If there were more than 10 operations of the name, you get instead a
   page with a *Views* panel at the bottom and the message to **Select a
   view below**. To get the descriptions of all these operations as
   mentioned above, select the **description** button.
