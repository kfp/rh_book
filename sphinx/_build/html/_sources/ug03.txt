Using 
======

ugHyper

(324,260)

is the gateway to FriCAS. It’s both an on-line tutorial and an on-line
reference manual. It also enables you to use FriCAS simply by using the
mouse and filling in templates. is available to you if you are running
FriCAS under the X Window System.

Pages usually have active areas, marked in **this font** (bold face). As
you move the mouse pointer to an active area, the pointer changes from a
filled dot to an open circle. The active areas are usually linked to
other pages. When you click on an active area, you move to the linked
page.

Headings
--------

ugHyperHeadings Most pages have a standard set of buttons at the top of
the page. This is what they mean:

    Click on this to get help. The button only appears if there is
    specific help for the page you are viewing. You can get *general*
    help for by clicking the help button on the home page.

    Click here to go back one page. By clicking on this button
    repeatedly, you can go back several pages and then take off in a new
    direction.

    Go back to the home page, that is, the page on which you started.
    Use to explore, to make forays into new topics. Don’t worry about
    how to get back. remembers where you came from. Just click on this
    button to return.

    From the root window (the one that is displayed when you start the
    system) this button leaves the program, and it must be restarted if
    you want to use it again. From any other window, it just makes that
    one window go away. You *must* use this button to get rid of a
    window. If you use the window manager “Close” button, then all of
    goes away.

The buttons are not displayed if they are not applicable to the page you
are viewing. For example, there is no button on the top-level menu.

Key Definitions
---------------

ugHyperKeys The following keyboard definitions are in effect throughout
. See and for some contextual key definitions.

F1
    Display the main help page.

F3
    Same as , makes the window go away if you are not at the top-level
    window or quits the facility if you are at the top-level.

F5
    Rereads the database, if necessary (for system developers).

F9
    Displays this information about key definitions.

F12
    Same as **F3**.

Up Arrow
    Scroll up one line.

Down Arrow
    Scroll down one line.

Page Up
    Scroll up one page.

Page Down
    Scroll down one page.

Scroll Bars
-----------

ugHyperScroll Whenever there is too much text to fit on a page, a
*scroll bar* automatically appears along the right side.

With a scroll bar, your page becomes an aperture, that is, a window into
a larger amount of text than can be displayed at one time. The scroll
bar lets you move up and down in the text to see different parts. It
also shows where the aperture is relative to the whole text. The
aperture is indicated by a strip on the scroll bar.

Move the cursor with the mouse to the “down-arrow” at the bottom of the
scroll bar and click. See that the aperture moves down one line. Do it
several times. Each time you click, the aperture moves down one line.
Move the mouse to the “up-arrow” at the top of the scroll bar and click.
The aperture moves up one line each time you click.

Next move the mouse to any position along the middle of the scroll bar
and click. attempts to move the top of the aperture to this point in the
text.

You cannot make the aperture go off the bottom edge. When the aperture
is about half the size of text, the lowest you can move the aperture is
halfway down.

To move up or down one screen at a time, use the and keys on your
keyboard. They move the visible part of the region up and down one page
each time you press them.

If the page does not contain an input area (see ), you can also use the
and and arrow keys to navigate. When you press the key, the screen is
positioned at the very top of the page. Use the and arrow keys to move
the screen up and down one line at a time, respectively.

Input Areas
-----------

ugHyperInput Input areas are boxes where you can put data.

To enter characters, first move your mouse cursor to somewhere within
the page. Characters that you type are inserted in front of the
underscore. This means that when you type characters at your keyboard,
they go into this first input area.

The input area grows to accommodate as many characters as you type. Use
the key to erase characters to the left. To modify what you type, use
the right-arrow and left-arrow keys and the keys , , and . These keys
are found immediately on the right of the standard IBM keyboard.

If you press the key, the cursor moves to the beginning of the line and
if you press the key, the cursor moves to the end of the line. Pressing
– deletes all the text from the cursor to the end of the line.

A page may have more than one input area. Only one input area has an
underscore cursor. When you first see apage, the top-most input area
contains the cursor. To type information into another input area, use
the or key to move from one input area to another. To move in the
reverse order, use –.

You can also move from one input area to another using your mouse.
Notice that each input area is active. Click on one of the areas. As you
can see, the underscore cursor moves to that window.

Radio Buttons and Toggles
-------------------------

ugHyperButtons Some pages have *radio buttons* and *toggles*. Radio
buttons are a group of buttons like those on car radios: you can select
only one at a time. Once you have selected a button, it appears to be
inverted and contains a checkmark. To change the selection, move the
cursor with the mouse to a different radio button and click.

A toggle is an independent button that displays some on/off state. When
“on”, the button appears to be inverted and contains a checkmark. When
“off”, the button is raised. Unlike radio buttons, you can set a group
of them any way you like. To change toggle the selection, move the
cursor with the mouse to the button and click.

Search Strings
--------------

ugHyperSearch A *search string* is used for searching some database. To
learn about search strings, we suggest that you bring up the glossary.
To do this from the top-level page of :

#. Click on , bringing up the FriCAS Reference page.

#. Click on , bringing up the glossary.

The glossary has an input area at its bottom. We review the various
kinds of search strings you can enter to search the glossary.

The simplest search string is a word, for example, operation. A word
only matches an entry having exactly that spelling. Enter the word
operation into the input area above then click on **Search**. As you can
see, operation matches only one entry, namely with operation itself.

Normally matching is insensitive to whether the alphabetic characters of
your search string are in uppercase or lowercase. Thus operation and
OperAtion both have the same effect. You will very often want to use the
wildcard in your search string so as to match multiple entries in the
list. The search key matches every entry in the list. You can also use
anywhere within a search string to match an arbitrary substring. Try
cat\* for example: enter cat\* into the input area and click on
**Search**. This matches several entries.

You use any number of wildcards in a search string as long as they are
not adjacent. Try search strings such as dom\*. As you see, this search
string matches domain, domain constructor, subdomain, and so on.

Logical Searches
~~~~~~~~~~~~~~~~

ugLogicalSearches For more complicated searches, you can use , , and
with basic search strings; write logical expressions using these three
operators just as in the FriCAS language. For example, domain or package
matches the two entries domain and package. Similarly, dom\* and \*con\*
matches domain constructor and others. Also not \*a\* matches every
entry that does not contain the letter a somewhere.

Use parentheses for grouping. For example, dom\* and (not \*con\*)
matches domain but not domain constructor.

There is no limit to how complex your logical expression can be. For
example,

a\* or b\* or c\* or d\* or e\* and (not \*a\*)

is a valid expression.

Example Pages
-------------

ugHyperExample Many pages have FriCAS example commands. Each command has
an active “button” along the left margin. When you click on this button,
the output for the command is “pasted-in.” Click again on the button and
you see that the pasted-in output disappears.

Maybe you would like to run an example? To do so, just click on any part
of its text! When you do, the example line is copied into a new
interactive FriCAS buffer for this page.

Sometimes one example line cannot be run before you run an earlier one.
Don’t worry— automatically runs all the necessary lines in the right
order!

The new interactive FriCAS buffer disappears when you leave . If you
want to get rid of it beforehand, use the **Cancel** button of the X
Window manager or issue the FriCAS system command .

X Window Resources for 
-----------------------

ugHyperResources You can control the appearance of while running under
Version 11 of the X Window System by placing the following resources in
the file **.Xdefaults** in your home directory. In what follows, *font*
is any valid X11 font name (for example, Rom14) and *color* is any valid
X11 color specification (for example, NavyBlue). For more information
about fonts and colors, refer to the X Window documentation for your
system.

FriCAS.hyperdoc.RmFont: *font*
     This is the standard text font.

FriCAS.hyperdoc.RmColor: *color*
     This is the standard text color.

FriCAS.hyperdoc.ActiveFont: *font*
     This is the font used for link buttons.

FriCAS.hyperdoc.ActiveColor: *color*
     This is the color used for link buttons.

FriCAS.hyperdoc.FriCASFont: *font*
     This is the font used for active FriCAS commands.

FriCAS.hyperdoc.FriCASColor: *color*
     This is the color used for active FriCAS commands.

FriCAS.hyperdoc.BoldFont: *font*
     This is the font used for bold face.

FriCAS.hyperdoc.BoldColor: *color*
     This is the color used for bold face.

FriCAS.hyperdoc.TtFont: *font*
     This is the font used for FriCAS output in . This font must be
    fixed-width.

FriCAS.hyperdoc.TtColor: *color*
     This is the color used for FriCAS output in .

FriCAS.hyperdoc.EmphasizeFont: *font*
     This is the font used for italics.

FriCAS.hyperdoc.EmphasizeColor: *color*
     This is the color used for italics.

FriCAS.hyperdoc.InputBackground: *color*
     This is the color used as the background for input areas.

FriCAS.hyperdoc.InputForeground: *color*
     This is the color used as the foreground for input areas.

FriCAS.hyperdoc.BorderColor: *color*
     This is the color used for drawing border lines.

FriCAS.hyperdoc.Background: *color*
     This is the color used for the background of all windows.

    Note: In the past resource names used word Axiom instead of FriCAS.
