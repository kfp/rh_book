Input Files and Output Styles
=============================

ugInOut In this chapter we discuss how to collect FriCAS statements and
commands into files and then read the contents into the workspace. We
also show how to display the results of your computations in several
different styles including TeX, FORTRAN and monospace two-dimensional
format. [1]_

The printed version of this book uses the FriCAS TeX output formatter.
When we demonstrate a particular output style, we will need to turn TeX
formatting off and the output style on so that the correct output is
shown in the text.

Input Files
-----------

ugInOutIn In this section we explain what an *input file* is and why you
would want to know about it. We discuss where FriCAS looks for input
files and how you can direct it to look elsewhere. We also show how to
read the contents of an input file into the and how to use the facility
to generate an input file from the statements you have entered directly
into the workspace.

An *input* file contains FriCAS expressions and system commands.
Anything that you can enter directly to FriCAS can be put into an input
file. This is how you save input functions and expressions that you wish
to read into FriCAS more than one time.

To read an input file into FriCAS, use the system command. For example,
you can read a file in a particular directory by issuing

::

    )read /spad/src/input/matrix.input

The “**.input**” is optional; this also works:

::

    )read /spad/src/input/matrix

What happens if you just enter or even ? FriCAS looks in your current
working directory for input files that are not qualified by a directory
name. Typically, this directory is the directory from which you invoked
FriCAS. To change the current working directory, use the system command.
The command by itself shows the current working directory. To change it
to the subdirectory for user “babar”, issue

::

    )cd /u/babar/src/input

FriCAS looks first in this directory for an input file. If it is not
found, it looks in the system’s directories, assuming you meant some
input file that was provided with FriCAS.

If you have the FriCAS history facility turned on (which it is by
default), you can save all the lines you have entered into the workspace
by entering

::

    )history )write

FriCAS tells you what input file to edit to see your statements. The
file is in your home directory or in the directory you specified with .

In we discuss using indentation in input files to group statements into
*blocks.*

The fricas.input File
---------------------

ugInOutSpadprof When FriCAS starts up, it tries to read the input file
**fricas.input** from your home directory. It there is no
**fricas.input** in your home directory, it reads the copy located in
its own **src/input** directory. The file usually contains system
commands to personalize your FriCAS environment. In the remainder of
this section we mention a few things that users frequently place in
their **fricas.input** files.

In order to have FORTRAN output always produced from your computations,
place the system command in **fricas.input**. If you do want to be
prompted for confirmation when you issue the system command, place in
**fricas.input**. If you then decide that you do not want to be
prompted, issue . This is the default setting  [2]_

To see the other system variables you can set, issue or use the facility
to view and change FriCAS system variables.

Common Features of Using Output Formats
---------------------------------------

ugInOutOut In this section we discuss how to start and stop the display
of the different output formats and how to send the output to the screen
or to a file. To fix ideas, we use FORTRAN output format for most of the
examples.

You can use the system command to toggle or redirect the different kinds
of output. The name of the kind of output follows “output” in the
command. The names are

+------------------------------------------------------------------+----+
| **fortran & for FORTRAN output.                                  |    |
| **algebra & for monospace two-dimensional mathematical output.   |    |
| **tex & for TeX output.                                          |    |
| **script & for IBM Script Formula Format output.********         |    |
+------------------------------------------------------------------+----+

For example, issue to turn on FORTRAN format and issue to turn it off.
By default, algebra is on and all others are off. When output is
started, it is sent to the screen. To send the output to a file, give
the file name without directory or extension. FriCAS appends a file
extension depending on the kind of output being produced.

Issue this to redirect FORTRAN output to, for example, the file
**linalg.sfort**.

::

    )set output fortran linalg

FORTRAN output will be written to file
/home/kfp/Devel/rh\_fricas/src/doc/linalg.sfort .

You must *also* turn on the creation of FORTRAN output. The above just
says where it goes if it is created.

::

    )set output fortran on

In what directory is this output placed? It goes into the directory from
which you started FriCAS, or if you have used the system command, the
one that you specified with . You should use before you send the output
to the file.

You can always direct output back to the screen by issuing this.

::

    )set output fortran console

Let’s make sure FORTRAN formatting is off so that nothing we do from now
on produces FORTRAN output.

::

    )set output fortran off

We also delete the demonstrated output file we created.

::

    )system rm linalg.sfort

You can abbreviate the words “,” “” and “” to the minimal number of
characters needed to distinguish them. Because of this, you cannot send
output to files called **on.sfort, off.sfort, of.sfort, console.sfort,
consol.sfort** and so on.

The width of the output on the page is set by for all formats except
FORTRAN. Use to change the FORTRAN line length from its default value of
.

Monospace Two-Dimensional Mathematical Format
---------------------------------------------

ugInOutAlgebra This is the default output format for FriCAS. It is
usually on when you start the system.

If it is not, issue this.

::

    )set output algebra on 

Since the printed version of this book (as opposed to the version) shows
output produced by the TeX output formatter, let us temporarily turn off
TeX output.

::

    )set output tex off 

Here is an example of what it looks like.

::

    matrix [[i*x^i + j*%i*y^j for i in 1..2] for j in 3..4] 

::

    +     3           3     2+
    |3%i y  + x  3%i y  + 2x |
    |                        |
    |     4           4     2|
    +4%i y  + x  4%i y  + 2x +

Issue this to turn off this kind of formatting.

::

    )set output algebra off

Turn TeX output on again.

::

    )set output tex on

The characters used for the matrix brackets above are rather ugly. You
get this character set when you issue . This character set should be
used when your machine or your version of FriCAS does not support
Unicode character set. If your machine and your version of FriCAS
support Unicode, issue to get better looking output.

TeX Format
----------

ugInOutTeX FriCAS can produce TeX output for your expressions. The
output is produced using macros from the LaTeX document preparation
system by Leslie Lamport. [3]_ The printed version of this book was
produced using this formatter.

To turn on TeX output formatting, issue this.

::

    )set output tex on 

Here is an example of its output.

::

    matrix [[i*x^i + j*\%i*y^j for i in 1..2] for j in 3..4]

    $$
    \begin{bmatrix}{2}3\TIMES \ImaginaryI \TIMES \SUPER{\SYMBOL{y}}{3}+\SYMBOL{x}&%
    3\TIMES \ImaginaryI \TIMES \SUPER{\SYMBOL{y}}{3}+2\TIMES \SUPER{\SYMBOL{x}}{2%
    }\\4\TIMES \ImaginaryI \TIMES \SUPER{\SYMBOL{y}}{4}+\SYMBOL{x}&4\TIMES %
    \ImaginaryI \TIMES \SUPER{\SYMBOL{y}}{4}+2\TIMES \SUPER{\SYMBOL{x}}{2}%
    \end{bmatrix}%
    $$

With the definition of the fricasmath environment as defined in
fricasmath.sty this formats as

.. math::

   \begin{bmatrix}{2}
   3{\:}{i}{\:}{{{{y}}}^{3}}+{{x}}&3{\:}{i}{\:}{{{{y}}}^{3}}+2{\:}{{{{x}}}^{2}}\\4{\:}{i}{\:}{{{{y}}}^{4}}+{{x}}&4{\:}{i}{\:}{{{{y}}}^{4}}+2{\:}{{{{x}}}^{2}}\end{bmatrix}

 To turn TeX output formatting off, issue . The LaTeX macros in the
output generated by FriCAS are generic. See the source file of for
appropriate definitions of these commands.

Math ML Format
--------------

ugInOutMathML FriCAS can produce Math ML format output for your
expressions.

To turn Math ML Format on, issue this.

::

    )set output mathml on

Here is an example of its output.

::

    x+sqrt(2)

    <math xmlns="http://www.w3.org/1998/Math/MathML" mathsize="big" display="block">
    <mrow><mi>x</mi><mo>+</mo><msqrt><mrow><mn>2</mn></mrow></msqrt></mrow>
    </math>

To turn Math ML Format output formatting off, issue this.

::

    )set output mathml off

Texmacs Format
--------------

ugInOutTexmacs FriCAS can produce Texmacs Scheme format output for your
expressions. This is mostly useful for interfacing with Texmacs.

To turn Texmacs Format on, issue this.

::

    )set output texmacs on

Here is an example of its output.

::

    x+sqrt(2)

    scheme: (with "mode" "math"
    (concat (concat  "x" ) "+" (sqrt (concat  "2" )))
    )

To turn Texmacs Format output formatting off, issue this.

::

    )set output texmacs off

IBM Script Formula Format
-------------------------

ugInOutScript FriCAS can produce IBM Script Formula Format output for
your expressions.

To turn IBM Script Formula Format on, issue this.

::

    )set output script on

Here is an example of its output.

::

    matrix [[i*x^i + j*%i*y^j for i in 1..2] for j in 3..4]

    .eq set blank @
    :df.
    <left lb <<<<3 @@ %i @@ <y sup 3>>+x> here <<3 @@ %i @@
    <y sup 3>>+<2 @@ <x sup 2>>>> habove <<<4 @@ %i @@
    <y sup 4>>+x> here <<4 @@ %i @@ <y sup 4>>+<2 @@
    <x up 2>>>>> right rb>
    :edf.

To turn IBM Script Formula Format output formatting off, issue this.

::

    )set output script off

FORTRAN Format
--------------

ugInOutFortran In addition to turning FORTRAN output on and off and
stating where the output should be placed, there are many options that
control the appearance of the generated code. In this section we
describe some of the basic options. Issue to see a full list with their
current settings.

The output FORTRAN expression usually begins in column 7. If the
expression needs more than one line, the ampersand character is used in
column 6. Since some versions of FORTRAN have restrictions on the number
of lines per statement, FriCAS breaks long expressions into segments
with a maximum of 1320 characters (20 lines of 66 characters) per
segment. If you want to change this, say, to 660 characters, issue the
system command . You can turn off the line breaking by issuing . Various
code optimization levels are available.

FORTRAN output is produced after you issue this.

::

    )set output fortran on 

For the initial examples, we set the optimization level to 0, which is
the lowest level.

::

    )set fortran optlevel 0 

The output is usually in columns 7 through 72, although fewer columns
are used in the following examples so that the output fits nicely on the
page.

::

    )set fortran fortlength 60

By default, the output goes to the screen and is displayed before the
standard FriCAS two-dimensional output. In this example, an assignment
to the variable was generated because this is the result of step 1.

::

    (x+y)^3 

.. math:: {{{{y}}}^{3}}+3{\:}{{x}}{\:}{{{{y}}}^{2}}+3{\:}{{{{x}}}^{2}}{\:}{{y}}+{{{{x}}}^{3}}

``Polynomial(Integer)``

Here is an example that illustrates the line breaking.

::

    (x+y+z)^3 

.. math:: {{{{z}}}^{3}}+{{\left ( {}3{\:}{{y}}+3{\:}{{x}} \right ) {}}}{\:}{{{{z}}}^{2}}+{{\left ( {}3{\:}{{{{y}}}^{2}}+6{\:}{{x}}{\:}{{y}}+3{\:}{{{{x}}}^{2}} \right ) {}}}{\:}{{z}}+{{{{y}}}^{3}}+3{\:}{{x}}{\:}{{{{y}}}^{2}}+3{\:}{{{{x}}}^{2}}{\:}{{y}}+{{{{x}}}^{3}}

``Polynomial(Integer)``

Note in the above examples that integers are generally converted to
floating point numbers, except in exponents. This is the default
behavior but can be turned off by issuing . The rules governing when the
conversion is done are:

#. If an integer is an exponent, convert it to a floating point number
   if it is greater than 32767 in absolute value, otherwise leave it as
   an integer.

#. Convert all other integers in an expression to floating point
   numbers.

These rules only govern integers in expressions. Numbers generated by
FriCAS for statements are also integers.

To set the type of generated FORTRAN data, use one of the following:

::

    )set fortran defaulttype REAL
    )set fortran defaulttype INTEGER
    )set fortran defaulttype COMPLEX
    )set fortran defaulttype LOGICAL
    )set fortran defaulttype CHARACTER

When temporaries are created, they are given a default type of REAL.
Also, the REAL versions of functions are used by default.

::

    sin(x) 

.. math:: \sin{{{x}}}

``Expression(Integer)``

At optimization level 1, FriCAS removes common subexpressions.

::

    )set fortran optlevel 1 

::

    (x+y+z)^3 

.. math:: {{{{z}}}^{3}}+{{\left ( {}3{\:}{{y}}+3{\:}{{x}} \right ) {}}}{\:}{{{{z}}}^{2}}+{{\left ( {}3{\:}{{{{y}}}^{2}}+6{\:}{{x}}{\:}{{y}}+3{\:}{{{{x}}}^{2}} \right ) {}}}{\:}{{z}}+{{{{y}}}^{3}}+3{\:}{{x}}{\:}{{{{y}}}^{2}}+3{\:}{{{{x}}}^{2}}{\:}{{y}}+{{{{x}}}^{3}}

``Polynomial(Integer)``

This changes the precision to DOUBLE. Substitute for to return to single
precision.

::

    )set fortran precision double 

Complex constants display the precision.

::

    2.3 + 5.6*%i  

.. math:: {\texttt{2.3}}+{\texttt{5.6}}{\:}{i}

``Complex(Float)``

The function names that FriCAS generates depend on the chosen precision.

::

    sin %e  

.. math:: \sin{{e}}

``Expression(Integer)``

Reset the precision to and look at these two examples again.

::

    )set fortran precision single 

::

    2.3 + 5.6*%i  

.. math:: {\texttt{2.3}}+{\texttt{5.6}}{\:}{i}

``Complex(Float)``

::

    sin %e  

.. math:: \sin{{e}}

``Expression(Integer)``

Expressions that look like lists, streams, sets or matrices cause array
code to be generated.

::

    [x+1,y+1,z+1] 

.. math:: {{\left \lbrack {}{{x}}+1{,\:}{{y}}+1{,\:}{{z}}+1 \right \rbrack {}}}

``List(Polynomial(Integer))``

A temporary variable is generated to be the name of the array. This may
have to be changed in your particular application.

::

    set[2,3,4,3,5] 

.. math:: {\left\{ 2{,\:}3{,\:}4{,\:}5 \right\}}

``Set(PositiveInteger)``

By default, the starting index for generated FORTRAN arrays is .

::

    matrix [[2.3,9.7],[0.0,18.778]] 

.. math:: \begin{bmatrix}{2}{\texttt{2.3}}&{\texttt{9.7}}\\{\texttt{0.0}}&{\texttt{18.778}}\end{bmatrix}

``Matrix(Float)``

To change the starting index for generated FORTRAN arrays to be , issue
this. This value can only be or .

::

    )set fortran startindex 1 

Look at the code generated for the matrix again.

::

    matrix [[2.3,9.7],[0.0,18.778]] 

.. math:: \begin{bmatrix}{2}{\texttt{2.3}}&{\texttt{9.7}}\\{\texttt{0.0}}&{\texttt{18.778}}\end{bmatrix}

``Matrix(Float)``

General Fortran-generation utilities in FriCAS
----------------------------------------------

ugGeneralFortran This section describes more advanced facilities which
are available to users who wish to generate Fortran code from within
FriCAS. There are facilities to manipulate templates, store type
information, and generate code fragments or complete programs.

Template Manipulation
~~~~~~~~~~~~~~~~~~~~~

ugGenForTemplate A template is a skeletal program which is “fleshed out”
with data when it is processed. It is a sequence of *active* and
*passive* parts: active parts are sequences of FriCAS commands which are
processed as if they had been typed into the interpreter; passive parts
are simply echoed verbatim on the Fortran output stream.

Suppose, for example, that we have the following template, stored in the
file “test.tem”:

::

    -- A simple template
    beginVerbatim
          DOUBLE PRECISION FUNCTION F(X)
          DOUBLE PRECISION X
    endVerbatim
    outputAsFortran("F",f)
    beginVerbatim
          RETURN
          END
    endVerbatim

The passive parts lie between the two tokens beginVerbatim and
endVerbatim. There are two active statements: one which is simply an
FriCAS ( –) comment, and one which produces an assignment to the current
value of f. We could use it as follows:

::

    (4) ->f := 4.0/(1+X^2)

               4
       (4)   ------
              2
             X  + 1

    (5) ->processTemplate "test.tem"
          DOUBLE PRECISION FUNCTION F(X)
          DOUBLE PRECISION X
          F=4.0D0/(X*X+1.0D0)
          RETURN
          END

       (5)  "CONSOLE"

(A more reliable method of specifying the filename will be introduced
below.) Note that the Fortran assignment F=4.0D0/(X\*X+1.0D0)
automatically converted 4.0 and 1 into DOUBLE PRECISION numbers; in
general, the FriCAS Fortran generation facility will convert anything
which should be a floating point object into either a Fortran REAL or
DOUBLE PRECISION object.

Which alternative is used is determined by the command

::

    )set fortran precision

————————– The precision Option —————————

Description: precision of generated FORTRAN objects

The precision option may be followed by any one of the following:

-> single double

The current setting is indicated within the list.

It is sometimes useful to end a template before the file itself ends
(e.g. to allow the template to be tested incrementally or so that a
piece of text describing how the template works can be included). It is
of course possible to “comment-out” the remainder of the file.
Alternatively, the single token endInput as part of an active portion of
the template will cause processing to be ended prematurely at that
point.

The command comes in two flavours. In the first case, illustrated above,
it takes one argument of domain , the name of the template to be
processed, and writes its output on the current Fortran output stream.
In general, a filename can be generated from *directory*, *name* and
*extension* components, using the operation , as in

::

    processTemplate filename("","test","tem")

There is an alternative version of , which takes two arguments (both of
domain ). In this case the first argument is the name of the template to
be processed, and the second is the file in which to write the results.
Both versions return the location of the generated Fortran code as their
result (“CONSOLE” in the above example).

It is sometimes useful to be able to mix active and passive parts of a
line or statement. For example you might want to generate a Fortran
Comment describing your data set. For this kind of application we
provide three functions as follows:

+----+---------------------------------------------------------------------+
|    | writes a string on the Fortran output stream                        |
+----+---------------------------------------------------------------------+
+----+---------------------------------------------------------------------+
|    | writes a carriage return on the Fortran output stream               |
+----+---------------------------------------------------------------------+
+----+---------------------------------------------------------------------+
|    | writes a string followed by a return on the Fortran output stream   |
+----+---------------------------------------------------------------------+

So we could create our comment as follows:

::

    m := matrix [[1,2,3],[4,5,6]]

.. math:: \begin{bmatrix}{3}1&2&3\\4&5&6\end{bmatrix}

``Matrix(Integer)``

::

    fortranLiteralLine(concat ["C\ \ \ \ \ \ The\ Matrix\ has\ ", nrows(m)::String, "\ rows\ and\ ", ncols(m)::String, "\ columns"])$FortranTemplate 

or, alternatively:

::

    fortranLiteral("C\ \ \ \ \ \ The\ Matrix\ has\ ")$FortranTemplate

::

    fortranLiteral(nrows(m)::String)$FortranTemplate

::

    fortranLiteral("\ rows\ and\ ")$FortranTemplate

::

    fortranLiteral(ncols(m)::String)$FortranTemplate

::

    fortranLiteral("\ columns")$FortranTemplate

::

    fortranCarriageReturn()$FortranTemplate

We should stress that these functions, together with the function are
the *only* sure ways of getting output to appear on the Fortran output
stream. Attempts to use FriCAS commands such as or may appear to give
the required result when displayed on the console, but will give the
wrong result when Fortran and algebraic output are sent to differing
locations. On the other hand, these functions can be used to send
helpful messages to the user, without interfering with the generated
Fortran.

Manipulating the Fortran Output Stream
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ugGneForManipulating

Sometimes it is useful to manipulate the Fortran output stream in a
program, possibly without being aware of its current value. The main use
of this is for gathering type declarations (see “Fortran Types” below)
but it can be useful in other contexts as well. Thus we provide a set of
commands to manipulate a stack of (open) output streams. Only one stream
can be written to at any given time. The stack is never empty—its
initial value is the console or the current value of the Fortran output
stream, and can be determined using

::

    topFortranOutputStack()$FortranOutputStackPackage

.. math:: {\texttt{"/home/kfp/Devel/rh\_fricas/src/doc/linalg.sfort"}}

``String``

(see below). The commands available to manipulate the stack are:

+----+----------------------------------------+
|    | resets the stack to the console        |
+----+----------------------------------------+
+----+----------------------------------------+
|    | pushes a onto the stack                |
+----+----------------------------------------+
+----+----------------------------------------+
|    | pops the stack                         |
+----+----------------------------------------+
+----+----------------------------------------+
|    | returns the current stack              |
+----+----------------------------------------+
+----+----------------------------------------+
|    | returns the top element of the stack   |
+----+----------------------------------------+

These commands are all part of .

Fortran Types
~~~~~~~~~~~~~

ugGenForTypes When generating code it is important to keep track of the
Fortran types of the objects which we are generating. This is useful for
a number of reasons, not least to ensure that we are actually generating
legal Fortran code. The current type system is built up in several
layers, and we shall describe each in turn.

FortranScalarType
~~~~~~~~~~~~~~~~~

ugGenForScalarType

This domain represents the simple Fortran datatypes: REAL, DOUBLE
PRECISION, COMPLEX, LOGICAL, INTEGER, and CHARACTER. It is possible to a
or into the domain, test whether two objects are equal, and also apply
the predicate functions etc.

FortranType
~~~~~~~~~~~

ugGenForType

This domain represents “full” types: i.e., datatype plus array
dimensions (where appropriate) plus whether or not the parameter is an
external subprogram. It is possible to an object of into the domain or
one from an element of , a list of s (which can of course be simple
integers or symbols) representing its dimensions, and a declaring
whether it is external or not. The list of dimensions must be empty if
the is true. The functions , and return the appropriate parts, and it is
possible to get the various basic Fortran Types via functions like .

For example:

::

    type:=construct(real,[i,10],false)$FortranType

or

::

    type:=[real,[i,10],false]$FortranType

::

    scalarTypeOf type

.. math:: {{REAL}}

``Union(fst: FortranScalarType, ...)``

::

    dimensionsOf type

.. math:: {{\left \lbrack {}{{i}}{,\:}10 \right \rbrack {}}}

``List(Polynomial(Integer))``

::

    external?  type

.. math:: {\texttt{false}}

``Boolean``

::

    fortranLogical()$FortranType

.. math:: {{LOGICAL}}

``FortranType``

::

    construct(integer,[],true)$FortranType

SymbolTable
~~~~~~~~~~~

ugGenForSymbolTable

This domain creates and manipulates a symbol table for generated Fortran
code. This is used by to represent the types of objects in a subprogram.
The commands available are:

+----+---------------------------------------------------------+
|    | creates a new                                           |
+----+---------------------------------------------------------+
+----+---------------------------------------------------------+
|    | creates a new entry in a table                          |
+----+---------------------------------------------------------+
+----+---------------------------------------------------------+
|    | returns the type of an object in a table                |
+----+---------------------------------------------------------+
+----+---------------------------------------------------------+
|    | returns a list of all the symbols in the table          |
+----+---------------------------------------------------------+
+----+---------------------------------------------------------+
|    | returns a list of all objects of a given type           |
+----+---------------------------------------------------------+
+----+---------------------------------------------------------+
|    | returns a list of lists of all objects sorted by type   |
+----+---------------------------------------------------------+
+----+---------------------------------------------------------+
|    | returns a list of all EXTERNAL objects                  |
+----+---------------------------------------------------------+
+----+---------------------------------------------------------+
|    | produces Fortran type declarations from a table         |
+----+---------------------------------------------------------+

::

    symbols := empty()$SymbolTable

.. math:: {\texttt{table}}{{\left ( {}\right ) {}}}

``SymbolTable``

::

    declare!(X, fortranReal()$FortranType, symbols)

.. math:: {{REAL}}

``FortranType``

::

    declare!(M,construct(real,[i,j],false)$FortranType,symbols)

::

    declare!([i,j], fortranInteger()$FortranType, symbols)

.. math:: {{INTEGER}}

``FortranType``

::

    symbols

::

    fortranTypeOf(i,symbols)

.. math:: {{INTEGER}}

``FortranType``

::

    typeList(real,symbols)

.. math:: {{\left \lbrack {}{{X}}{,\:}{{\left \lbrack {}{{M}}{,\:}{{i}}{,\:}{{j}} \right \rbrack {}}} \right \rbrack {}}}

``List(Union(name: Symbol, bounds: List(Union(S: Symbol, P: Polynomial(Integer)))))``

::

    printTypes symbols

TheSymbolTable
~~~~~~~~~~~~~~

ugGenForTheSymbolTable

This domain creates and manipulates one global symbol table to be used,
for example, during template processing. It is also used when linking to
external Fortran routines. The information stored for each subprogram
(and the main program segment, where relevant) is:

-  its name;

-  its return type;

-  its argument list;

-  and its argument types.

Initially, any information provided is deemed to be for the main program
segment.

Issuing the following command indicates that from now on all information
refers to the subprogram .

::

    newSubProgram(F)$TheSymbolTable

It is possible to return to processing the main program segment by
issuing the command:

::

    endSubProgram()$TheSymbolTable

.. math:: {{MAIN}}

``Symbol``

The following commands exist:

+----+-----------------------------------------------------------------------+
|    | declares the return type of the current subprogram                    |
+----+-----------------------------------------------------------------------+
+----+-----------------------------------------------------------------------+
|    | returns the return type of a subprogram                               |
+----+-----------------------------------------------------------------------+
+----+-----------------------------------------------------------------------+
|    | declares the argument list of the current subprogram                  |
+----+-----------------------------------------------------------------------+
+----+-----------------------------------------------------------------------+
|    | returns the argument list of a subprogram                             |
+----+-----------------------------------------------------------------------+
+----+-----------------------------------------------------------------------+
|    | provides type declarations for parameters of the current subprogram   |
+----+-----------------------------------------------------------------------+
+----+-----------------------------------------------------------------------+
|    | returns the symbol table of a subprogram                              |
+----+-----------------------------------------------------------------------+
+----+-----------------------------------------------------------------------+
|    | produces the Fortran header for the current subprogram                |
+----+-----------------------------------------------------------------------+

In addition there are versions of these commands which are parameterised
by the name of a subprogram, and others parameterised by both the name
of a subprogram and by an instance of .

::

    newSubProgram(F)$TheSymbolTable 

::

    argumentList!(F, [X])$TheSymbolTable

::

    returnType!(F,real)$TheSymbolTable 

::

    declare!(X,fortranReal(),F)$TheSymbolTable 

.. math:: {{REAL}}

``FortranType``

::

    printHeader(F)$TheSymbolTable

Advanced Fortran Code Generation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ugGenForAdvanced This section describes facilities for representing
Fortran statements, and building up complete subprograms from them.

Switch
~~~~~~

ugGenForSwitch

This domain is used to represent statements like x < y. Although these
can be represented directly in FriCAS, it is a little cumbersome.
Instead we have a set of operations, such as to represent , to let us
build such statements. The available constructors are:

+----+----------------+
|    | :math:`<`      |
+----+----------------+
|    | :math:`>`      |
+----+----------------+
|    | :math:`\leq`   |
+----+----------------+
|    | :math:`\geq`   |
+----+----------------+
|    | :math:`=`      |
+----+----------------+
|    | :math:`and`    |
+----+----------------+
|    | :math:`or`     |
+----+----------------+
|    | :math:`not`    |
+----+----------------+

So for example:

::

    LT(x,y)$Switch

.. math:: {{x}}<{{y}}

``Switch``

FortranCode
~~~~~~~~~~~

ugGenForCode This domain represents code segments or operations:
currently assignments, conditionals, blocks, comments, gotos, continues,
various kinds of loops, and return statements.

For example we can create quite a complicated conditional statement
using assignments, and then turn it into Fortran code:

::

    c := cond(LT(X,Y),assign(F,X),cond(GT(Y,Z),assign(F,Y),assign(F,Z))$FortranCode)$FortranCode 

.. math:: {\texttt{"conditional"}}

``FortranCode``

::

    printCode c

The Fortran code is printed on the current Fortran output stream.

FortranProgram
~~~~~~~~~~~~~~

ugGenForProgram

This domain is used to construct complete Fortran subprograms out of
elements of . It is parameterised by the name of the target subprogram
(a ), its return type (from (,“void”)), its arguments (from ), and its
symbol table (from ). One can elements of either or into it.

First of all we create a symbol table:

::

    symbols := empty()$SymbolTable

.. math:: {\texttt{table}}{{\left ( {}\right ) {}}}

``SymbolTable``

Now put some type declarations into it:

::

    declare!([X,Y],fortranReal()$FortranType,symbols)

.. math:: {{REAL}}

``FortranType``

Then (for convenience) we set up the particular instantiation of

::

    FP := FortranProgram(F,real,[X,Y],symbols)

.. math:: {\texttt{FortranProgram(F,REAL,[X,Y],table(Y=REAL,X=REAL))}}

``Type``

Create an object of type :

::

    asp := X*sin(Y)

.. math:: {{X}}{\:}\sin{{{Y}}}

``Expression(Integer)``

Now it into , and print its Fortran form:

::

    outputAsFortran(asp::FP)

We can generate a using . For example:

Augment our symbol table:

::

    declare!(Z,fortranReal()$FortranType,symbols)

.. math:: {{REAL}}

``FortranType``

and transform the conditional expression we prepared earlier:

::

    outputAsFortran([c,returns()$FortranCode]::FP) 

Cannot convert the value from type List(Any) to FortranProgram(F,
REAL,[X,Y],table(Z=REAL,Y=REAL,X=REAL)) .

.. [1]
   TeX is a trademark of the American Mathematical Society.

.. [2]
   The system command always prompts you for confirmation.

.. [3]
   See Leslie Lamport, *LaTeX: A Document Preparation System,* Reading,
   Massachusetts: Addison-Wesley Publishing Company, Inc., 1986.
