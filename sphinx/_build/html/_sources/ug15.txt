What’s New in FriCAS
====================

ugWhatsNew

Release Notes
-------------

releaseNotes FriCAS information can be found online at
http://fricas.sourceforge.net

FriCAS 1.3.1
^^^^^^^^^^^^

-  Categories with associative multiplication are now subcategories of
   categories with nonassociative multiplication.

-  Inlining optimization in now effective also in command line
   (interpreter) compiler.

-  Added conversions between finitely presented groups and permutation
   groups (Todd-Coxeter algorithm) and back.

-  Removed special handling of coercion of String to OutputForm from
   Spad compiler.

-  Former FramedModule is renamed to FractionalIdealAsModule. Added new
   FramedModule.

-  Whole interpreter is now included in executable (no need to load
   parts before use).

Bug fixes, in particular:

-  Fixed build with sbcl-1.3.13.

-  Limits using the name of variable in limit point work now.

-  A few output fixes.

-  Several integrator fixes.

-  Removed wrond interpreter transform of ’ =’.

-  Fixed compilation of type parameters containing non-type values.

-  Plots sometimes used single precision. Now they should always use
   double precision.

FriCAS 1.3.0
^^^^^^^^^^^^

-  Several domains and categories are more general, in particular
   matrices, indexed products and direct product.

-  ’)show’ now evaluates predicates.

-  Improved integrator, handles few more ’erf’ cases and more algebraic
   functions. Result should be simpler.

-  Added support for using FriCAS as ECL shared library.

-  Polynomial factorization uses Kaltofen-Shoup method when applicable.

-  ’$createLocalLibDb’ defaults to false.

-  Simpler, more predictable equality for algebraic numbers (no longer
   uses ’trueEqual’).

-  Renamed LinearlyExplicitRingOver to LinearlyExplicitOver.

-  Renamed ’length’ in Tuple to ’#’.

-  Removed argumentless ’random’.

Bug fixes, in particular:

-  Fixed several build problems.

-  Handle scripted symbols in DeRhamComplex.

-  Handle empty matrices in more places.

-  Fixed unparse of negative integers.

-  No longer crashes on quoted expressions in types.

FriCAS 1.2.7
^^^^^^^^^^^^

-  New package implementing van Hoej factorization algorithm for LODO-s.

-  Gcd over Expression(Integer) now uses modular method.

-  Improvements to integrator, in particular trigonometric functions are
   consistently integrated via transformation to complex exponentials.

-  Some categories and domains are more general. In particular
   OrderedFreeMonoid is removed, as ordered case is handled by
   FreeMonoid.

-  Category Monad in renamed to Magma. Domain Magma is renamed to
   FreeMagma.

Bug fixes, in particular:

-  Coercion of square matrices to polynomials is fixed.

-  Problem with division by 0 in derivative of ’ellipticPi’ is fixed.

-  Division in Ore algebras used to cause infinite loop when
   coefficients were power series.

FriCAS 1.2.6
^^^^^^^^^^^^

-  Polynomial factorization is available for larger class of base rings.

-  Improvements to integrator.

-  ’normalize’ can be applied to list of expressions.

-  Eigenvalues can be computed over larger range of base fields.

-  Common denominator package handles now multivariate polynomials.

-  More uniform break (error) handling.

Bug fixes, in particular:

-  ’distribute’ handles ’box’ operator.

-  Fixed problem with guessing over multivariate polynomials.

-  Fixed hashcode handling for Void in Aldor.

FriCAS 1.2.5
^^^^^^^^^^^^

-  Several improvements to integrator.

-  Improvements to handling of series, in particular new function
   ’prodiag’ to compute infinite products, ’series’ and ’coefficients’
   for multivariate Taylor series, new ’laurent’ function which builds
   Laurent series from order and stream of coefficients.

-  GMP should now work with sbcl on all platforms and with Closure CL on
   all platforms except for Power PC.

-  Added a few domains for discrete groups.

-  Extended GCD in Ore algebras can now return coefficients of both GCD
   and LCM.

-  New function for computing integrals of solutions of linear
   differential operators.

-  ’)savesystem’ command is now removed.

-  Continuation lines which begins like commands are no longer treated
   as commands.

Bug fixes, in particular:

-  Fixed printing of scripted symbols.

-  Fixed ’totalDegreeSorted’ (affected Groebner bases).

-  Fixed few problems with Hensel lifting (including SF bug 47).

-  Fixed ’series’ in UnivariateLaurentSeriesConstructor.

-  Fixed ’order’ in SparseUnivariatePowerSeries.

-  Printing of series now respect ’showall’ setting, cyclic series are
   detected.

-  Fixed problem with interpreter preferring Union to base type.

FriCAS 1.2.4
^^^^^^^^^^^^

-  New cylindrical decomposition package.

-  New GnuDraw package for plotting via gnuplot.

-  Texmacs interface now handles Cork symbols.

-  Added double precision versions of several special functions (needed
   for plotting).

-  Nopile mode for Spad is changed to be more convenient.

-  ’stringMatch’ is removed (was broken beyond repair).

Bug fixes, in particular:

-  Fixed interpreter assignment to parts of nested aggregates (issue
   376).

-  Fixed interpreter coercion from Equation to Boolean (issue 359).

-  Fix printing of ’%i’ in types (issue 132).

-  Disabled incorrect shortcut during coercion (issue 29).

-  Difference of intervals now agrees with definition as interval
   operation.

-  Avoid overwriting loop limit and increment.

-  Fix a polynomial gcd failure due to bad reduction.

-  Avoid mangling unevaluated algebraic integrals.

-  Fix integration of unevaluated derivatives.

-  | Restore parser handling of ’
   | /’ and ’/
   | ’.

-  Properly escape strings and symbols in TeXFormat.

-  Fix toplevel multiparameter macros.

-  Fix problem with missing parentheses around plexes.

-  Avoid crash when printing error message from ’-eval’.

-  Redirect I/O when running programs from Closure CL.

FriCAS 1.2.3
^^^^^^^^^^^^

-  Improved integration in terms of ’Ei’ and ’erf’.

-  Classical orthogonal polynomials may be used as expressions.

-  More cases of generalized indexing for two dimensional arrays.

-  Value of ’lambertW’ at ’-1/e’ is now simplified.

-  FriCAS now knows that formal derivatives are commutative.

-  ’setelt’ is renamed to ’setelt!’.

-  ’)read’ now creates intermediate files in current directory.

-  Continuation characters in comments are now respected.

-  In Spad ’$Lisp’ calls now must have a type.

-  In Spad did only minimal checking of its argument. Now argument to
   must be a or or a literal list of -s.

Bug fixes, in particular:

-  Input lines with empty continuation are no longer lost.

-  Types like “failed” now consistently use string quotes in output
   form.

-  Fixed pattern matching using in patterns.

-  Fixed ’)display op coerce’.

-  Fixed ’)version’ command.

-  Fixed crash when printing ’%’.

-  Fix a buffer overflow in HyperDoc.

-  Fixed HyperDoc errors in ’Dependants’ and ’Users’.

-  HyperDoc browser better handles constructors with parameters.

FriCAS 1.2.2
^^^^^^^^^^^^

-  Improvements to ’integrate’: better handling of algebraic integrals,
   new routine which handles some integrals containing ’lambertW’.

-  Improvements to ’limit’, now Gruntz algorithm knows about a few
   tractable functions.

-  Smith form of sparse integer matrices is now much more efficient.

-  Generalized indexing for two dimensional arrays.

-  Pile/nopile mode is now restored after ’)read’ or ’)compile’. Piling
   rules now accept some forms of multiline lists.

-  Eliminated version checking in generated code. Note: this change
   means that Spad code compiled by earlier FriCAS versions will not run
   in FriCAS 1.2.2.

-  Updated Aldor interface to work with free Aldor.

Bug fixes, in particular:

-  Interpreter can now handle complicated mutually recursive functions.

-  Spad compiler should now correctly handle ’has’ inside a function.

-  Fixed derivatives of Whittaker functions.

FriCAS 1.2.1
^^^^^^^^^^^^

-  Improvements to ’integrate’: a new routine for integration in terms
   of Ei, better handling of algebraic integrals.

-  Implemented ’erfi’.

-  Derivatives of ’asec’, ’asech’, ’acsc’ and ’acsch’ use different
   formula so that numeric evaluation of derivative will take correct
   branch on real axis.

-  Linear dependence package is changed to be consistent with linear
   solvers.

-  It is now possible to extract empty submatrices.

-  Changed default style of 3D graphics.

-  Support for building Mac OS application bundle.

Bug fixes, in particular:

-  fixed few cases of wrong or unevaluated integrals.

-  better zero test during limit computation avoids division by zero.

-  fixed buffer overflow problems in view3D.

-  ’reducedSystem’ on empty input returns basis of correct size.

FriCAS 1.2.0
^^^^^^^^^^^^

-  New MatrixManipulation package.

-  New ParallelIntegrationTools package.

-  Gruntz algorithm is now used also for finite one-sided limits.

-  FriCAS has now true 2-dimensional arrays (previously they were
   emulated using vectors of vectors).

-  Speedups in some matrix operations and in arithmetic with algebraic
   expressions.

-  FreeModule is now more general, it allows Comparable as second
   argument.

-  Changed Spad parser, it now uses common scanner with interpreter.
   Spad language is now closer to interpreter language and Aldor.
   ’leave’ is removed, ’free’, ’generate’ and ’goto’ are now keywords.
   Pile rules changed slightly, they should be more intuitive now. Error
   messages from Spad parser should be slightly better.

Bug fixes, in particular:

-  Fixed a few build problems.

-  Eliminated division by 0 during ’normalize’.

-  ’nthRootIfCan’ removes leading zeros from generalized series (this
   avoids problems with power series expanders).

-  Fixed corruption of formal derivatives.

-  Fixed two problems with Fortran output.

-  Fixed ’)untrace’ and ’)undo’. Fixed ’)trace’ with ECL.

-  Fixed problem with calling efricas if user’s default shell is (t)csh.

FriCAS 1.1.8
^^^^^^^^^^^^

-  Improvements of pattern matching integrator, it can now integrate in
   terms of Fresnel integrals and better handles integrals in terms of
   Si and Ci.

-  Better integration of symbolic derivatives.

-  Better normalization of Liouvillian functions.

-  New package for computing limits using Gruntz algorithm.

-  Faster removal of roots from denominators.

-  New domains for multivariate Ore algebras and partial differential
   operators.

-  New package for noncommutative Groebner bases.

-  New domain for univariate power series with arbitrary exponents.

-  New special functions: Shi and Chi.

-  Several aggregates (in particular tables) allow more general
   parameter types.

-  New domain for hash tables using equality from underlying domain.

Bug fixes, in particular:

-  Fixed problem with gcd failing due to bad reduction.

-  Fixed series of ’acot’ and Puiseux series of several special
   functions.

-  Fixed wrong factorization of differential operators.

-  Fixed build problem on recent Mac OS X.

FriCAS 1.1.7
^^^^^^^^^^^^

-  Improved integration in terms of special functions.

-  Updated new graphics framework and graph theory package.

-  Added routines for numerical evaluation of several special functions.

-  Added modular method for computing polynomial gcd over algebraic
   extensions.

-  Derivatives of fresnelC and fresnelS are changed to agree with
   established convention.

-  When printing floats groups of digits are now separated by
   underscores (previously were separated by spaces).

-  Added C code for removing directories, this speeds up full build and
   should avoid build problems on Mac OSX.

Bug fixes, in particular:

-  Series expansion now handle poles of Gamma.

-  Fixed derivatives of meijerG.

FriCAS 1.1.6
^^^^^^^^^^^^

-  Added experimental graph theory package.

-  Added power series expanders for Weierstrass elliptic functions at 0.

-  New functions: kroneckerProduct and kroneckerSum for matrices,
   numeric weierstrassInvariants and modularInvariantJ, symbolic Jacobi
   Zeta, double float numeric elliptic integrals.

-  New domains for vectors and matrices of unsigned 8 and 16 bit
   integers.

-  Changes to Spad compiler: underscores which are not needed as escape
   are now significant in Spad names and strings, macros with parameters
   are supported, added partial support for exceptions, braces can be
   used for grouping.

-  A few speedups.

-  Reduced disc space usage during build.

Bug fixes, in particular:

-  Fixed eval of hypergeometricF at 0

-  Fixed problem with scope of macros.

-  Worked around problems with opening named pipes in several Lisp
   implementations.

-  Fixed a problem with searching documentation via HyperDoc.

-  Fixed build problem on Mac OSX.

FriCAS 1.1.5
^^^^^^^^^^^^

-  Added numeric version of lambertW.

-  New function ’rootFactor’ which tries to write roots of products as
   products of roots.

-  ’try’, ’catch’ and ’finally’ are now Spad keywords.

-  Experimental support for using gmp with Closure CL (64-bit Intel/Amd
   only).

-  New categories CoercibleFrom and ConvertibleFrom. New domain for
   ordinals up to epsilon0. New domain for matrices of machine integers.
   New package for solving linear equations written as expressions
   (faster then general expression solver).

-  Functions exported by Product() are now called ’construct’, ’first’
   and ’second’ (instead of ’makeprod’, ’selectfirst’ and ’selectsecond’
   respectively).

-  Some functions are now much faster, in particular bivariate
   factorization over small finite fields.

-  When using sbcl FriCAS now tries to preload statistical profiler.

Bug fixes, in particular:

-  Fixed handling of Control-C in FriCAS compiled by recent sbcl.

-  Fixed HyperDoc crash due to bad handling of ’#’.

-  Fixed power series expanders for elliptic integrals.

-  Fixed ’possible wild ramification’ problem with algebraic integrals.

-  ’has’ in interpreter now correctly handles .

-  Spad compiler can now handle single at top level of a function.

-  Fixed few problems with conditional types in Spad compiler.

FriCAS 1.1.4
^^^^^^^^^^^^

-  New domains for combinatorial probability theory by Franz Lehner.

-  Improved integration of algebraic functions.

-  Initial support for semirings.

-  Updated framework for theory of computations.

-  In Spad parser and are now right-associative.

-  Spad parser no longer transforms relational operators.

-  Join of categories is faster which speeds up Spad compiler.

Bug fixes, in particular:

-  Retraction of ’rootOf’ from Expression(Integer) to AlgebraicNumber
   works now.

-  Attempt to print error message about invalid type no longer crash (SF
   2977357).

-  Fixed few problems in Spad compiler dealing with conditional exports.

-  HyperDoc now should find all function descriptions (previously it
   missed several).

FriCAS 1.1.3
^^^^^^^^^^^^

-  Added “jet bundle” framework by Werner Seiler and Joachim Schue,
   which includes completion procedure and symmetry analysis for PDE.

-  Better splitting of group representations (added Holt-Rees
   improvement to meatAxe).

-  Added numeric versions of some elliptic integrals and few more
   elliptic functions.

-  Speeded up FFCGP (finite fields via Zech logarithms).

-  New experimental flag (off by default, set via setSimplifyDenomsFlag)
   which if on causes removal of irrationalities from denominators.
   Usually it causes slowdown, but on some examples gives huge speedup.
   It may go away in future (when no longer needed).

-  Added experimental framework for theory of computations.

Bug fixes, in particular:

-  Numerical solutions of polynomial systems have now required accuracy
   (SF 2418832).

-  Fixed problem with crashes during tracing.

-  Fixed a problem with nested iteration (SF 3016806).

-  Eliminated stack overflow when concatenating long lists.

FriCAS 1.1.2
^^^^^^^^^^^^

-  Experimental Texmacs interface and Texmacs format output.

-  Guessing package can now guess algebraic dependencies.

-  Expansion into Taylor series and limits now work for most special
   functions.

-  Spad to Aldor translator is removed.

-  Spad compiler no longer allows to denote sets using braces.

Bug fixes, in particular:

-  Fixed few cases where elementary integrals were returned unevaluated
   or produced wrong results.

-  Unwanted numerical evaluation should be no longer a problem (FriCAS
   interpreter now very strongly prefers symbolic evaluation over
   numerical evaluation).

-  Fixed a truncation bug in guessing package which caused loss of some
   correct solutions.

-  TeX and MathML format should correctly put parentheses around and
   inside sums and products.

-  Fixed few problems with handling of Unicode.

FriCAS 1.1.1
^^^^^^^^^^^^

-  New graphics framework.

-  Support for using GMP with sbcl on 32/64 bit AMD/Intel processors (to
   activate it one must use ’–with-gmp’ option to configure).

-  Improvements to integration and normalization. In particular
   integrals containing multiple non-nested roots should now work much
   faster. Also FriCAS now can compute more integrals of Liouvillian
   functions.

-  Several new special functions.

-  Improvements to efricas.

-  Looking for default init file FriCAS now first tries to use
   ’.fricas.input’ and only if that fails it looks for ’.axiom.input’.

Bug fixes, in particular:

-  Numeric atan, asin and acos took wrong branch.

-  WeierstrassPreparation package did not work.

-  Saving and restoring history should be now more reliable.

-  Fixed two bugs in Spad compiler related to conditional compilation.

-  Fixed a problem with rational reconstruction which affected guessing
   package.

FriCAS 1.1.0
^^^^^^^^^^^^

-  New domains and packages: VectorSpaceBasis domain, DirichletRing
   domain, 3D graphic output in Wavefront .obj format, specialized
   machine precision numeric vectors and matrices (faster then general
   vectors and matrices), Html output.

-  Support Clifford algebras corresponding to non-diagonal matrix, added
   new operations.

-  ’normalize’ now tries to simplify logarithms of algebraic constants.

-  New functions: Fresnel integrals, carmichaelLambda.

-  Speed improvements: several polynomial operations are faster, faster
   multiplication in Ore algebras, faster computation of strong
   generating set for permutation groups, faster coercions.

-  Several improvements to the guessing package (in particular new
   option Somos for restricting attention to Somos-like sequences

Bug fixes, in particular:

-  FriCAS can now compute multiplicative inverse of a power series with
   constant term not equal to 1.

-  Fixed a problem with passing interpreter functions to algebra.

-  Two bugs causing crashes in HyperDoc interface are fixed.

-  FriCAS now ignores sign when deciding if number is prime.

-  A failing coercion that used to crash FriCAS is now detected.

-  ’has’ test sometimes gave wrong result.

-  Plotting fixes.

FriCAS 1.0.9
^^^^^^^^^^^^

-  Speed improvements to polynomial multiplication, power series
   multiplication, guessing package and coercion of polynomials to
   expressions.

-  Domains for tensor products.

-  ’Complex(Integer)’ is now UniqueFactorizationDomain.

-  Types in interpreter are now of type ’Type’ (instead of ’Domain’) and
   categories in interpreter are of type ’Category’ (instead of
   ’Subdomain(Domain)’).

-  Interpreter functions can now return ’Type’.

-  New function for files: ’flush’.

-  Spad compiler: return in nested functions and nested functions
   returning functions.

Bug fixes, in particular:

-  Several fixes to guessing package.

-  Avoid crash when unparsing equations.

-  Equation solver accepts more solutions.

-  Fixed handling of ’Tuple’ in Spad parser.

-  Fixed miscompilation of record constructor by Spad compiler.

FriCAS 1.0.8
^^^^^^^^^^^^

-  Improved version of guessing package. It can now handle much larger
   problems than before. Added ability to guess functional substitution
   equations.

-  Experimental support for build using CMU CL

-  Various speed improvements including faster indexing for two
   dimensional arrays

-  By default FriCAS build tries to use sbcl.

-  Building no longer require patch.

Bug fixes, in particular:

-  correct definition of random() for matrices

-  conditionals in .input files work again

-  Spad compiler now recognizes more types as equal

-  fixed problem with pattern-matching quote

FriCAS 1.0.7
^^^^^^^^^^^^

-  Comparisons between elements of the Expression domain are undefined.
   Earlier versions gave confusing results for expressions like ’%e <
   ()’ – now FriCAS will complain about ’<’ being undefined.

-  A domain for general quaternions was added.

-  Equality in Any is now more reasonable – it uses equality from
   underlying domain if available.

-  Messages about loading of components are switched off by default.

-  Release build benefits from parallel make.

-  In Spad code a single quote now means that the following token is a
   symbol.

-  Reorganization of algebra sources, in particular several types have
   changed (this may affect users Spad code).

Bug fixes, in particular:

-  Categories with default package can be used just after definition
   (fixes 1.0.6 regression).

-  Plots involving 0 or 1 work now.

-  Numbers in radix bigger than 10 appear correctly in TeX output.

-  Fixed browser crashes when displaying some domains.

-  Fix horizontal display of fractions.

-  Allow local domains in conditionals (in Spad code).

-  Fixed problem with splitting polynomials and nested extensions.

FriCAS 1.0.6
^^^^^^^^^^^^

-  the axiom script is no longer installed (use fricas script instead)

-  some undesirable simplification are no longer done by default, for
   example now asin(sin(3)) is left unevaluated

-  support lambda expressions using ’+->’ syntax and nested functions in
   Spad

-  better configure, support for Dragonfly BSD

-  faster bootstrap, also parallel (this does not affect speed of
   release build)

Several bug fixes, in particular:

-  fixed a regression introduced in 1.0.4 which caused equality for
   nested products to sometimes give wrong result

-  corrected fixed output of floating point numbers,

-  operations on differential operators like symmetric power work now

-  fixed crashes related to coercing power series

-  functions returning Void can be traced

FriCAS 1.0.5
^^^^^^^^^^^^

-  improvement to normalize function, it performs now much stronger
   simplifications than before

-  better integration: due to improved normalize FriCAS can now
   integrate many functions that it previously considered unintegrable

-  improvement to Martin Rubey guessing package, for example it can now
   guess differential equation for the generating function of integer
   partitions

-  better support for using type valued functions

-  several bug fixes

FriCAS 1.0.4
^^^^^^^^^^^^

-  significant speedups for some operations (for example definite
   integration)

-  support for building algebra using user-defined optimization settings

-  support for mouse wheel in HyperDoc browser

-  included support for interfacing with Aldor

-  new optional Emacs mode and efricas script to run FriCAS inside emacs

-  better unparse

-  removed support for attributes (replaced by empty categories) and use
   of colon for type conversions in Spad code

-  a few bug fixes

FriCAS 1.0.3
^^^^^^^^^^^^

-  added multiple precision Gamma and logGamma functions

-  better line editing

-  removed some undocumented and confusing constructs from Spad language

-  added new categories for semiring and ordered semigroup, direct
   product of monoids is now a monoid

-  internal cleanups and restructurings

-  a few bug fixes

FriCAS 1.0.2
^^^^^^^^^^^^

-  ’)nopiles’ command gives conventional syntax

-  added pfaffian function

-  ECL support

-  Graphics and Hyperdoc work using openmcl or ECL

-  Output may be now delimited by user defined markers

-  Experimental support for using as a Lisp library

-  Spad compiler is now significantly faster

-  Several bug fixes

FriCAS 1.0.1
^^^^^^^^^^^^

-  Graphics and Hyperdoc work using sbcl or clisp

-  Builds under Cygwin (using Cygwin clisp)

-  MathML support contributed by Arthur C. Ralfs

-  Help files created by Tim Daly

-  Added SPADEDIT script

-  Full release caches all generated HyperDoc pages

-  Bug fixes, including implementing some missing functions and build
   fixes

FriCAS 1.0.0
^^^^^^^^^^^^

The 1.0 release is the first release of FriCAS. Below we list main
differences compared to AXIOM September 2006.

Numerous bug fixes (in particular HyperDoc is now fully functional on
Unix systems).

FriCAS includes guessing package written by Martin Rubey. This package
provides unique ability to guess formulas for sequences of numbers or
polynomials.

Some computation, in particular involving Expression domain, should be
much faster. FriCAS to go trough its testsuite needs only half of the
time needed by AXIOM September 2006.

Spad compilation is faster (in some cases 2 times faster).

FriCAS is much more portable than AXIOM September 2006. It can be build
on Linux, many Unix systems (for example Mac OSX and Solaris 10) and
Windows. It can be build on top of gcl, sbcl, clisp or openmcl (gcl and
sbcl based FriCAS is fully functional, clisp or openmcl based one lacks
graphic support).

Many unused or non-working parts are removed from FriCAS. In particular
FriCAS does not contain support for NAG numerical library.

FriCAS can be build from sources using only a few pre-generated Lisp
files for bootstrap – only to bootstrap Shoe translator. This means that
modifying FriCAS algebra is now much easier.

Changes to Spad language
------------------------

ugSpadChanges

#. as name of current domain is no longer supported, use instead.

#. Attributes are no longer supported, use niladic categories with no
   exports instead.

#. Floating point numbers without leading zero are no longer supported,
   so instead of use

#. Anonymous functions using , , etc. are no longer supported, to define
   anonymous functions use .

#. Braces no longer construct sets. So instead of use .

#. Old Spad used colon () to denote conversion, like but performing even
   less checking. This is no longer supported, use or instead.

#. There was an alternative spelling for brackets and braces, in FriCAS
   this is no longer supported, so one has to write brackets and braces
   as is.

#. was handled in special way by the compiler. This is no longer
   supported.

#. Old Spad compiler used to transform relational operators in ways
   which are correct for linear order, but may conflict with other uses
   (as partial order or when generating ). FriCAS no longer performs
   this transformation. Similarely, Spad parser no longer treats and in
   special way.

#. Quote in old Spad allowed to insert arbitrary literal Lisp data,
   FriCAS only allows symbols after quote. Code using old behavior needs
   to be rewritten, however it seems that this feature was almost
   unused, so this should be no problem.

#. Old Spad treated statement consisting just of constructor name (with
   arguments if needed) as request to import the constructor. FriCAS
   requires keyword.

#. In FriCAS are right associative. Also, right binding power of is
   increased, which allows more natural writing of code.

#. Few non-working experimental features are removed, in particular
   partial support for APL-like syntax.

#. FriCAS implemented parametric macros in the Spad compiler.

#. FriCAS allows simplified form for exporting constants (without
   keyword).

#. FriCAS added partial support for exception handling (currently only
   part).

#. The construct is removed from FriCAS. Use instead.

#. is no longer a keyword. , , are FriCAS keywords.

#. ’$Lisp’ calls now must have a type

#. did only minimal checking of its argument. Now argument to must be a
   or or a literal list of -s.

There are also library changes that affect user code:

#. lost its definition as exponentiation, use instead.

#. is no longer used as negation (it means exponentiation now) and no
   longer means inequality, use and instead.

#. is renamed to .

#. Operator properties are now symbols and not strings, so instead of
   use

#. There is new category , several constructors that asserted now only
   assert .

Online Information
------------------

onlineInformation FriCAS information can be found online at

-  http://fricas.sourceforge.net – The official homepage of FriCAS.

-  http://axiom-wiki.newsynthesis.org – A wiki site related to FriCAS.

-  http://sourceforge.net/p/fricas/code/HEAD/tree/ – The official source
   code repository.

-  https://github.com/fricas/fricas – A live git mirror of the official
   SVN repository.

-  http://fricas.github.io – Documentation of FriCAS including the API
   of the FriCAS library.

Old News about AXIOM Version 2.x
--------------------------------

ugWhatsNewNAG Many things have changed in this version of AXIOM and we
describe many of the more important topics here.

The NAG Library Link
~~~~~~~~~~~~~~~~~~~~

nagLinkIntro Content removed, NAGLink is no longer included in FriCAS.

Interactive Front-end and Language
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ugWhatsNewLanguage The keyword has been replaced by the keyword for
compatibility with the new AXIOM extension language. See section for
more information.

Curly braces are no longer used to create sets. Instead, use followed by
a bracketed expression. For example,

::

    set [1,2,3,4]

.. math:: {\left\{ 1{,\:}2{,\:}3{,\:}4 \right\}}

``Set(PositiveInteger)``

Curly braces are now used to enclose a block (see section for more
information). For compatibility, a block can still be enclosed by
parentheses as well.

New coercions to and from type have been added. For example, it is now
possible to map a polynomial represented as an expression to an
appropriate polynomial type.

Various messages have been added or rewritten for clarity.

Library
~~~~~~~

ugWhatsNewLibrary The domain has been added. This domain computes
factor-free full partial fraction expansions. See section for examples.

We have implemented the Bertrand/Cantor algorithm for integrals of
hyperelliptic functions. This brings a major speedup for some classes of
algebraic integrals.

We have implemented a new (direct) algorithm for integrating
trigonometric functions. This brings a speedup and an improvement in the
answer quality.

The SmallFloat domain has been renamed and SmallInteger has been renamed
. The new abbreviations as and , respectively. We have defined the macro
SF, the old abbreviation for SmallFloat, to expand to and modified the
documentation and input file examples to use the new names and
abbreviations. You should do the same in any private FriCAS files you
have.

We have made improvements to the differential equation solvers and there
is a new facility for solving systems of first-order linear differential
equations. In particular, an important fix was made to the solver for
inhomogeneous linear ordinary differential equations that corrected the
calculation of particular solutions. We also made improvements to the
polynomial and transcendental equation solvers including the ability to
solve some classes of systems of transcendental equations.

The efficiency of power series have been improved and left and right
expansions of at a pole of can now be computed. A number of power series
bugs were fixed and the domain was added. The power series variable can
appear in the coefficients and when this happens, you cannot
differentiate or integrate the series. Differentiation and integration
with respect to other variables is supported.

A domain was added for representing asymptotic expansions of a function
at an exponential singularity.

For limits, the main new feature is the exponential expansion domain
used to treat certain exponential singularities. Previously, such
singularities were treated in an *ad hoc* way and only a few cases were
covered. Now AXIOM can do things like

::

    limit( (x+1)^(x+1)/x^x - x^x/(x-1)^(x-1), x = %plusInfinity)

in a systematic way. It only does one level of nesting, though. In other
words, we can handle some function with a pole , but not some function
with a pole

The computation of integral bases has been improved through careful use
of Hermite row reduction. A P-adic algorithm for function fields of
algebraic curves in finite characteristic has also been developed.

Miscellaneous: There is improved conversion of definite and indefinite
integrals to ; binomial coefficients are displayed in a new way; some
new simplifications of radicals have been implemented; the operation for
converting to rectangular coordinates has been added; symmetric product
operations have been added to .

ugWhatsNewHyperDoc The buttons on the titlebar and scrollbar have been
replaced with ones which have a 3D effect. You can change the foreground
and background colors of these “controls” by including and modifying the
following lines in your **.Xdefaults** file.

::

    Axiom.hyperdoc.ControlBackground: White
    Axiom.hyperdoc.ControlForeground: Black

For various reasons, sometimes displays a secondary window. You can
control the size and placement of this window by including and modifying
the following line in your **.Xdefaults** file.

::

    Axiom.hyperdoc.FormGeometry: =950x450+100+0

This setting is a standard X Window System geometry specification: you
are requesting a window 950 pixels wide by 450 deep and placed in the
upper left corner.

Some key definitions have been changed to conform more closely with the
CUA guidelines. Press F9 to see the current definitions.

Input boxes (for example, in the Browser) now accept paste-ins from the
X Window System. Use the second button to paste in something you have
previously copied or cut. An example of how you can use this is that you
can paste the type from an FriCAS computation into the main Browser
input box.

Documentation
~~~~~~~~~~~~~

ugWhatsNewDocumentation We describe here a few additions to the on-line
version of the AXIOM book which you can read with HyperDoc.

A section has been added to the graphics chapter, describing how to
build graphs from lists of points. An example is given showing how to
read the points from a file. See section for details.

A further section has been added to that same chapter, describing how to
add a graph to a viewport which already contains other graphs. See
section for details.

Chapter 3 and the on-line help have been unified.

An explanation of operation names ending in “?” and “!” has been added
to the first chapter. See the end of the section for details.

An expanded explanation of using predicates has been added to the sixth
chapter. See the example involving in the middle of the section for
details.

Documentation for the , and commands has been greatly changed. This
reflects the ability of the to now invoke the compiler, the impending
deletion of the command and the new command. The command replaces and is
compatible with the compiled output from both the old and new compilers.

 compiler - Enhancements and Additions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ugTwoTwoAldor Content removed - (now using name *Aldor*) is a separate
project.

New polynomial domains and algorithms
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ugTwoTwoPolynomials Univariate polynomial factorization over the
integers has been enhanced by updates to the type and friends from
Frederic Lehobey (Frederic.Lehobey@lifl.fr, University of Lille I,
France).

The package constructor provides efficient algorithms by Lionel Ducos
(Lionel.Ducos@mathlabo.univ-poitiers.fr, University of Poitiers, France)
for computing sub-resultants. This leads to a speed up in many places in
FriCAS where sub-resultants are computed (polynomial system solving,
algebraic factorization, integration).

Based on this package, the domain constructor extends the constructor .
In a similar way, the extends the constructor ; it also provides some
additional operations related to polynomial system solving by means of
triangular sets.

Several domain constructors implement regular triangular sets (or
regular chains). Among them and . They also implement an algorithm by
Marc Moreno Maza (marc@nag.co.uk, NAG) for computing triangular
decompositions of polynomial systems. This method is refined in the
package in order to produce decompositions by means of Lazard triangular
sets. For the case of polynomial systems with finitely many solutions,
these decompositions can also be computed by the package .

The domain constructor by Renaud Rioboo (Renaud.Rioboo@lip6.fr,
University of Paris 6, France) provides the real closure of an ordered
field. The implementation is based on interval arithmetic. Moreover, the
design of this constructor and its related packages allows an easy use
of other codings for real algebraic numbers.

Based on triangular decompositions and the constructor, the package
provides operations for computing symbolically the real or complex roots
of polynomial systems with finitely many solutions.

Polynomial arithmetic with non-commutative variables has been improved
too by a contribution of Michel Petitot (Michel.Petitot@lifl.fr,
University of Lille I, France). The domain constructors and provide
recursive and distributed representations for these polynomials. They
are the non-commutative equivalents for the and constructors. The
constructor implement Lie polynomials in the Lyndon basis. The
constructor manage polynomials with non-commutative variables in the
Poincaré-Birkhoff-Witt basis from the Lyndon basis. This allows to
compute in the Lie Group associated with a free nilpotent Lie algebra by
using the domain constructor.

Enhancements to HyperDoc and Graphics
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ugTwoTwoHyperdoc From this version of AXIOM onwards, the pixmap format
used to save graphics images in color and to display them in HyperDoc
has been changed to the industry-standard XPM format. See
ftp://koala.inria.fr/pub/xpm.

Enhancements to NAGLink
~~~~~~~~~~~~~~~~~~~~~~~

ugTwoTwoNAGLink Content removed - NAGLink is no longer included in
FriCAS.

Enhancements to the Lisp system
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ugTwoTwoCCL Content removed - no longer relevant since FriCAS runs on
different Lisp systems.
